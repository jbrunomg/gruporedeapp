<?php
// defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Auth_model','',TRUE);	
        $this->load->helper('cookie');
	}

	public function index()
	{
		//var_dump(get_cookie('CookieEmail'));die();
	
		// $email    =  (isset($_COOKIE['CookieEmail'])) ? base64_decode($_COOKIE['CookieEmail']) : '';
		// $senha    =  (isset($_COOKIE['CookieSenha'])) ? base64_decode($_COOKIE['CookieSenha']) : '';	
		// $lembrete =  'SIM';

		//$emai = 
		$email    =    get_cookie('CookieEmail') ? base64_decode(get_cookie('CookieEmail')) : '';
		$senha    =    get_cookie('CookieSenha') ? base64_decode(get_cookie('CookieSenha')) : '';

		// var_dump($email);die();

		$versao = getOS();

		if ($versao == 'Android' OR $versao == 'iPhone' ) {
			# Só abre o APP se estiver no modo celular 
			if (!empty($email) && !empty($senha)){
		
			$this->processarLogin($email,$senha);

			}else{			
			$this->load->view('login');

			}
		}else{
			$this->load->view('offline');
		}

		
	}

	public function processarLogin($email = null,$senha = null)
	{
		$email = $email <> '' ? $email : $this->input->post('login');
		$senha = $senha <> '' ? $senha : $this->input->post('senha');
		$lembrete = 'SIM';

		// var_dump($email.'Aqui');die();

		// $this->form_validation->set_rules('login', 'Login', 'trim|required|valid_email');
       
        //$this->form_validation->set_rules('senha', 'Senha', 'trim|required');

        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        // if ($this->form_validation->run() == FALSE)
        // {	
        // 	var_dump($email.'Aqui');die();	
        //     $this->load->view('login');
        // }
        // else
        // {	   	
        	

        	$dados = array(
        					'usuario_email'   => $email, 
        					'usuario_senha'   => sha1(md5($senha)),
        					'usuario_visivel' => 1,
        					'usuario_ativo'   => 1,
        					'perfil_visivel'  => 1
        				  );        	

        	$resultado = $this->Auth_model->processarLogin($dados); 

        	// echo "<pre>";
        	// var_dump($resultado);die();

        	if($resultado){

	        	$session = array(
						        'usuario_id'            => $resultado[0]->usuario_id,
						        'usuario_email'         => $resultado[0]->usuario_email,
						        'usuario_nome'          => $resultado[0]->usuario_nome,
						        'permissao_nome'        => $resultado[0]->perfil_descricao,						        					       
						        'usuario_permissoes_id' => $resultado[0]->usuario_perfil,
						        'usuario_imagem'        => $resultado[0]->usuario_imagem,                               						      
						        'permissao'             => $resultado[0]->perfil_permissoes					       
							);

				$this->session->set_userdata($session);

    			if($lembrete == 'SIM'):

				   $expira = time() + 60*60*24*30; 
				   set_cookie('CookieLembrete', base64_encode('SIM'), $expira);
				   set_cookie('CookieEmail', base64_encode($email), $expira);
				   set_cookie('CookieSenha', base64_encode($senha), $expira);

				else:

				   set_cookie('CookieLembrete');
				   set_cookie('CookieEmail');
				   set_cookie('CookieSenha');

				endif;	
			

				redirect('Dashboard','refresh');

        	}else{

        		$this->session->set_flashdata('erro', 'Login/Senha Inválidos');
        		$this->load->view('login');
        	}
        //}
		
	}

	public function sair()
	{
		$this->session->sess_destroy();

	    delete_cookie('CookieLembrete');
		delete_cookie('CookieEmail');
		delete_cookie('CookieSenha');

		$this->load->view('login');
	}










































	

	public function sendmail($view,$titulo,$email)
    {               

       //$emitente = $this->mapos_model->getEmitente();
       // var_dump($view);
       // $result = $this->load->view($view,'',TRUE);

        $this->load->library('email');
        $this->email->from('emailsite@wdmtecnologia.com.br', 'WDM-Controller');
        $this->email->to($email);
        //$this->email->to('bruno@wdmtecnologia.com.br');
        //$this->email->cc('bruno@wdmtecnologia.com.br'); 
		$this->email->reply_to('financeiro@wdmtecnologia.com.br');
		
        $this->email->subject($titulo);
       // $this->email->message($result);
        $this->email->message($view);
        //$this->email->attach('/path/to/file1.png');
        //$this->email->attach('/path/to/file2.pdf');
        if ($this->email->send()){
            $this->session->set_flashdata('success','Email enviado com sucesso.');
            return true;
        }
        else{
            $this->session->set_flashdata('error','Erro ao tentar enviar o email');
            return false;
        }
    }

    public function boasVindas()
    {   

        $view = '/templates/boasvindas';

        $titulo = "[WDM-Controller] Seja bem vindo ao WDM-Controller";        
        $resultado = $this->load->view($view,'',TRUE); 
        //$resultado = $this->sendmail($resultado,$titulo);
        var_dump($resultado);
    }

    public function checkList()
    {        
        $view = '/templates/checklistauto';

        $titulo = "[WDM-Controller] Confirmação de pagamento!";        
        //$resultado = $this->sendmail($view,$titulo);        
        $resultado = $this->load->view($view,'',TRUE); 
        var_dump($resultado);
    }

    public function pagamentoAtraso()
    {        
        foreach (unserialize(SGBD) as $c) {
        	
        	$this->data['pagamento']  = $this->Auth_model->pagamentoAtraso($c); 

        	// Só entrar se tiver pagamento em atraso;
	        if ($this->data['pagamento']) {           
	        $num = $this->data['pagamento'][0]->dias_vencido;
	            // Só envia se tiver com 05 dias em atraso;     
	            // é múltiplo de 5!
	            if (($num % 5) == 0) {
	                $email = $this->data['pagamento'][0]->email_cron;	                      
	                $view = $this->load->view('/templates/pagamentoatraso',$this->data,true);
	                $titulo = "[WDM-Controller] Notificação de atraso!"; 
	                $resultado = $this->sendmail($view,$titulo,$email);
	                //var_dump($view);           
	            }       
	        }

        }

         


    } 

    public function pagamentoaVencer()
    {        
        foreach (unserialize(SGBD) as $c) {

	        $this->data['pagamento']  = $this->Auth_model->pagamentoaVencer($c); 

	        // Só entrar com dois dias a vencer;
	        if ($this->data['pagamento']) {           
	        $num = $this->data['pagamento'][0]->dias_vencido;
	        	// Só envia se tiver com 02 dias a vencer     
		        if ($num == 2) {    
		            $email = $this->data['pagamento'][0]->email_cron;	    
		            $view = $this->load->view('/templates/pagamentoavencer',$this->data,true);
		            $titulo = "[WDM-Controller] Lembrete de pagamento!"; 
		            $resultado = $this->sendmail($view,$titulo,$email);
		            //var_dump($view);
		        }	              
	        }
	    }       
    }

    public function pagamentoRealizado()
    {           
        
        foreach (unserialize(SGBD) as $c) {

        $this->data['pagamento']  = $this->Auth_model->pagamentoRealizado($c);  
   
	        // Só entrar para enviar email se tiver pagamento realizado e com situação envio = 0
	        if ($this->data['pagamento']) {
	        	$email = $this->data['pagamento'][0]->email_cron;	
	            $view   = $this->load->view('/templates/pagamentorealizado',$this->data,true);
	            $titulo = "[WDM-Controller] Confirmação de pagamento!"; 
	            $resultado = $this->sendmail($view,$titulo,$email);
	            if ($resultado) {
	                $id = $this->data['pagamento'][0]->pagamento_sistema_id;
	                $atualizado = $this->Auth_model->updatePagamento($id,$c); // Update para não enviar novamente o mesmo e-mail.
	            } 
	        	//var_dump($view);             
	        } 
	    }     
    }


    public function resumoIndicador()
    {   


        foreach (unserialize(SGBD) as $c) {
			
           // $c = 'wdmcon46_megafios_megafios';
			
			
			if(($c <> 'wdmcon46_carvalhal_ouvidoria') OR ($c <> 'wdmcon46_nudep_nudep')){
				//var_dump($c);die();	
				
				# Vendas
				$this->data['indicador_venda_dia']         = $this->Auth_model->getIndicadorVendaDia($c);
				$this->data['indicador_margem_lucro']      = $this->Auth_model->getIndicadorMargemLucro($c);
				$this->data['indicador_cliente_atendido']  = $this->Auth_model->getIndicadorClienteAtendido($c);
				$this->data['indicador_vendedor_dia']      = $this->Auth_model->getIndicadorVendedorDia($c);
				$this->data['indicador_vendido_mes_atual'] = $this->Auth_model->getIndicadorVendidoMesAtual($c);

				# estoque
				$this->data['indicador_estoque_baixo']   = $this->Auth_model->getIndicadorEstoqueBaixo($c);
				$this->data['indicador_reposicao']       = $this->Auth_model->getIndicadorReposicao($c);
				// PARA RECEBER HOJE
				// PARA ENTREGAR HOJE            
				$this->data['indicador_comprado_mes_atual']  = $this->Auth_model->getIndicadorCompradoMesAtual($c);

				# financeiro
				$this->data['indicador_saldo_inicial']   = $this->Auth_model->getIndicadorSaldoInicial($c);
				$this->data['indicador_recebimento']     = $this->Auth_model->getIndicadorRecebimento($c);
				$this->data['indicador_pagamento']       = $this->Auth_model->getIndicadorPagamento($c);

				$this->data['indicador_receber_amanha']  = $this->Auth_model->getIndicadorReceberAmanha($c);
				$this->data['indicador_pagar_amanha']    = $this->Auth_model->getIndicadorPagarAmanha($c);



				$this->data['emitente'] = $this->Auth_model->getEmitente($c);


				// var_dump( $this->data['emitente']->email_cron);die();

				if ($this->data['emitente']->email_cron) {
					$email = $this->data['emitente']->email_cron; 
					$view  = $this->load->view('/templates/resumoindicadores',$this->data,true);            
					$titulo = "[WDM-Controller] Resumo indicadores do dia ".(date('y-m-d'))."!"; 

				   $resultado = $this->sendmail($view,$titulo,$email);
				   //var_dump($view);             
				} 
          
			}	
			// continue;
			// die('aqui irá enviar o resumo financeiro para o cliente com gráficos e estatísticas');
        } 
    } 

    public function resumoFinanceiroMes()
    {   
       // foreach (unserialize(SGBD) as $c) {

            $c = 'wdm_quitandaorg_quitandaorg';

            $this->data['estatisticas_financeiro'] = $this->Auth_model->getEstatisticasFinanceiroGeral($c);

            // var_dump( $this->data['estatisticas_financeiro']->email_cron);die();

            if ($this->data['estatisticas_financeiro']) {
                $email = $this->data['estatisticas_financeiro']->email_cron; 
                $view  = $this->load->view('/templates/resumofinanceiro2',$this->data,true);            
                $titulo = "[WDM-Controller] Resumo financeiro do mês ".(date('m')-1)."!"; 

                //$resultado = $this->sendmail($view,$titulo,$email);
                var_dump($view);             
            } 
          
            die('aqui irá enviar o resumo financeiro para o cliente com gráficos e estatísticas');
      //  } 
    }    

    public function resumoFinanceiroGeral()
    {            
            die('aqui irá enviar o resumo financeiro para o cliente com gráficos e estatísticas de todo o sistema');
    } 
    
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */