<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cliente_model');
	}

	public function top10Mes()
	{
		
		$dadosView['dados'] = $this->Cliente_model->top10Mes();
		$dadosView['meio']  = 'cliente/top10Mes';
		$this->load->view('tema/tema',$dadosView);
	
	}

	public function top10MesDetalhe($loja)
	{
		$dadosView['dados'] = $this->Cliente_model->top10MesDetalhe($loja);
		$dadosView['meio']  = 'cliente/top10MesDetalhe';
		$this->load->view('tema/tema',$dadosView);
	
	}



	public function top10Ano()
	{		
		
		$dadosView['dados'] = $this->Cliente_model->top10Ano();
		$dadosView['meio']  = 'cliente/top10Ano';
		$this->load->view('tema/tema',$dadosView);
	
	}

	public function top10AnoDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Cliente_model->top10AnoDetalhe($loja);
		$dadosView['meio']  = 'cliente/top10AnoDetalhe';
		$this->load->view('tema/tema',$dadosView);
	
	}


	public function top10AnoAnterior()
	{		
		
		$dadosView['dados'] = $this->Cliente_model->top10AnoAnterior();
		$dadosView['meio']  = 'cliente/top10AnoAnterior';
		$this->load->view('tema/tema',$dadosView);
	
	}


	public function top10AnoAnteriorDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Cliente_model->top10AnoAnteriorDetalhe($loja);
		$dadosView['meio']  = 'cliente/top10AnoAnteriorDetalhe';
		$this->load->view('tema/tema',$dadosView);
	
	}




    public function pagamentoAberto()
	{
		$dadosView['dados'] = $this->Cliente_model->pagamentoAberto();
		$dadosView['meio']  = 'cliente/pagamentoAberto';
		$this->load->view('tema/tema',$dadosView);
	}


	public function pagamentoAbertoDetalhe($loja)
	{
		$dadosView['dados'] = $this->Cliente_model->pagamentoAbertoDetalhe($loja);

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'cliente/pagamentoAbertoDetalhe';
		$this->load->view('tema/tema',$dadosView);
	}

	public function detalhamentoPago($loja, $id, $valorAberto = null)
	{
		$dadosPago = $this->Cliente_model->detalhamentoPago($loja,$id);

		// $dadosView['valorAberto'] = $valorAberto;

		$dadosAberto = $this->Cliente_model->detalhamentoAberto($loja,$id);

		$dadosCompra = $this->Cliente_model->detalhamentoCompra($loja,$id);


		foreach ($dadosPago as $pago) {

			foreach ($dadosCompra as $compra) {
				// code...
				if (($pago->mes == $compra->mes) and ($pago->ano == $compra->ano) ) {
					// code...
					$pago->totalCompra = $compra->totalCompra; 
					break;
				}else{
					$pago->totalCompra = 0;
				}
			}


			foreach ($dadosAberto as $aberto) {
				// code...
				if (($pago->mes == $aberto->mes) and ($pago->ano == $aberto->ano) ) {
					// code...
					$pago->totalAberto = $aberto->totalAberto; 
					break;
				}else{
					$pago->totalAberto = 0;
				}
			}

		}


		$cliente = array(
			'loja' => $loja,
			'cliente_id' => $id 
		);

		$dadosView['cliente'] = $cliente;

		$dadosView['dados'] = $dadosPago;

		

		// var_dump($dadosPago);die();

		$dadosView['meio']  = 'cliente/detalhamentoPago';
		$this->load->view('tema/tema',$dadosView);
	}


	public function detalhamentoDiasPago($loja, $id, $ano, $mes)
	{
		$totalPago = 0;
		$dias   =  $this->diasJaPassaram($mes, $ano); 

		$dados  = $this->Cliente_model->detalhamentoDiasPago($loja, $id, $ano, $mes);

		foreach ($dados as $v) {
			$totalPago = $totalPago + $v->valor_baixa;
		}

		$media = $totalPago / $dias; 


		$dadosView['dados']  =  $dados;

		$dadosView['resutado'] = array(
			'dias'      => $dias,
			'totalpago' => $totalPago,
			'media'     => $media
	    ); 


		//var_dump($media);die();

		$dadosView['meio']  = 'cliente/detalhamentoDiasPago';
		$this->load->view('tema/tema',$dadosView);
	}


	function diasJaPassaram($mes, $ano) {

	    // Verifica se os parâmetros são válidos
	    if (!is_numeric($mes) || !is_numeric($ano) || $mes < 1 || $mes > 12 || $ano < 1) {
	        return "Parâmetros inválidos. Insira um mês (1 a 12) e um ano válido.";
	    }

	    // Obtém o mês e ano atual
	    $mesAtual = (int)date('n');
	    $anoAtual = (int)date('Y');
	    $diaAtual = (int)date('j');

	    // Verifica se o mês e ano são superiores ao atual
	    if ($ano > $anoAtual || ($ano == $anoAtual && $mes > $mesAtual)) {
	        return 0;
	    }

	    // Se for o mês e ano atual, retorna os dias até hoje
	    if ($ano == $anoAtual && $mes == $mesAtual) {
	        return $diaAtual;
	    }

	    // Caso contrário, calcula os dias do mês inteiro
	    return cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
	}


	
}

/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */