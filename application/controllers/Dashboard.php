<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');
	}

	public function index()
	{

		$data['dadosFinanceiroReceitaDia']   = $this->Dashboard_model->resumoFinanceiroReceitaDia();
		$data['resumoFinanceiroCompraDia']   = $this->Dashboard_model->resumoFinanceiroCompraDia();
		$data['resumoFinanceiroDespesaDia']  = $this->Dashboard_model->resumoFinanceiroDespesaDia();
		

		$data['dadosFinanceiro']  = $this->Dashboard_model->resumoFinanceiro();
		$data['dadosFinanceiroDia']   = $this->Dashboard_model->resumoFinanceiroDia();
		// v($data['dadosFinanceiro']);


		$data['meio'] = 'dashboard/listar';
		$this->load->view('tema/tema',$data);


	}



	public function notificacoes()
	{

		$dados['resultadoTce']  = $this->Dashboard_model->tcePendente();
		$dados['resultadoTa']   = $this->Dashboard_model->taPendente();


		$resposta['ta'] = $dados['resultadoTa'][0]->total;
		$resposta['tce'] = $dados['resultadoTce'][0]->total;


		echo json_encode($resposta);

	}





	public function carregarnoticias()

	{

		$xml = simplexml_load_file("http://g1.globo.com/dynamo/pernambuco/educacao/rss2.xml");

		$quant = 4;
		$noticias = array();


		for($i=0;$i<$quant;$i++) 
		{

			$hora                       = explode(" ",$xml->channel->item[$i]->pubDate);

			$noticias[$i]['titulo']     = $xml->channel->item[$i]->title;
			$noticias[$i]['link']       = $xml->channel->item[$i]->link;
			$noticias[$i]['dia']       = $hora[1];
			$noticias[$i]['mes']       = $hora[2];
			$fimimg                    = strpos($xml->channel->item[$i]->description, "<br />");
			$semimg                    = substr($xml->channel->item[$i]->description, 6 + $fimimg);
			//var_dump($novo);die();
			$noticias[$i]['descricao']  = $semimg;	

		}
 

	    return $noticias;

	}


	public function sistemaOpenClose()
	{	

		$resultado = $this->Dashboard_model->sistemaOpenClose();

		if ($resultado[0]->emitente_botao_panico == '0') {	// Loja Aberta		
			//$this->session->set_flashdata('sucesso', 'Deseja fechar loja?');
			echo json_encode(array('result'=> 0)); 
		}else{
			//$this->session->set_flashdata('erro', 'Deseja realmente abrir a loja?');
			echo json_encode(array('result'=> 1));
		}

		// redirect('Dashboard', 'refresh');
	}


	public function desativarLoja()
	{

		$sql = "UPDATE `mixcel17_megacell_matriz`.`emitente` SET `emitente_botao_panico` = '1'  "; 
            $this->db->query($sql);   

	}


	public function ativarLoja()
	{

		$sql = "UPDATE `mixcel17_megacell_matriz`.`emitente` SET `emitente_botao_panico` = '0'  "; 
            $this->db->query($sql);   

	}




	public function validacaoGerente()
	{
		$senha = sha1(md5(strtolower($this->input->post('senha'))));
  
		if($this->Dashboard_model->validacaoGerente($senha) == true){ 
  
			echo json_encode(array('result'=> true));
		}else{
			echo json_encode(array('result'=> false));
		} 
	 }







}



/* End of file Dashboard.php */

/* Location: ./application/controllers/Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */