<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faturamento extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Faturamento_model');
	}


	public function previsaoFinanceiro()
	{
		// $dadosView['dados'] = $this->Faturamento_model->previsaoFinanceiro();
		$dadosView['meio']  = 'faturamento/listarPrevisaoNew';
		$this->load->view('tema/tema',$dadosView);
	
	}

	public function previsaoFinanceiroDetalhe($dia)
	{			
		$dadosView['dia']   = $dia;
		$dadosView['dados'] = $this->Faturamento_model->previsaoFinanceiroDetalhe();
		$dadosView['meio']  = 'faturamento/listarPrevisaoDetalheNew';
		$this->load->view('tema/tema',$dadosView);
	
	}



	public function previsaoFinanceiroAno()
	{
	
		$dadosView['dados'] = $this->Faturamento_model->previsaoFinanceiroAno();
		$dadosView['meio']  = 'faturamento/listarPrevisaoAno';
		$this->load->view('tema/tema',$dadosView);
	
	}


	public function previsaoFinanceiroDetalheAno($loja)
	{
	
		$dadosView['dados'] = $this->Faturamento_model->previsaoFinanceiroDetalheAno($loja);
		$dadosView['meio']  = 'faturamento/listarPrevisaoDetalheAno';
		$this->load->view('tema/tema',$dadosView);
	
	}

	
}

/* End of file Faturamento.php */
/* Location: ./application/controllers/Faturamento.php */