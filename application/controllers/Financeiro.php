<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class financeiro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Financeiro_model');
	}

	public function financeiroDia()
	{
		
		$dadosView['dados'] = $this->Financeiro_model->financeiroDia();
		
		$dadosView['meio']  = 'financeiro/listarfinanceirodia';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function financeiroMes()
	{
		$dadosView['dados'] = $this->Financeiro_model->financeiroMes();

		$dadosView['meio']  = 'financeiro/listarfinanceiromes';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function financeiroAno()
	{
		$dadosView['dados']      = $this->Financeiro_model->financeiroAno();
		
		$dadosView['meio']       = 'financeiro/listarfinanceiroano';
		$this->load->view('tema/tema',$dadosView);	
	}




	public function financeiroDiaDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Financeiro_model->financeiroDiaDetalhe(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'financeiro/listarfinanceirodiadetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function financeiroMesDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Financeiro_model->financeiroMesDetalhe(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'financeiro/listarfinanceiromesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function financeiroAnoDetalhe($loja)
	{	
			
		$dadosView['dados'] = $this->Financeiro_model->financeiroAnoDetalhe(strtolower($loja));
		
		$dadosView['meio']  = 'financeiro/listarfinanceiroanodetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}

	
}

/* End of file Financeiro.php */
/* Location: ./application/controllers/Financeiro.php */