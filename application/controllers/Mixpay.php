<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mixpay  extends CI_Controller {
	

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mixpay_model');
	}

	public function teste()
	{
		$dadosView['dados'] = $this->Mixpay_model->listarQtd();

		//var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'venda/listarvendasdia';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function operacaoDiaAnterior()
	{
		$dadosView['dados'] = $this->Mixpay_model->operacaoDiaAnterior();
			$dadosView['despesa'] = $this->Mixpay_model->despesa('dia');
		$dadosView['sobra']       = $this->Mixpay_model->sobra('dia');
		
		$dadosView['meio']  = 'mixpay/listaroperacaodiaanterior';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function operacaoMesAnterior()
	{
		$dadosView['dados']   = $this->Mixpay_model->operacaoMesAnt();
		$dadosView['despesa'] = $this->Mixpay_model->despesa('mesAnt');
		$dadosView['sobra']   = $this->Mixpay_model->sobra('mesAnt');

		// var_dump($dadosView['despesa']);die();

		$dadosView['meio']  = 'mixpay/listaroperacaomesant';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function operacaoMes()
	{
		$dadosView['dados']   = $this->Mixpay_model->operacaoMes();
		$dadosView['despesa'] = $this->Mixpay_model->despesa('mes');
		$dadosView['sobra']   = $this->Mixpay_model->sobra('mes');

		// var_dump($dadosView['despesa']);die();

		$dadosView['meio']  = 'mixpay/listaroperacaomes';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function operacaoAno()
	{
		$dadosView['dados']    = $this->Mixpay_model->operacaoAno();
	    $dadosView['despesa']  = $this->Mixpay_model->despesa('ano');
		$dadosView['sobra']    = $this->Mixpay_model->sobra('ano');

		$dadosView['meio']       = 'mixpay/listaroperacaoano';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function operacaoAnoAnterior()
	{
		$dadosView['dados']    = $this->Mixpay_model->operacaoAnoAnterior();
		$dadosView['despesa']  = $this->Mixpay_model->despesa('ano');
		$dadosView['sobra']    = $this->Mixpay_model->sobra('ano');
		
		$dadosView['meio']       = 'mixpay/listarvendasanoanterior';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function previsaoFinanceiro()
	{	
		$dadosView['meio']  = 'mixpay/listarPrevisao';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function previsaoFinanceiroDetalhe($dia)
	{			
		$dadosView['dia']   = $dia;
		$dadosView['dados'] = $this->Mixpay_model->previsaoFinanceiroDetalhe();


		//var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'mixpay/listarPrevisaoDetalhe';
		$this->load->view('tema/tema',$dadosView);
	
	}




	
}

/* End of file Financeiro.php */
/* Location: ./application/controllers/Financeiro.php */