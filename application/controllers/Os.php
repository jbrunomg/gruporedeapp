<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class os extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Os_model');
	}

	public function osDia()
	{
		
		$dadosView['dados'] = $this->Os_model->osDia();
		
		$dadosView['meio']  = 'os/listarosdia';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function osMes()
	{
		$dadosView['dados'] = $this->Os_model->osMes();

		$dadosView['meio']  = 'os/listarosmes';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function osAno()
	{
		$dadosView['dados']      = $this->Os_model->osAno();		

		$dadosView['meio']       = 'os/listarosano';
		$this->load->view('tema/tema',$dadosView);	
	}













	public function osMesDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Os_model->osMesDetalhe(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'os/listarosmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function osMesDetalheAnoAnterior($loja)
	{
		
		$dadosView['dados'] = $this->Os_model->osMesDetalheAnoAnterior(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'os/listarosmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function osGrupo()
	{	

		$loja = $this->uri->segment(3);	
		$ano  = $this->uri->segment(4);		
		$mes  = $this->uri->segment(5);	
		$dia  = $this->uri->segment(6);		


		$dadosView['detalhe'] = array(
			'loja' => $loja,
			'ano'  => $ano,
			'mes'  => $mes,
			'dia'  => $dia
			
		);

		// var_dump($mes);die();	

        $dadosView['dados'] = $this->Os_model->VendasMensalGrupo($loja, $ano, $mes, $dia);
		

	    $dadosView['meio']  = 'os/listarvendasgrupo';
		$this->load->view('tema/tema',$dadosView);	

	}


	public function vendasAnoAnterior()
	{
		$dadosView['dados']      = $this->Os_model->vendasAnoAnterior();
		
		$dadosView['meio']       = 'os/listarvendasanoanterior';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasAnoDetalhe($loja)
	{	
		var_dump($loja);die();

		$dadosView['dados'] = $this->Os_model->vendasAnoDetalhe(strtolower($loja));

		var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'os/listarvendasanodetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function emAberto($dias = 1)
	{
		$dadosView['dados'] = $this->Os_model->emAberto($dias);
		$dadosView['meio']  = 'os/emAberto';

		$this->load->view('tema/tema', $dadosView);
	}

	public function emAbertoDetalhe($loja, $dias = 1)
	{
		$dadosView['dados'] = $this->Os_model->emAbertoDetalhe($loja, $dias);
		$dadosView['meio']  = 'os/emAbertoDetalhe';

		$this->load->view('tema/tema',$dadosView);
	}

	public function quantidadesOs()
	{
		echo json_encode($this->Os_model->quantidadesOs([7, 12, 16, 21, 30]));
	}
}

/* End of file Os.php */
/* Location: ./application/controllers/Os.php */