<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Produto extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

		$this->load->model('Produto_model');

	}



	public function valorEstoque()

	{

		$dadosView['dados'] = $this->Produto_model->valorEstoque();

		$dadosView['meio']  = 'produto/valorEstoque';

		$this->load->view('tema/tema',$dadosView);

	

	}



	// Detalhe do valorEstoque

	public function valorEstoqueGrupo($loja)
	{		

		$dadosView['dados'] = $this->Produto_model->valorEstoqueGrupo($loja);
		// v($dadosView['dados']);

		$dadosView['meio']  = 'produto/valorEstoqueGrupo';
		$this->load->view('tema/tema',$dadosView);
	

	}


	public function valorEstoqueGrupoDetalhe()
	{		
		$loja  = $this->uri->segment(3);
		$grupo = $this->uri->segment(4);
		
		// var_dump($grupo);die();

		$dadosView['dados'] = $this->Produto_model->valorEstoqueGrupoDetalhe($loja,urldecode($grupo));
		// v($dadosView['dados']);

		$dadosView['meio']  = 'produto/valorEstoqueGrupoDetalhe';
		$this->load->view('tema/tema',$dadosView);
	}	



	public function maisVendido()
	{

	    // die('Aqui');
	    $dadosView['dados'] = $this->Produto_model->maisVendido();
		$dadosView['meio']  = 'produto/maisVendido';
		$this->load->view('tema/tema',$dadosView);	

	}



	// detalhe do maisVendido

	public function maisVendidoDetalhe($loja)
	{
	
		$dadosView['dados'] = $this->Produto_model->maisVendidoDetalhe($loja);
		$dadosView['meio']  = 'produto/maisVendidoDetalhe';
		$this->load->view('tema/tema',$dadosView);


	}



	public function maisVendidoT()
	{    

	    $dadosView['dados'] = $this->Produto_model->maisVendidoT();
		$dadosView['meio']  = 'produto/maisVendidoT';
		$this->load->view('tema/tema',$dadosView);	

	}



	// detalhe do maisVendidoT

	public function maisVendidoDetalheT($loja)
	{
	
		$dadosView['dados'] = $this->Produto_model->maisVendidoDetalheT($loja);
		$dadosView['meio']  = 'produto/maisVendidoDetalheT';
		$this->load->view('tema/tema',$dadosView);
	

	}



	public function maisVendidoS()
	{    

	    $dadosView['dados'] = $this->Produto_model->maisVendidoS();
		$dadosView['meio']  = 'produto/maisVendidoS';
		$this->load->view('tema/tema',$dadosView);	

	}





	// detalhe do maisVendidoS

	public function maisVendidoDetalheS($loja)

	{

		

		$dadosView['dados'] = $this->Produto_model->maisVendidoDetalheS($loja);

		$dadosView['meio']  = 'produto/maisVendidoDetalheS';

		$this->load->view('tema/tema',$dadosView);

	

	}









	public function maisParado()

	{

		

		$dadosView['dados'] = $this->Produto_model->maisParado();

		$dadosView['meio']  = 'produto/maisParado';

		$this->load->view('tema/tema',$dadosView);

	

	}





	public function maisParadoDetalhe($loja)

	{

		$dadosView['dados'] = $this->Produto_model->maisParadoDetalhe($loja);

		$dadosView['meio']  = 'produto/maisParadoDetalhe';

		$this->load->view('tema/tema',$dadosView);

	

	}





	public function maisParadoT()

	{

		

		$dadosView['dados'] = $this->Produto_model->maisParadoT();

		$dadosView['meio']  = 'produto/maisParadoT';

		$this->load->view('tema/tema',$dadosView);

	

	}





	public function maisParadoDetalheT($loja)

	{

		$dadosView['dados'] = $this->Produto_model->maisParadoDetalheT($loja);

		$dadosView['meio']  = 'produto/maisParadoDetalheT';

		$this->load->view('tema/tema',$dadosView);

	

	}





	public function maisParadoS()

	{

		

		$dadosView['dados'] = $this->Produto_model->maisParadoS();

		$dadosView['meio']  = 'produto/maisParadoS';

		$this->load->view('tema/tema',$dadosView);

	

	}





	public function maisParadoDetalheS($loja)

	{

		$dadosView['dados'] = $this->Produto_model->maisParadoDetalheS($loja);

		$dadosView['meio']  = 'produto/maisParadoDetalheS';

		$this->load->view('tema/tema',$dadosView);

	

	}



	
	public function maisParadoGenerico($dias = 1)
	{
		$dadosView['dados'] = $this->Produto_model->maisParadoGenerico($dias);
		$dadosView['meio']  = 'produto/maisParadoGenerico';

		$this->load->view('tema/tema', $dadosView);
	}

	public function maisParadoGenericoDetalhe($loja, $dias = 1)
	{
		$dadosView['dados'] = $this->Produto_model->maisParadoGenericoDetalhe($loja, $dias);
		$dadosView['meio']  = 'produto/maisParadoGenericoDetalhe';

		$this->load->view('tema/tema', $dadosView);
	}

	public function quantidadesProdutosParados()
	{
		echo json_encode($this->Produto_model->quantidadesProdutosParados([7, 12, 16, 21, 30, 90, 180]));
	}
}



/* End of file Produto.php */

/* Location: ./application/controllers/Produto.php */