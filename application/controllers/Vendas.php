<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vendas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Venda_model');
	}

	public function vendasDia()
	{
		$dadosView['dados'] = $this->Venda_model->vendasDia();
		
		$dadosView['meio']  = 'venda/listarvendasdia';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function vendasMes()
	{
		$dadosView['dados'] = $this->Venda_model->vendasMes();

		$dadosView['meio']  = 'venda/listarvendasmes';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function vendasMesDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Venda_model->vendasMesDetalhe(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'venda/listarvendasmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function clientesMesDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Venda_model->clientesMesDetalhe(strtolower($loja));

		// var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'venda/listarclientesmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}



	public function vendasGrupoDetalhe()
	{	
		$grupo = $this->uri->segment(3);	
		$loja  = $this->uri->segment(4);	
		$ano   = $this->uri->segment(5);		
		$mes   = $this->uri->segment(6);	
		$dia   = $this->uri->segment(7);		


		$dadosView['detalhe'] = array(
			'grupo' => $grupo,
			'loja'  => $loja,
			'ano'   => $ano,
			'mes'   => $mes,
			'dia'   => $dia
			
		);

        $dadosView['dados'] = $this->Venda_model->vendasGrupoDetalhe($grupo, $loja, $ano, $mes, $dia);

        $total = 0;

        foreach ($dadosView['dados'] as $value) {

        	$total += $value->qtd;
    
        }

        $dadosView['total'] = $total;
		
        

	    $dadosView['meio']  = 'venda/listarvendasgrupodetalhe';
		$this->load->view('tema/tema',$dadosView);	

	}


	public function vendasMesDetalheAnoAnterior($loja)
	{
		
		$dadosView['dados'] = $this->Venda_model->vendasMesDetalheAnoAnterior(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'venda/listarvendasmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasGrupo()
	{	

		$loja = $this->uri->segment(3);	
		$ano  = $this->uri->segment(4);		
		$mes  = $this->uri->segment(5);	
		$dia  = $this->uri->segment(6);		


		$dadosView['detalhe'] = array(
			'loja' => $loja,
			'ano'  => $ano,
			'mes'  => $mes,
			'dia'  => $dia
			
		);

			

        $dadosView['dados'] = $this->Venda_model->VendasMensalGrupo($loja, $ano, $mes, $dia);
		
        //var_dump($dadosView['dados']);die();

	    $dadosView['meio']  = 'venda/listarvendasgrupo';
		$this->load->view('tema/tema',$dadosView);	

	}


	public function clienteGrupo()
	{	

		$loja = $this->uri->segment(3);	
		$ano  = $this->uri->segment(4);		
		$mes  = $this->uri->segment(5);	
		$dia  = $this->uri->segment(6);	

		$dadosView['detalhe'] = array(
			'loja' => $loja,
			'ano'  => $ano,
			'mes'  => $mes,
			'dia'  => $dia
			
		);			

        $dadosView['dados'] = $this->Venda_model->ClientesMensalGrupo($loja, $ano, $mes, $dia);
		
       // var_dump($dadosView['dados']);die();

	    $dadosView['meio']  = 'venda/listarclientesgrupo';
		$this->load->view('tema/tema',$dadosView);	

	}



	public function vendasAno()
	{
		$dadosView['dados']      = $this->Venda_model->vendasAno();
		//$dadosView['financeiro'] = $this->Venda_model->financeiroAno();


		$dadosView['meio']       = 'venda/listarvendasano';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasAnoAnterior()
	{
		$dadosView['dados']      = $this->Venda_model->vendasAnoAnterior();
		
		$dadosView['meio']       = 'venda/listarvendasanoanterior';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasAnoDetalhe($loja)
	{	
		//var_dump($loja);die();

		$dadosView['dados'] = $this->Venda_model->vendasAnoDetalhe(strtolower($loja));

		// var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'venda/listarvendasanodetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}

	
}

/* End of file Financeiro.php */
/* Location: ./application/controllers/Financeiro.php */