<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vendascell extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Vendacell_model');
	}

	public function vendasDia()
	{
		$dadosView['dados'] = $this->Vendacell_model->vendasDia();

		// var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'vendacell/listarvendasdia';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function vendasMes()
	{
		$dadosView['dados'] = $this->Vendacell_model->vendasMes();

		$dadosView['meio']  = 'vendacell/listarvendasmes';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasAno()
	{
		$dadosView['dados']      = $this->Vendacell_model->vendasAno();
		//$dadosView['financeiro'] = $this->Vendacell_model->financeiroAno();


		$dadosView['meio']       = 'vendacell/listarvendasano';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasAnoAnterior()
	{
		$dadosView['dados']      = $this->Vendacell_model->vendasAnoAnterior();
		
		$dadosView['meio']       = 'vendacell/listarvendasanoanterior';
		$this->load->view('tema/tema',$dadosView);	
	}

	public function vendasMesDetalhe($loja)
	{
		
		$dadosView['dados'] = $this->Vendacell_model->vendasMesDetalhe(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'vendacell/listarvendasmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}
















	public function vendasGrupoDetalhe()
	{	
		$grupo = $this->uri->segment(3);	
		$loja  = $this->uri->segment(4);	
		$ano   = $this->uri->segment(5);		
		$mes   = $this->uri->segment(6);	
		$dia   = $this->uri->segment(7);		


		$dadosView['detalhe'] = array(
			'grupo' => $grupo,
			'loja'  => $loja,
			'ano'   => $ano,
			'mes'   => $mes,
			'dia'   => $dia
			
		);

        $dadosView['dados'] = $this->Vendacell_model->vendasGrupoDetalhe($grupo, $loja, $ano, $mes, $dia);
		
       // var_dump($dadosView['dados']);die();

	    $dadosView['meio']  = 'vendacell/listarvendasgrupodetalhe';
		$this->load->view('tema/tema',$dadosView);	

	}


	public function vendasMesDetalheAnoAnterior($loja)
	{
		
		$dadosView['dados'] = $this->Vendacell_model->vendasMesDetalheAnoAnterior(strtolower($loja));

		//var_dump($dadosView['dados']);die();

		$dadosView['meio']  = 'vendacell/listarvendasmesdetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}


	public function vendasGrupo()
	{	

		$loja = $this->uri->segment(3);	
		$ano  = $this->uri->segment(4);		
		$mes  = $this->uri->segment(5);	
		$dia  = $this->uri->segment(6);		


		$dadosView['detalhe'] = array(
			'loja' => $loja,
			'ano'  => $ano,
			'mes'  => $mes,
			'dia'  => $dia
			
		);

			

        $dadosView['dados'] = $this->Vendacell_model->VendasMensalGrupo($loja, $ano, $mes, $dia);
		
        //var_dump($dadosView['dados']);die();

	    $dadosView['meio']  = 'vendacell/listarvendasgrupo';
		$this->load->view('tema/tema',$dadosView);	

	}






	public function vendasAnoDetalhe($loja)
	{	
		//var_dump($loja);die();

		$dadosView['dados'] = $this->Vendacell_model->vendasAnoDetalhe(strtolower($loja));

		// var_dump($dadosView['dados']);die();
		
		$dadosView['meio']  = 'vendacell/listarvendasanodetalhe';
		$this->load->view('tema/tema',$dadosView);	
	}

	
}

/* End of file Financeiro.php */
/* Location: ./application/controllers/Financeiro.php */