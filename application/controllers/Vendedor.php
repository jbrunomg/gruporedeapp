<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Vendedor extends CI_Controller {



	public function __construct()
	{

		parent::__construct();
		$this->load->model('Vendedor_model');

	}



	public function top10Dia()
	{
        $dadosView['dados'] = $this->Vendedor_model->top10Dia();
		$dadosView['meio']  = 'vendedor/top10Dia';
		$this->load->view('tema/tema',$dadosView);	

	}



	public function top10DiaDetalhe($loja)
	{

		$dadosView['dados'] =  $this->Vendedor_model->top10DiaDetalhe($loja);
		$dadosView['meio']  = 'vendedor/top10DiaDetalhe';
		$this->load->view('tema/tema',$dadosView);	

	}



	public function top10Mes()
	{
		
		$dadosView['dados'] = $this->Vendedor_model->top10Mes();
		$dadosView['meio']  = 'vendedor/top10Mes';
		$this->load->view('tema/tema',$dadosView);	

	}



	public function top10MesDetalhe($loja)
	{

		$dadosView['dados'] = $this->Vendedor_model->top10MesDetalhe($loja);
		$dadosView['meio']  = 'vendedor/top10MesDetalhe';
		$this->load->view('tema/tema',$dadosView);


	}


	public function top10Ano()
	{
		$dadosView['dados'] = $this->Vendedor_model->top10Ano();
		$dadosView['meio']  = 'vendedor/top10Ano';
		$this->load->view('tema/tema',$dadosView);

	}



	public function top10AnoDetalhe($loja)
	{

		$dadosView['dados'] = $this->Vendedor_model->top10AnoDetalhe($loja);
		$dadosView['meio']  = 'vendedor/top10AnoDetalhe';
		$this->load->view('tema/tema',$dadosView);	

	}

	

}


/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */