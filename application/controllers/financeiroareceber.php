<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiroareceber extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Financeiro_model');
	}

	public function index()
	{
		$resultado = $this->Financeiro_model->contasaReceber();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'financeiro/listaraReceber';
		$this->load->view('tema/tema',$dadosView);
	
	}



	
}

/* End of file Financeiro.php */
/* Location: ./application/controllers/Financeiro.php */