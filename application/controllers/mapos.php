<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mapos_model','',TRUE);
        
    }

   
    public function do_upload(){
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','Você não tem permissão para configurar emitente.');
           redirect(base_url());
        }

        $this->load->library('upload');

        $image_upload_folder = FCPATH . 'assets/uploads';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }

        $this->upload_config = array(
            'upload_path'   => $image_upload_folder,
            'allowed_types' => 'png|jpg|jpeg|bmp',
            'max_size'      => 2048,
            'remove_space'  => TRUE,
            'encrypt_name'  => TRUE,
        );

        $this->upload->initialize($this->upload_config);

        if (!$this->upload->do_upload()) {
            $upload_error = $this->upload->display_errors();
            print_r($upload_error);
            exit();
        } else {
            $file_info = array($this->upload->data());
            return $file_info[0]['file_name'];
        }

    }

    public function get_req($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
    }



    public function sendmail($view,$titulo)
    {               

        $emitente = $this->mapos_model->getEmitente();
       // var_dump($view);
       // $result = $this->load->view($view,'',TRUE);

        $this->load->library('email');
        $this->email->from('emailsite@wdmtecnologia.com.br', 'WDM-Controller');
        //$this->email->to($emitente[0]->email);
        $this->email->to('bruno@wdmtecnologia.com.br');
        //$this->email->cc('bruno@wdmtecnologia.com.br'); 
        $this->email->subject($titulo);
       // $this->email->message($result);
        $this->email->message($view);
        //$this->email->attach('/path/to/file1.png');
        //$this->email->attach('/path/to/file2.pdf');
        if ($this->email->send()){
            $this->session->set_flashdata('success','Email enviado com sucesso.');
            return true;
        }
        else{
            $this->session->set_flashdata('error','Erro ao tentar enviar o email');
            return false;
        }
    }


    public function boasVindas()
    {        
        $view = '/templates/boasvindas';

        $titulo = "[WDM-Controller] Seja bem vindo ao WDM-Controller";        
        $resultado = $this->load->view($view,'',TRUE); 
        $resultado = $this->sendmail($resultado,$titulo);
        //var_dump($resultado);
    }

    public function checkList()
    {        
        $view = '/templates/checklistauto';

        $titulo = "[WDM-Controller] Confirmação de pagamento!";        
        //$resultado = $this->sendmail($view,$titulo);        
        $resultado = $this->load->view($view,'',TRUE); 
        var_dump($resultado);
    }

    public function pagamentoAtraso()
    {        
        
        $this->load->model('pagamentosistema_model');
        
        $this->data['pagamento']  = $this->pagamentosistema_model->pagamentoAtraso(); 

        // Só entrar se tiver pagamento em atraso;
        if ($this->data['pagamento']) {           
        $num = $this->data['pagamento'][0]->dias_vencido;
            // Só envia se tiver com 05 dias em atraso;     
            // é múltiplo de 5!
            if (($num % 5) == 0) {        
                $view = $this->load->view('/templates/pagamentoatraso',$this->data,true);
                $titulo = "[WDM-Controller] Notificação de atraso!"; 
                $resultado = $this->sendmail($view,$titulo);
                //var_dump($view);           
            }       
        }
    } 

    public function pagamentoaVencer()
    {        
        
        $this->load->model('pagamentosistema_model');
        
        $this->data['pagamento']  = $this->pagamentosistema_model->pagamentoaVencer(); 

        // Só entrar com dois dias a vencer;
        if ($this->data['pagamento']) {           
        $num = $this->data['pagamento'][0]->dias_vencido;
        // Só envia se tiver com 02 dias a vencer     
        if ($num == 2) {        
            $view = $this->load->view('/templates/pagamentoavencer',$this->data,true);
            $titulo = "[WDM-Controller] Lembrete de pagamento!"; 
            $resultado = $this->sendmail($view,$titulo);
            //var_dump($view);
        }
              
        } 
        
    }

    public function pagamentoRealizado()
    {    
      
        $this->load->model('pagamentosistema_model');
        
        $this->data['pagamento']  = $this->pagamentosistema_model->pagamentoRealizado();  
   
        // Só entrar para enviar email se tiver pagamento realizado e com situação envio = 0
        if ($this->data['pagamento']) {
            $view   = $this->load->view('/templates/pagamentorealizado',$this->data,true);
            $titulo = "[WDM-Controller] Confirmação de pagamento!"; 
            $resultado = $this->sendmail($view,$titulo);
            if ($resultado) {
                $id = $this->data['pagamento'][0]->pagamento_sistema_id;
                $atualizado = $this->pagamentosistema_model->updatePagamento($id); // Update para não enviar novamente o mesmo e-mail.
            } 

        var_dump($view); 
             
         } 
     
    }

    public function resumoFinanceiroMes()
    {
        $this->data['estatisticas_financeiro'] = $this->mapos_model->getEstatisticasFinanceiroGeral();

        $view = $this->load->view('/templates/resumofinanceiro',$this->data,true);
        $titulo = "[WDM-Controller] Resumo financeiro do mês ".(date('m'))."!";        
        //$resultado = $this->sendmail($view,$titulo);        
        $resultado = $this->load->view($view,'',TRUE); 
        var_dump($resultado);
        
        die('aqui irá enviar o resumo financeiro para o cliente com gráficos e estatísticas');
    } 

     public function resumoFinanceiroGeral()
    {            
        die('aqui irá enviar o resumo financeiro para o cliente com gráficos e estatísticas de todo o sistema');
    } 

   


   
}