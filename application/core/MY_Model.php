<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {

	protected $active_group;
    protected $db2; // <------ ADICIONE 

    function __construct() {
       parent::__construct();
       $this->connect(); 
    }

    public function __destruct() {
        $this->db->close();
    }



    public function connect($active_group = 'default'){    	
        $this->active_group = $active_group;
        $db = $this->load->database($active_group, TRUE);
        $this->db = $db;

        // ADICIONE ISTO ABAIXO
        $db2 = $this->load->database('auxiliar', TRUE);
        $this->db2 = $db2;
    }


    /* CRUD PADRÃO */
    /* Bruno Magnata 2024 */

    public function listar()
	{
		$this->db->select('*');
		$this->db->where($this->visivel,'1');
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
        $this->db->insert($this->tabela, $dados);
        if ($this->db->affected_rows() == '1')
            return $this->db->insert_id();
        return false;
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,[$this->visivel => 0]);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	/* SQL GENERICO - PADRÃO */
	/* Bruno Magnata 2019 */

    public function operacaoDiaAnterior()
    {
        $this->db2->select('DISTINCT(usuarios.`usuario_id`) AS idUsua, usuario_nome AS loja, "Red" AS label, 
           `categoria_cart_nome_credenciadora` AS credenciadora,
            COUNT(`idFinanceiro`) AS `operacaoQtd`,       
        	
        	(SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao`  WHERE
	    	`operacao_data_cadastro` = SUBDATE(CURDATE(), 1)  AND `operacao_visivel` = 1 
	        AND cliente_id = idUsua) AS valorOperacao,       
      
      		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE `data_vencimento` = SUBDATE(CURDATE(), 1)
      		AND `financeiro_tipo` = "despesa" AND `financeiro_visivel` = 1 AND  financeiro_forn_clie_usua_id = idUsua  ) AS repasseLoja,
             
      		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE `data_vencimento` = SUBDATE(CURDATE(), 1)
       		AND `financeiro_tipo` = "receita" AND `financeiro_visivel` = 1 AND  financeiro_forn_clie_usua_id = idUsua  ) AS lucroMix ');

        $this->db2->join('usuarios','financeiro.financeiro_forn_clie_usua_id = usuarios.usuario_id');
        $this->db2->join('operacao','financeiro.`idOperacao` = `operacao`.`operacao_id`','RIGHT');

        $this->db2->join('taxa_cartao_associado','taxa_cartao_associado.taxa_usuario_id = usuarios.usuario_id');
        $this->db2->join('categoria_cartao','taxa_cartao_bandeira = categoria_cart_id');

        $this->db2->where('data_vencimento = SUBDATE(CURDATE(), 1)');
        $this->db2->where('taxa_visivel',1);
        $this->db2->where('financeiro_visivel', 1);


        $this->db2->group_by('usuarios.`usuario_id`,financeiro_tipo');
        // $this->db2->order_by('usuario_nome');
        $this->db2->order_by('valorOperacao','DESC');

        return $this->db2->get($this->tabela)->result();

    }


    public function operacaoMesAnt()
    {
        $this->db2->select('DISTINCT(usuarios.`usuario_id`) AS idUsua, usuario_nome AS loja, "Red" AS label, 
       `categoria_cart_nome_credenciadora` AS credenciadora,
        COUNT(`idFinanceiro`) AS `operacaoQtd`,  

    	(SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao`  WHERE
    	MONTH(`operacao_data_cadastro`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH))  
    	AND YEAR(`operacao_data_cadastro`) = YEAR(CURDATE())   AND `operacao_visivel` = 1 
        AND cliente_id = idUsua) AS valorOperacao,     
  
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  MONTH(`data_vencimento`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH))  
     	AND YEAR(`data_vencimento`) = YEAR(CURDATE()) AND `financeiro_tipo` = "despesa" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS repasseLoja,
         
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  MONTH(`data_vencimento`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH))  
     	AND YEAR(`data_vencimento`) = YEAR(CURDATE()) AND `financeiro_tipo` = "receita" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS lucroMix ');

        $this->db2->join('usuarios','financeiro.financeiro_forn_clie_usua_id = usuarios.usuario_id');
        $this->db2->join('operacao','financeiro.`idOperacao` = `operacao`.`operacao_id`','RIGHT');

        $this->db2->join('taxa_cartao_associado','taxa_cartao_associado.taxa_usuario_id = usuarios.usuario_id');
        $this->db2->join('categoria_cartao','taxa_cartao_bandeira = categoria_cart_id');

        $this->db2->where('MONTH(`data_vencimento`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH))');
        $this->db2->where('YEAR(`data_vencimento`) = YEAR(CURDATE())');
        $this->db2->where('taxa_visivel',1);
        $this->db2->where('financeiro_visivel', 1);


        $this->db2->group_by('usuarios.`usuario_id`,financeiro_tipo');
        // $this->db2->order_by('usuario_nome');
        $this->db2->order_by('valorOperacao','DESC');


        return $this->db2->get($this->tabela)->result();

    }



    public function operacaoMes()
    {
        $this->db2->select('DISTINCT(usuarios.`usuario_id`) AS idUsua, usuario_nome AS loja, "Red" AS label, 
       `categoria_cart_nome_credenciadora` AS credenciadora,
        COUNT(`idFinanceiro`) AS `operacaoQtd`,  

    	(SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao`  WHERE
    	MONTH(`operacao_data_cadastro`) = MONTH(CURDATE()) 
    	AND YEAR(`operacao_data_cadastro`) = YEAR(CURDATE())   AND `operacao_visivel` = 1 
        AND cliente_id = idUsua) AS valorOperacao,     
  
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  MONTH(`data_vencimento`) = MONTH(CURDATE()) 
     	AND YEAR(`data_vencimento`) = YEAR(CURDATE()) AND `financeiro_tipo` = "despesa" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS repasseLoja,
         
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  MONTH(`data_vencimento`) = MONTH(CURDATE()) 
     	AND YEAR(`data_vencimento`) = YEAR(CURDATE()) AND `financeiro_tipo` = "receita" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS lucroMix ');

        $this->db2->join('usuarios','financeiro.financeiro_forn_clie_usua_id = usuarios.usuario_id');
        $this->db2->join('operacao','financeiro.`idOperacao` = `operacao`.`operacao_id`','RIGHT');

        $this->db2->join('taxa_cartao_associado','taxa_cartao_associado.taxa_usuario_id = usuarios.usuario_id');
        $this->db2->join('categoria_cartao','taxa_cartao_bandeira = categoria_cart_id');

        $this->db2->where('MONTH(`data_vencimento`) = MONTH(CURDATE())');
        $this->db2->where('YEAR(`data_vencimento`) = YEAR(CURDATE())');
        $this->db2->where('taxa_visivel',1);
        $this->db2->where('financeiro_visivel', 1);


        $this->db2->group_by('usuarios.`usuario_id`,financeiro_tipo');
        // $this->db2->order_by('usuario_nome');
        $this->db2->order_by('valorOperacao','DESC');


        return $this->db2->get($this->tabela)->result();

    }


    public function operacaoAno()
    {
        $this->db2->select('DISTINCT(usuarios.`usuario_id`) AS idUsua, usuario_nome AS loja, "Red" AS label, 
       `categoria_cart_nome_credenciadora` AS credenciadora,
        COUNT(`idFinanceiro`) AS `operacaoQtd`,       
    	
    	(SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao`  WHERE
    	YEAR(`operacao_data_cadastro`) = YEAR(CURDATE())   AND `operacao_visivel` = 1 
        AND cliente_id = idUsua) AS valorOperacao,      
  
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE YEAR(`data_vencimento`) = YEAR(CURDATE())
  		AND `financeiro_tipo` = "despesa" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS repasseLoja,
         
  		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  YEAR(`data_vencimento`) = YEAR(CURDATE())
  		AND `financeiro_tipo` = "receita" AND `financeiro_visivel` = 1
     	AND  financeiro_forn_clie_usua_id = idUsua  ) AS lucroMix ');

        $this->db2->join('usuarios','financeiro.financeiro_forn_clie_usua_id = usuarios.usuario_id');
        $this->db2->join('operacao','financeiro.`idOperacao` = `operacao`.`operacao_id`','RIGHT');

        $this->db2->join('taxa_cartao_associado','taxa_cartao_associado.taxa_usuario_id = usuarios.usuario_id');
        $this->db2->join('categoria_cartao','taxa_cartao_bandeira = categoria_cart_id');
       
        $this->db2->where('YEAR(`data_vencimento`) = YEAR(CURDATE())');
        $this->db2->where('taxa_visivel',1);
        $this->db2->where('financeiro_visivel', 1);


        $this->db2->group_by('usuarios.`usuario_id`,financeiro_tipo');
         // $this->db2->order_by('usuario_nome');
        $this->db2->order_by('valorOperacao','DESC');

        return $this->db2->get($this->tabela)->result();

    }


    public function despesa($periodo)
    {
        
        $this->db2->select(' SUM(`financeiro_valor`) AS totalDespesa ');


       if ($periodo == 'ano') {
       	$this->db2->where('YEAR(`data_vencimento`) = YEAR(CURDATE())');
        }elseif	($periodo == 'mesAnt') {
       	$this->db2->where('MONTH(`data_vencimento`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH)) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');      
        }elseif ($periodo == 'mes') {
       	$this->db2->where('MONTH(`data_vencimento`) = MONTH(CURDATE()) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');
        }elseif ($periodo == 'dia') {
       	 $this->db2->where('DAY(`data_vencimento`) = DAY(CURDATE()) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');
        }   

      	$this->db2->where('financeiro_tipo', 'despesa' );
        $this->db2->where('financeiro_visivel', 1);
        $this->db2->not_like('financeiro_descricao', 'Fatura Operação -');

        return $this->db2->get($this->tabela)->result();

    }


    public function sobra($periodo)
    {
        
        $this->db2->select(' SUM(`financeiro_valor`) AS totalSobra ');


       if ($periodo == 'ano') {
       	$this->db2->where('YEAR(`data_vencimento`) = YEAR(CURDATE())');
       }elseif ($periodo == 'mesAnt') {
       	$this->db2->where('MONTH(`data_vencimento`) = MONTH(ADDDATE(NOW(), INTERVAL -1 MONTH)) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');	
       }elseif ($periodo == 'mes') {
       	$this->db2->where('MONTH(`data_vencimento`) = MONTH(CURDATE()) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');
       }elseif ($periodo == 'dia') {
       	 $this->db2->where('DAY(`data_vencimento`) = DAY(CURDATE()) AND YEAR(`data_vencimento`) = YEAR(CURDATE())');
       }          


      	$this->db2->where('financeiro_tipo', 'receita' );
        $this->db2->where('financeiro_visivel', 1);
        $this->db2->where('categoria_fin_id', 3);

        return $this->db2->get($this->tabela)->result();

    }




    public function previsaoFinanceiroDetalhe()
    {
            
            $sql = 
            "SELECT 'MixPay' AS `loja`, '01' AS `num`, 'Red' AS `label`,
            COUNT(DISTINCT `financeiro`.`data_vencimento`) AS `dias_lancamento`,
            MONTH(`financeiro`.`data_vencimento`) AS `mes`,
            YEAR(`financeiro`.`data_vencimento`) AS `ano`,
            (SELECT ROUND(SUM(`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `financeiro`
            WHERE ((`mes` = MONTH(`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`financeiro`.`data_vencimento`)) AND (`financeiro`.`financeiro_tipo` = 'receita') AND (`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
            (SELECT ROUND(SUM(`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `financeiro`
            WHERE ((`mes` = MONTH(`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`financeiro`.`data_vencimento`)) AND (`financeiro`.`financeiro_tipo` = 'despesa') AND (`financeiro`.`financeiro_visivel` = 1) AND (
        `financeiro`.`idOperacao` IS NULL) )) AS `despesa`,
            /*
            (SELECT ROUND(SUM((`itens_de_vendas`.`prod_preco_custo` * `itens_de_vendas`.`quantidade`)), 2) AS `total`
            FROM (`itens_de_vendas` JOIN `vendas` ON ((`itens_de_vendas`.`vendas_id` = `vendas`.`idVendas`)))
            WHERE ((`vendas`.`venda_visivel` = 1) AND (`mes` = MONTH(`vendas`.`dataVenda`)) AND (`ano` = YEAR(`vendas`.`dataVenda`)))
            GROUP BY MONTH(`vendas`.`dataVenda`)
            ORDER BY MONTH(`vendas`.`dataVenda`)) AS `custo`,
            */
           
            '0.00' AS `custo`,
            (SELECT ROUND((`receita` / COUNT(DISTINCT `financeiro`.`data_vencimento`)), 2)) AS `media`,
            (SELECT ROUND(((0.0 * `receita`) / 100), 2)) AS `comissao`
            FROM `financeiro`
            WHERE ((MONTH(`financeiro`.`data_vencimento`) = MONTH(`financeiro`.`data_vencimento`)) AND (YEAR(`financeiro`.`data_vencimento`) = YEAR(`financeiro`.`data_vencimento`)) AND (`financeiro`.`financeiro_visivel` = 1))
            GROUP BY MONTH(`financeiro`.`data_vencimento`) , YEAR(`financeiro`.`data_vencimento`)";
 

        $sql = "SELECT *
                FROM ($sql) as subquery 
                WHERE mes = MONTH(CURDATE())
                AND ano = YEAR(CURDATE())";

        return $this->db2->query($sql)->result();
    }

   

}








