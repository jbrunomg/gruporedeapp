<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function v($parametro)
	{
		echo "<pre>";
		var_dump($parametro);
		die();
	}

	function p($parametro)
	{
		echo "<pre>";
		print_r($parametro);
		die();
	}

	function checarPermissao($permissao){

		$CI = get_instance();
	    $CI->load->library('session');


	    
		$todasPermissoes = unserialize($CI->session->userdata('permissao'));


		return in_array($permissao, $todasPermissoes);
	}



	function gerarPdf($html,$nomeArquivo = 'relatorio',$download = true)
	{
		// Instancia a classe mPDF
		//$mpdf = new mPDF();
		$mpdf = new mPDF(
	        'c',    // mode - default ''

	        '', // format - A4, for example, default ''

	        9,     // font size - default 0

	        'Arial',    // default font family

	        8,    // margin_left

	        8,    // margin right

	        5,     // margin top

	        16,    // margin bottom

	        9,     // margin header

	        9,     // margin footer

	        $relatorio['orientacao']);  // L - landscape, P - portrait
		// Ao invés de imprimir a view 'welcome_message' na tela, passa o código
		// HTML dela para a variável $html	
		// Define um Cabeçalho para o arquivo PDF
		$mpdf->SetHeader('','0');



		

		$footer =  "<div align=\"center\">
		              <span>Rua João Fernandes Vieira, 574 - Ed. Empr. Fernandes Vieira - Sala 602 - Boa Vista - Recife/PE</span><br/>
		              <span>Fone: (81) 3221-4740 - Site: www.nudep.com.br / e-mail: nudep@nudep.com.br</span>	              
		            <div>";
		// Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
		// página através da pseudo-variável PAGENO
		$mpdf->SetFooter($footer);
		// Insere o conteúdo da variável $html no arquivo PDF

		// Load a stylesheet
		$stylesheet = file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		$mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text
	
		$mpdf->writeHTML($html);
		// Cria uma nova página no arquivo
		/*$mpdf->AddPage();*/
		// Insere o conteúdo na nova página do arquivo PDF
		/*$mpdf->WriteHTML('<p><b>Minha nova página no arquivo PDF</b></p>');*/
		// Gera o arquivo PDF
		
		if($download){
			$mpdf->Output($nomeArquivo.'_'.date('d_m_Y').'.pdf','D');
		}else{
			$mpdf->Output();
		}
		
	}





	function extenso($valor = 0, $maiusculas = false) {
	    if(!$maiusculas){
	        $singular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
	        $plural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões"];
	        $u = ["", "um", "dois", "três", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
	    }else{
	        $singular = ["CENTAVO", "REAL", "MIL", "MILHÃO", "BILHÃO", "TRILHÃO", "QUADRILHÃO"];
	        $plural = ["CENTAVOS", "REAIS", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
	        $u = ["", "um", "dois", "TRÊS", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
	    }

	    $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
	    $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
	    $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];

	    $z = 0;
	    $rt = "";

	    $valor = number_format($valor, 2, ".", ".");
	    $inteiro = explode(".", $valor);
	    for($i=0;$i<count($inteiro);$i++)
	    for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
	    $inteiro[$i] = "0".$inteiro[$i];

	    $fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	    for ($i=0;$i<count($inteiro);$i++) {
	        $valor = $inteiro[$i];
	        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
	        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
	        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

	        $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd &&
	        $ru) ? " e " : "").$ru;
	        $t = count($inteiro)-1-$i;
	        $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
	        if ($valor == "000")$z++; elseif ($z > 0) $z--;
	        if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
	        if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
	    }

	    if(!$maiusculas){
	        $return = $rt ? $rt : "zero";
	    } else {
	        if ($rt) $rt = preg_replace(" /E/ "," e ",ucwords($rt));
	            $return = ($rt) ? ($rt) : "Zero" ;
	    }

	    if(!$maiusculas){
	        return preg_replace(" /E/ "," e ",ucwords($return));
	    }else{
	        return strtoupper($return);
	    }
	}


	

	function getOS() { 



        global $user_agent;



        $user_agent     =   $_SERVER['HTTP_USER_AGENT'];



        $os_platform    =   "Unknown OS Platform";



        $os_array       =   array(

                '/windows nt 10.0/i'    =>  'Windows 10',

                '/windows nt 6.2/i'     =>  'Windows 8',

                '/windows nt 6.1/i'     =>  'Windows 7',

                '/windows nt 6.0/i'     =>  'Windows Vista',

                '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',

                '/windows nt 5.1/i'     =>  'Windows XP',

                '/windows xp/i'         =>  'Windows XP',

                '/windows nt 5.0/i'     =>  'Windows 2000',

                '/windows me/i'         =>  'Windows ME',

                '/win98/i'              =>  'Windows 98',

                '/win95/i'              =>  'Windows 95',

                '/win16/i'              =>  'Windows 3.11',

                '/macintosh|mac os x/i' =>  'Mac OS X',

                '/mac_powerpc/i'        =>  'Mac OS 9',

                '/linux/i'              =>  'Linux',

                '/ubuntu/i'             =>  'Ubuntu',

                '/iphone/i'             =>  'iPhone',

                '/ipod/i'               =>  'iPod',

                '/ipad/i'               =>  'iPad',

                '/android/i'            =>  'Android',

                '/blackberry/i'         =>  'BlackBerry',

                '/webos/i'              =>  'Mobile'

            

            );



        foreach ($os_array as $regex => $value) { 



            if (preg_match($regex, $user_agent)) {

                $os_platform    =   $value;

            }



        }   



        return $os_platform;



    }


?>