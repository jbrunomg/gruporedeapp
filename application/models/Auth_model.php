<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $tabela = 'usuarios';

	public function processarLogin($dados)
	{
        $this->db->select('perfis.*, usuarios.*, SUBSTRING(usuario_imagem,1, INSTR(usuario_imagem, ".br")+2) as usuario_dominio  ');
		$this->db->join('perfis','perfil_id = usuario_perfil');		
		$this->db->where($dados);
        $this->db->where('usuario_ativo','1');
		return $this->db->get($this->tabela)->result();		
	}

























    


	public function pagamentoAtraso($conexao)
    {
        $query = $this->db->query(" SELECT DATEDIFF(NOW(),pagamento_sistema_data_vencimento) AS dias_vencido, `pagamento_sistema_id`, `pagamento_sistema_data_vencimento`, `email_cron` 
                                    FROM  {$conexao}.`pagamento_sistema`, 
                                    	  {$conexao}.`emitente` 
                                    WHERE `pagamento_sistema_status` = '0'
                                    AND pagamento_sistema_data_vencimento < CURRENT_DATE
                                    ORDER BY pagamento_sistema_data_vencimento DESC; ");
        return $query->result();
    }

    public function pagamentoaVencer($conexao)
    {
        $query = $this->db->query(" SELECT DATEDIFF(pagamento_sistema_data_vencimento,NOW()) AS dias_vencido, `pagamento_sistema_id`, `pagamento_sistema_data_vencimento`, `email_cron`
                                    FROM  {$conexao}.`pagamento_sistema`, 
                                    	  {$conexao}.`emitente`  
                                    WHERE `pagamento_sistema_status` = '0'
                                    AND pagamento_sistema_data_vencimento > CURRENT_DATE
                                    ORDER BY pagamento_sistema_data_vencimento DESC; ");
        return $query->result();
    }


    public function pagamentoRealizado($conexao)
    {
        $query = $this->db->query(" SELECT pagamento_sistema_id, pagamento_sistema_data_vencimento, pagamento_sistema_data_pagamento, `email_cron`
        							FROM {$conexao}.`pagamento_sistema`, 
                                    	 {$conexao}.`emitente` 
                                    WHERE
                                    pagamento_sistema_status = 1 AND
                                    pagamento_sistema_data_pagamento IS NOT NULL AND
                                    pagamento_sistema_evioEmail = 0; ");
        return $query->result();
    }


    public function updatePagamento($id,$conexao)
    {

        $query = $this->db->query(" UPDATE {$conexao}.`pagamento_sistema` SET `pagamento_sistema_evioEmail` = 1 WHERE pagamento_sistema_id = {$id}; ");
        return $query->result();
    }


    public function getEmitente($conexao)
    {
        $sql = "SELECT * FROM {$conexao}.emitente ";

        return $this->db->query($sql)->row();
    }

    /* INDICADOR VENDAS INICIO */
   
    public function getIndicadorVendaDia($conexao)
    {
    
    // VENDAS DO DIA
    $sql = " SELECT SUM(`valorTotal`) AS venda_dia FROM {$conexao}.`vendas`
        WHERE `dataVenda` = CURDATE() 
        AND vendas.`status` = 1 ";

    return $this->db->query($sql)->row();   

    }

    public function getIndicadorMargemLucro($conexao)
    {
    
    # MARGEM DE LUCRO (MÉDIA)
    $sql = "  SELECT ROUND((((SUM(`subTotal`) / SUM(`precoCompra`))* 100)-100),2) AS percentual FROM {$conexao}.`itens_de_vendas`
        INNER JOIN {$conexao}.`vendas` ON `itens_de_vendas`.`vendas_id` = vendas.`idVendas`
        WHERE `dataVenda` = CURDATE() 
        AND vendas.`status` = 1 ";

    return $this->db->query($sql)->row(); 
         
    }

    public function getIndicadorClienteAtendido($conexao)
    {
    
    # CLIENTES ATENDIDOS
    $sql = "  SELECT COUNT( DISTINCT `clientes_id`) AS clie_antendido FROM {$conexao}.`vendas`
        WHERE `dataVenda` = CURDATE()  -- '2020-11-03' 
        AND vendas.`status` = 1";

    return $this->db->query($sql)->row();

    }

    public function getIndicadorVendedorDia($conexao)
    {
    
    # VENDEDOR DO DIA
    $sql = "SELECT ROUND(SUM(`valorTotal`),2) AS total, `usuarios`.`nome` AS vendedor
                FROM {$conexao}.vendas 
                LEFT JOIN {$conexao}.`usuarios`
                ON vendas.`usuarios_id` = `usuarios`.`idUsuarios`    
                WHERE `dataVenda` = CURDATE()  -- '2020-11-03'
        AND vendas.`status`  = 1 
                GROUP BY vendas.`usuarios_id`
                ORDER BY total DESC
                LIMIT 1";

    return $this->db->query($sql)->row();
                    
    }


    public function getIndicadorVendidoMesAtual($conexao)
    {
    
    # VENDIDO EM MES ATUAL
    $sql = " SELECT ROUND(SUM(`valorTotal`),2) AS total FROM {$conexao}.`vendas`
            WHERE 
            MONTH(dataVenda) =  MONTH(CURDATE())
			AND YEAR(dataVenda) = YEAR(CURDATE())
            AND vendas.`status` = 1";
    
    return $this->db->query($sql)->row();
             
    }

    /* INDICADOR VENDAS FIM */


    /* INDICADOR ESTOQUE INICIO */
    public function getIndicadorEstoqueBaixo($conexao)
    {
    
    # COM ESTOQUE BAIXO
    $sql = " SELECT COUNT(`idProdutos`) AS estoque_baixo FROM {$conexao}.`produtos`
    WHERE `estoque` < `estoqueMinimo`
    AND `estoque` <> 0 ";
    
    return $this->db->query($sql)->row();    

    }

    public function getIndicadorReposicao($conexao)
    {
    
    # VALOR ESTIMADO PARA REPOSIÇÂO
    $sql = " SELECT (SUM(`precoCompra`) * (`estoqueMinimo` - estoque))  AS valor_reposicao  FROM {$conexao}.`produtos`
    WHERE `estoque` < `estoqueMinimo`
    AND `estoque` <> 0 ";
    
    return $this->db->query($sql)->row();    

    }

    // PARA RECEBER HOJE
    // PARA ENTREGAR HOJE 

    public function getIndicadorCompradoMesAtual($conexao)
    {
        # COMPRADO EM SETEMBRO
    $sql = " SELECT ROUND(SUM((`precoCompra`)*`quantidade`) ,2) AS total  FROM {$conexao}.`entrada_saida_produtos` 
    INNER JOIN {$conexao}.`itens_entrada_saida` ON entrada_saida_produtos.`idEntradaSaida` = `itens_entrada_saida`.`entradasaida_id`
    WHERE MONTH(data_entr_saida) =  MONTH(CURDATE())
    AND tipo = 'Entrada' ";
    
    return $this->db->query($sql)->row();     

    }


    /* INDICADOR ESTOQUE FIM */

    
    /* INDICADOR FINANCEIRO INICIO */

      public function getIndicadorSaldoInicial($conexao)
    {
        # SALDO INICIAL
        $sql = " SELECT  SUM( DISTINCT(
        SELECT SUM(`valor`)  FROM {$conexao}.`lancamentos`
        WHERE tipo = 'receita'
        AND baixado = 1  
        AND data_vencimento = CURDATE()-1 ) -
        ( SELECT SUM(`valor`)  FROM {$conexao}.`lancamentos`
        WHERE tipo = 'despesa'
        AND baixado = 1
        AND data_vencimento = CURDATE()-1 ) ) AS saldo_inicial
        FROM {$conexao}.lancamentos
        WHERE baixado = 1
        AND data_vencimento = CURDATE()-1; ";
    
    return $this->db->query($sql)->row(); 

    }

    public function getIndicadorRecebimento($conexao)
    {
        # RECEBIMENTOS
        $sql = " SELECT SUM(valor) AS recebimento FROM {$conexao}.`lancamentos`
        WHERE `data_pagamento` = CURDATE() 
        AND tipo = 'receita'
        AND baixado = 1 ";
    
        return $this->db->query($sql)->row(); 

    }

    public function getIndicadorPagamento($conexao)
    {
        # PAGAMENTOS
        $sql = " SELECT SUM(valor) AS pagamento FROM {$conexao}.`lancamentos`
        WHERE `data_pagamento` = CURDATE() 
        AND tipo = 'despesa'
        AND baixado = 1 ";
    
        return $this->db->query($sql)->row(); 
    }

    public function getIndicadorReceberAmanha($conexao)
    {
        # PREVISTO PARA AMANHÃ - RECEBER
        $sql = " SELECT SUM(`valor`) AS receber_amanha FROM {$conexao}.lancamentos
        WHERE tipo = 'receita'
        AND baixado = 0
        AND data_vencimento = CURDATE()+1; ";
    
        return $this->db->query($sql)->row(); 
    }

    public function getIndicadorPagarAmanha($conexao)
    {
        # PREVISTO PARA AMANHÃ - PAGAR
        $sql = " SELECT SUM(`valor`) AS pagar_amanha FROM {$conexao}.lancamentos
        WHERE tipo = 'despesa'
        AND baixado = 0
        AND data_vencimento = CURDATE()+1; ";
    
        return $this->db->query($sql)->row(); 
    }

    /* INDICADOR FINANCEIRO FIM */    

    public function getEstatisticasFinanceiroGeral($conexao, $mesAnterior = true){

        $sql = "SELECT 
                  SUM(
                    CASE
                      WHEN baixado = 1 
                      AND tipo = 'receita' 
                      THEN ROUND(valor,2) 
                    END
                  ) AS total_receita,
                  SUM(
                    CASE
                      WHEN baixado = 1 
                      AND tipo = 'despesa' 
                      THEN ROUND(valor,2) 
                    END
                  ) AS total_despesa,
                  SUM(
                    CASE
                      WHEN baixado = 0 
                      AND tipo = 'receita' 
                      THEN ROUND(valor,2) 
                    END
                  ) AS total_receita_pendente,
                  SUM(
                    CASE
                      WHEN baixado = 0 
                      AND tipo = 'despesa' 
                      THEN ROUND(valor,2) 
                    END
                  ) AS total_despesa_pendente,
                  `email_cron` 
                FROM
                  {$conexao}.lancamentos,
                  {$conexao}.`emitente` ";
        if($mesAnterior){

          $sql .=" WHERE data_vencimento BETWEEN DATE_SUB(
                    DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW()) - 1 DAY),
                    INTERVAL 1 MONTH
                  ) 
                  AND DATE_SUB(LAST_DAY(NOW()), INTERVAL 1 MONTH)";
        }

        return $this->db->query($sql)->row();
    }

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */