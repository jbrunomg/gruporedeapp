<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Cliente_model extends CI_Model {


	private $base  =  '';


    function __construct($foo = null) {

        $this->base =  $this->session->userdata('empresa_nome_base');

    }


    /**
     * Top 10 Mês (ANTIGO)
     */

    /*
    public function top10Mes()
    {

        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_clie_top10_mes 
                    GROUP BY loja
                    ORDER BY num	";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Top 10 Mês Detalhe (ANTIGO)
     */

    /*
    public function top10MesDetalhe($loja)
    {

        $query = "  SELECT view_clie_top10_mes.*, (valorTotal - custoTotal) AS lucro, 
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_clie_top10_mes` where loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
    */


    /**
     * Top 10 Ano (ANTIGO)
     */

    /*
    public function top10Ano()
    {

        $query = "  SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( ( ( (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_clie_top10_ano 
                    GROUP BY loja										ORDER BY num	";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Top 10 Ano Detalhe (ANTIGO)
     */

    /*
    public function top10AnoDetalhe($loja)
    {

        $query = "  SELECT view_clie_top10_ano.*, (valorTotal - custoTotal) AS lucro, 
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_clie_top10_ano` where loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Top 10 Ano Anterior (ANTIGO)
     */

    /*
    public function top10AnoAnterior()
    {

        $query = "  SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( ( ( (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_clie_top10_ano_anterior 
                    GROUP BY loja 
                    ORDER BY num  ";

        return $this->db->query($query)->result();

    }
    */


    /**
     * Top 10 Ano Anterior Detalhe (ANTIGO)
     */

    /*
    public function top10AnoAnteriorDetalhe($loja)
    {

        $query = "  SELECT view_clie_top10_ano_anterior.*, (valorTotal - custoTotal) AS lucro, 
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_clie_top10_ano_anterior` where loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Pagamento Aberto (ANTIGO)
     */

    /*
    public function pagamentoAberto()
    {

        $query = " SELECT loja, SUM(qtd) AS qtd, SUM(valor) AS valorTotal, recebidoMes AS valorTotalR, recebidoDia AS valorTotalRD,                  
            label 
            FROM view_clie_pagamento_aberto 
            GROUP BY loja
            ORDER BY num     ";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Pagamento Aberto Detalhe (ANTIGO)
     */

    /*
    public function pagamentoAbertoDetalhe($loja)
    {

        $query = " SELECT loja, cliente_id, cliente, valor AS valorTotal,                  
            label 
            FROM view_clie_pagamento_aberto 
            where loja = '{$loja}' ORDER BY `valorTotal` DESC
            ";

        return $this->db->query($query)->result();

    }
    */

    public function detalhamentoPago($loja,$id)
    {
        $servidorAtual = BDCAMINHO;
        $servidorAtual = explode("_", $servidorAtual);
        $servidorAtual = $servidorAtual[0] . '_' . $servidorAtual[1] . '_';

        $loja = strtolower($loja);

        $base = $servidorAtual . $loja;
        
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_pagamento) AS ano,
        MONTH(data_pagamento) AS mes,
        SUM(`financeiro_valor`) AS `total`,
        '' AS totalCompra,
        '' AS totalAberto         
            FROM `$base`.`financeiro`
        WHERE `financeiro_baixado` = 1 
           AND `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $id
           AND YEAR(`data_pagamento`) = YEAR(`data_pagamento`)
        GROUP BY YEAR(`data_pagamento`),MONTH(data_pagamento)
        ORDER BY YEAR(`data_pagamento`) DESC, MONTH(data_pagamento) DESC 
            ";

        return $this->db->query($query)->result();

    }


    public function detalhamentoDiasPago($loja, $id, $ano, $mes)
    {
        $servidorAtual = BDCAMINHO;
        $servidorAtual = explode("_", $servidorAtual);
        $servidorAtual = $servidorAtual[0] . '_' . $servidorAtual[1] . '_';

        $loja = strtolower($loja);

        $base = $servidorAtual . $loja;
        
        $query = " SELECT * FROM  `$base`.`baixar_conta`
            WHERE `id_cliente` = $id
            AND YEAR(`data_baixa`) = '$ano'
            AND MONTH(`data_baixa`) = '$mes'
            ORDER BY id_baixa DESC 
        ";

        return $this->db->query($query)->result();

    }



    public function detalhamentoAberto($loja,$id)
    {
        $servidorAtual = BDCAMINHO;
        $servidorAtual = explode("_", $servidorAtual);
        $servidorAtual = $servidorAtual[0] . '_' . $servidorAtual[1] . '_';

        $loja = strtolower($loja);

        $base = $servidorAtual . $loja;
        
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_pagamento) AS ano,
        MONTH(data_pagamento) AS mes,
        SUM(`financeiro_valor`) AS `totalAberto`      
            FROM `$base`.`financeiro`
        WHERE `financeiro_baixado` = 0 
           AND `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $id
           AND YEAR(`data_pagamento`) = YEAR(`data_pagamento`)
        GROUP BY YEAR(`data_pagamento`),MONTH(data_pagamento)
        ORDER BY YEAR(`data_pagamento`) DESC, MONTH(data_pagamento) DESC 
            ";

        return $this->db->query($query)->result();

    }


    public function detalhamentoCompra($loja,$id)
    {
        $servidorAtual = BDCAMINHO;
        $servidorAtual = explode("_", $servidorAtual);
        $servidorAtual = $servidorAtual[0] . '_' . $servidorAtual[1] . '_';

        $loja = strtolower($loja);

        $base = $servidorAtual . $loja;
        
        $query = " SELECT 
        `financeiro_forn_clie` AS cliente,
        YEAR(data_vencimento) AS ano,
        MONTH(data_vencimento) AS mes,
        SUM(`financeiro_valor`) AS `totalCompra`
       
            FROM `$base`.`financeiro`
        WHERE  `financeiro_visivel` = 1 
           AND `financeiro_forn_clie_id` = $id
           AND YEAR(`data_vencimento`) = YEAR(`data_vencimento`)
        GROUP BY YEAR(`data_vencimento`),MONTH(data_vencimento)
        ORDER BY YEAR(`data_vencimento`) DESC, MONTH(data_vencimento) DESC 
            ";

        return $this->db->query($query)->result();

    }

    public function top10Mes()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Purple", "Blue-light", "Red", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))) AS `qtdVenda`,
            COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
            GROUP BY `$b`.`clientes`.`cliente_id`
            ORDER BY `valorTotal`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10Ano()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (0 <> NOW()))) AS `qtdVenda`,
            COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (0 <> NOW()))
            GROUP BY `$b`.`clientes`.`cliente_id`
            ORDER BY `valorTotal` DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10AnoAnterior()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (0 <> NOW()))) AS `qtdVenda`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (0 <> NOW()))
            GROUP BY `$b`.`clientes`.`cliente_id`
            ORDER BY `valorTotal` DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery 
                GROUP BY loja 
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function pagamentoAberto()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`, '1' AS `qtd`,
            `$b`.`clientes`.`cliente_id` AS `cliente_id`,
            `$b`.`clientes`.`cliente_nome` AS `cliente`,
            SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
            (SELECT SUM(`$b`.`financeiro`.`financeiro_valor`)
            FROM `$b`.`financeiro`
            WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `recebidoMes`,
            (SELECT SUM(`$b`.`financeiro`.`financeiro_valor`)
            FROM `$b`.`financeiro`
            WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) = DAYOFMONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `recebidoDia`
            FROM (`$b`.`financeiro` JOIN `$b`.`clientes` ON ((`$b`.`financeiro`.`financeiro_forn_clie_id` = `$b`.`clientes`.`cliente_id`)))
            WHERE ((`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
            GROUP BY `$b`.`financeiro`.`financeiro_forn_clie_id`";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtd) AS qtd, SUM(valor) AS valorTotal, recebidoMes AS valorTotalR, recebidoDia AS valorTotalRD, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10MesDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))) AS `qtdVenda`,
        COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
        GROUP BY `$b`.`clientes`.`cliente_id`
        ORDER BY `valorTotal`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal` / `custoTotal`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function top10AnoDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (0 <> NOW()))) AS `qtdVenda`,
        COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (0 <> NOW()))
        GROUP BY `$b`.`clientes`.`cliente_id`
        ORDER BY `valorTotal` DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal` / `custoTotal`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function top10AnoAnteriorDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`, `$b`.`clientes`.`cliente_nome` AS `cliente_nome`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (0 <> NOW()))) AS `qtdVenda`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`clientes` ON ((`$b`.`vendas`.`clientes_id` = `$b`.`clientes`.`cliente_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (0 <> NOW()))
        GROUP BY `$b`.`clientes`.`cliente_id`
        ORDER BY `valorTotal` DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal` / `custoTotal`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function pagamentoAbertoDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`clientes`.`cliente_id` AS `cliente_id`,
        `$b`.`clientes`.`cliente_nome` AS `cliente`,
        SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
        (SELECT SUM(`$b`.`financeiro`.`financeiro_valor`)
        FROM `$b`.`financeiro`
        WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `recebidoMes`,
        (SELECT SUM(`$b`.`financeiro`.`financeiro_valor`)
        FROM `$b`.`financeiro`
        WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) = DAYOFMONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `recebidoDia`
        FROM (`$b`.`financeiro` JOIN `$b`.`clientes` ON ((`$b`.`financeiro`.`financeiro_forn_clie_id` = `$b`.`clientes`.`cliente_id`)))
        WHERE ((`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
        GROUP BY `$b`.`financeiro`.`financeiro_forn_clie_id`";

        $sql = "SELECT loja, cliente_id, cliente, valor AS valorTotal
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    private function pegarBaseDaLojaPelaBasePrincipal($loja)
    {
        $basePrincipal = BDCAMINHO;
        $basePrincipal = explode("_", $basePrincipal);
        $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

        $base = $basePrincipal . strtolower($loja);

        return $base;
    }
}



/* End of file Produto_model.php */

/* Location: ./application/models/Produto_model.php */