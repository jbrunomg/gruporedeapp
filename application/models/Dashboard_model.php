<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	// private $tabela  = 'tbcurcursos';
	// private $id      = 'pcurcucodig';
	// private $visivel = 'curso_visivel';

	/**
	 * Resumo Financeiro (ANTIGO)
	 */

	/*
public function resumoFinanceiro()
{

	$query = $this->db->query(" SELECT SUM(valor) AS valor, financeiro_tipo FROM view_com_rec_des_ano 
								WHERE mes = MONTH(CURDATE())
								GROUP BY `financeiro_tipo`
								ORDER BY financeiro_tipo  ");

	return $query->result();

}
	*/

	/**
	 * Resumo Financeiro Dia (ANTIGO)
	 */

	/*
public function resumoFinanceiroDia()
{

	$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
								WHERE  dia > DAY(CURRENT_DATE)-10
								GROUP BY dia, financeiro_tipo
								ORDER BY dia DESC
								LIMIT 7 ");

	return $query->result();

}
	*/

	/**
	 * Resumo Financeiro Receita Dia (ANTIGO)
	 */

	/*
public function resumoFinanceiroReceitaDia()
{

	$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
								WHERE financeiro_tipo = 'receita'
								AND dia > DAY(CURRENT_DATE)-10
								GROUP BY dia
								order by dia desc
								LIMIT 7 ");

	return $query->result();

}
	*/

	/**
	 * Resumo Financeiro Compra Dia (ANTIGO)
	 */

	/*
public function resumoFinanceiroCompraDia()
{

	$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
								WHERE financeiro_tipo = 'compras'
								AND dia > DAY(CURRENT_DATE)-10
								GROUP BY dia
								order by dia desc
								LIMIT 7 ");

	return $query->result();

}
	*/

	/**
	 * Resumo Financeiro Despesa Dia (ANTIGO)
	 */

	/*
public function resumoFinanceiroDespesaDia()
{

	$query = $this->db->query(" SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo  FROM view_com_rec_des_dia
								WHERE financeiro_tipo = 'despesa'
								AND dia > DAY(CURRENT_DATE)-10
								GROUP BY dia
								order by dia desc
								LIMIT 7 ");

	return $query->result();

}
	*/


public function clienteTop01Ano()
{

	$query = $this->db->query(" ");

    return $query->result();

}

public function funcionarioTop01Ano()
{
	$query = $this->db->query("  ");

    return $query->result();

}


public function sistemaOpenClose()
{

    $bases = SGBD;

    var_dump($bases);die();

        // $base = explode('_', $b);
        // $loja = strtoupper($base[2]);


	$this->db->select('emitente_botao_panico');
	return $this->db->get('mixcel17_'.GRUPOLOJA.'_matriz.`emitente`')->result();

}




public function validacaoGerente($senha){

	$this->db->select('IF("'.$senha.'" = `usuarios`.`usuario_senha`,"success","danger")AS validacao');
	$this->db->where_in('usuario_perfil','2');
	$result = $this->db->get('usuarios')->result();

   foreach ($result as $key) {
	  if ($key->validacao == 'success') {

	  return true;           
	  }
   }

	return false;


  }

  	public function resumoFinanceiroReceitaDia()
  	{
		$bases = SGBD;
		$queries = [];
		$labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

		foreach ($bases as $indice => $b) {
			$base = explode('_', $b);
			$loja = strtoupper($base[2]);
			$label = $labels[$indice];

			$num = sprintf("%02d", $indice + 1);

			$queries[] = 
			"SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) AS `dia`,
			MONTH(`$b`.`financeiro`.`data_pagamento`) AS `mes`,
			SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
			`$b`.`financeiro`.`financeiro_tipo` AS `financeiro_tipo`
			FROM `$b`.`financeiro`
			WHERE ((MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
			GROUP BY DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) , `$b`.`financeiro`.`financeiro_tipo` 
			UNION ALL
			SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`vendas`.`dataVenda`) AS `dia`,
			MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
			ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`,
			'compras' AS `financeiro_tipo`
			FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
			WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
			GROUP BY DAYOFMONTH(`$b`.`vendas`.`dataVenda`)";
		}

		$sql = implode(' UNION ALL ', $queries);

		$sql = "SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo
				FROM ($sql) as subquery
				WHERE financeiro_tipo = 'receita'
				AND dia > DAY(CURRENT_DATE) - 10
				GROUP BY dia
				ORDER BY dia
				DESC
				LIMIT 7";

		return $this->db->query($sql)->result();
	}

	public function resumoFinanceiroCompraDia()
  	{
		$bases = SGBD;
		$queries = [];
		$labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

		foreach ($bases as $indice => $b) {
			$base = explode('_', $b);
			$loja = strtoupper($base[2]);
			$label = $labels[$indice];

			$num = sprintf("%02d", $indice + 1);

			$queries[] = 
			"SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) AS `dia`,
			MONTH(`$b`.`financeiro`.`data_pagamento`) AS `mes`,
			SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
			`$b`.`financeiro`.`financeiro_tipo` AS `financeiro_tipo`
			FROM `$b`.`financeiro`
			WHERE ((MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
			GROUP BY DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) , `$b`.`financeiro`.`financeiro_tipo` 
			UNION ALL
			SELECT 
			'$loja' AS `loja`,
			DAYOFMONTH(`$b`.`vendas`.`dataVenda`) AS `dia`,
			MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
			ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`,
			'compras' AS `financeiro_tipo`
			FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
			WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
			GROUP BY DAYOFMONTH(`$b`.`vendas`.`dataVenda`)";
		}

		$sql = implode(' UNION ALL ', $queries);

		$sql = "SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo 
				FROM ($sql) as subquery
				WHERE financeiro_tipo = 'compras'
				AND dia > DAY(CURRENT_DATE) - 10
				GROUP BY dia
				ORDER BY dia
				DESC
				LIMIT 7";

		return $this->db->query($sql)->result();
	}

	public function resumoFinanceiroDespesaDia()
  	{
		$bases = SGBD;
		$queries = [];
		$labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

		foreach ($bases as $indice => $b) {
			$base = explode('_', $b);
			$loja = strtoupper($base[2]);
			$label = $labels[$indice];

			$num = sprintf("%02d", $indice + 1);

			$queries[] = 
			"SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) AS `dia`,
			MONTH(`$b`.`financeiro`.`data_pagamento`) AS `mes`,
			SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
			`$b`.`financeiro`.`financeiro_tipo` AS `financeiro_tipo`
			FROM `$b`.`financeiro`
			WHERE ((MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
			GROUP BY DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) , `$b`.`financeiro`.`financeiro_tipo` 
			UNION ALL
			SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`vendas`.`dataVenda`) AS `dia`,
			MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
			ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`,
			'compras' AS `financeiro_tipo`
			FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
			WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
			GROUP BY DAYOFMONTH(`$b`.`vendas`.`dataVenda`)";
		}

		$sql = implode(' UNION ALL ', $queries);

		$sql = "SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo 
				FROM ($sql) as subquery
				WHERE financeiro_tipo = 'despesa'
				AND dia > DAY(CURRENT_DATE) - 10
				GROUP BY dia
				ORDER BY dia
				DESC
				LIMIT 7";

		return $this->db->query($sql)->result();
	}

	public function resumoFinanceiro()
  	{
		$bases = SGBD;
		$queries = [];
		$labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

		foreach ($bases as $indice => $b) {
			$base = explode('_', $b);
			$loja = strtoupper($base[2]);
			$label = $labels[$indice];

			$num = sprintf("%02d", $indice + 1);

			$queries[] = 
			"SELECT '$loja' AS `loja`,
			MONTH(`$b`.`financeiro`.`data_pagamento`) AS `mes`,
			SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
			`$b`.`financeiro`.`financeiro_tipo` AS `financeiro_tipo`
			FROM `$b`.`financeiro`
			WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
			GROUP BY MONTH(`$b`.`financeiro`.`data_pagamento`) , `$b`.`financeiro`.`financeiro_tipo` 
			UNION ALL
			SELECT '$loja' AS `loja`,
			MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
			ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`,
			'compras' AS `financeiro_tipo`
			FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
			WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(`$b`.`vendas`.`dataVenda`)) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
			GROUP BY MONTH(`$b`.`vendas`.`dataVenda`)";
		}

		$sql = implode(' UNION ALL ', $queries);

		$sql = "SELECT SUM(valor) AS valor, financeiro_tipo
				FROM ($sql) as subquery
				WHERE mes = MONTH(CURDATE())
				GROUP BY `financeiro_tipo`
				ORDER BY financeiro_tipo";

		return $this->db->query($sql)->result();
	}

	public function resumoFinanceiroDia()
  	{
		$bases = SGBD;
		$queries = [];
		$labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

		foreach ($bases as $indice => $b) {
			$base = explode('_', $b);
			$loja = strtoupper($base[2]);
			$label = $labels[$indice];

			$num = sprintf("%02d", $indice + 1);

			$queries[] = 
			"SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) AS `dia`,
			MONTH(`$b`.`financeiro`.`data_pagamento`) AS `mes`,
			SUM(`$b`.`financeiro`.`financeiro_valor`) AS `valor`,
			`$b`.`financeiro`.`financeiro_tipo` AS `financeiro_tipo`
			FROM `$b`.`financeiro`
			WHERE ((MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
			GROUP BY DAYOFMONTH(`$b`.`financeiro`.`data_pagamento`) , `$b`.`financeiro`.`financeiro_tipo` 
			UNION ALL
			SELECT '$loja' AS `loja`,
			DAYOFMONTH(`$b`.`vendas`.`dataVenda`) AS `dia`,
			MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
			ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`,
			'compras' AS `financeiro_tipo`
			FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
			WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
			GROUP BY DAYOFMONTH(`$b`.`vendas`.`dataVenda`)";
		}

		$sql = implode(' UNION ALL ', $queries);

		$sql = "SELECT dia, SUM(valor) AS valor, mes, financeiro_tipo
				FROM ($sql) as subquery
				WHERE dia > DAY(CURRENT_DATE) - 10
				GROUP BY dia, financeiro_tipo
				ORDER BY dia
				DESC
				LIMIT 7";

		return $this->db->query($sql)->result();
	}
}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */
