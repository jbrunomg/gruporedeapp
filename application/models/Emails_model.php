<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails_model extends CI_Model {


	public function pegarEmailsCron()
	{		
		$this->db->select('*');				
		$this->db->where('maladireta_enviar_enviado', null);
		$this->db->limit(400);
		return $this->db->get('maladireta_enviar')->result_array();
	}

	public function editarcron($id,$dados)
	{
		$valor = ['maladireta_enviar_enviado'=> $dados];
		
		$this->db->where('maladireta_enviar_id', $id);
		$this->db->update('maladireta_enviar', $valor);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

}

/* End of file Emails_model.php */
/* Location: ./application/models/Emails_model.php */