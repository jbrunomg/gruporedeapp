<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faturamento_model extends CI_Model {

	private $base  =  '';

	function __construct($foo = null) {
        $this->base =  $this->session->userdata('empresa_nome_base');
    }

    /**
     * [pegarContasAPagar description] -- Financeiro -- Previsão (ANTIGO)
     * @return [type] [description]
     */

    /*
	public function previsaoFinanceiro()
    {
        $query = " SELECT view_financeiro_previsao.* ,ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual FROM view_financeiro_previsao
                    WHERE mes = MONTH(CURDATE())
                    AND ano = YEAR(CURDATE())
                    GROUP BY loja 
                    ORDER BY num ";

        return $this->db->query($query)->result();
    }
    */

    /**
     * [pegarContasAPagar description] -- Financeiro -- Previsão - Detalhamento (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function previsaoFinanceiroDetalhe()
    {
        $query = " SELECT *  FROM view_financeiro_previsao
                    WHERE mes = MONTH(CURDATE())
                    AND ano = YEAR(CURDATE())
                    ORDER BY num ";

        return $this->db->query($query)->result();
    }
    */


    /**
     * [pegarContasAPagar description] -- Financeiro -- Previsão -- Ano (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function previsaoFinanceiroAno()
    {
        $query = " SELECT loja, label, `ano`,
                 SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo,
                 ROUND(AVG(media),2) AS media, 
                 ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual FROM view_financeiro_previsao
                    WHERE
                    ano = YEAR(CURDATE())
                    GROUP BY loja  										ORDER BY num ";

        return $this->db->query($query)->result();
    }
    */

    /**
     * [pegarContasAPagar description] -- Financeiro -- Previsão -- Ano -- Detalhe (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function previsaoFinanceiroDetalheAno($loja)
    {
        $query = " SELECT loja, label, `ano`,
                 SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo,
                 ROUND(AVG(media),2) AS media, 
                 ROUND( ( (  (  (SUM(`receita`)/SUM(`custo`)) *100) -100) /100) ,2) AS percentual,
                 ROUND(((0.0 * SUM(`receita`))/100),2) AS comissao 
                 FROM view_financeiro_previsao
                WHERE loja = '{$loja}'
                GROUP BY loja, ano 
                ORDER BY ano DESC  ";

        return $this->db->query($query)->result();
    }
    */

    public function previsaoFinanceiro()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`) AS `dias_lancamento`,
            MONTH(`$b`.`financeiro`.`data_vencimento`) AS `mes`,
            YEAR(`$b`.`financeiro`.`data_vencimento`) AS `ano`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro`
            WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro` WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
            (SELECT ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`
            FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`mes` = MONTH(`$b`.`vendas`.`dataVenda`)) AND (`ano` = YEAR(`$b`.`vendas`.`dataVenda`)))
            GROUP BY MONTH(`$b`.`vendas`.`dataVenda`)
            ORDER BY MONTH(`$b`.`vendas`.`dataVenda`)) AS `custo`,
            (SELECT ROUND((`receita` / COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`)), 2)) AS `media`,
            (SELECT ROUND(((0.0 * `receita`) / 100), 2)) AS `comissao`
            FROM `$b`.`financeiro`
            WHERE ((MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
            GROUP BY MONTH(`$b`.`financeiro`.`data_vencimento`) , YEAR(`$b`.`financeiro`.`data_vencimento`)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT *, ((((SUM(`receita`) / SUM(`custo`)) * 100) - 100) / 100) AS percentual
                FROM ($sql) as subquery
                WHERE mes = MONTH(CURDATE())
                AND ano = YEAR(CURDATE())
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function previsaoFinanceiroDetalhe()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`) AS `dias_lancamento`,
            MONTH(`$b`.`financeiro`.`data_vencimento`) AS `mes`,
            YEAR(`$b`.`financeiro`.`data_vencimento`) AS `ano`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro`
            WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro`
            WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
            (SELECT ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`
            FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`mes` = MONTH(`$b`.`vendas`.`dataVenda`)) AND (`ano` = YEAR(`$b`.`vendas`.`dataVenda`)))
            GROUP BY MONTH(`$b`.`vendas`.`dataVenda`)
            ORDER BY MONTH(`$b`.`vendas`.`dataVenda`)) AS `custo`,
            (SELECT ROUND((`receita` / COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`)), 2)) AS `media`,
            (SELECT ROUND(((0.0 * `receita`) / 100), 2)) AS `comissao`
            FROM `$b`.`financeiro`
            WHERE ((MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
            GROUP BY MONTH(`$b`.`financeiro`.`data_vencimento`) , YEAR(`$b`.`financeiro`.`data_vencimento`)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT *
                FROM ($sql) as subquery 
                WHERE mes = MONTH(CURDATE())
                AND ano = YEAR(CURDATE())
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function previsaoFinanceiroAno()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`) AS `dias_lancamento`,
            MONTH(`$b`.`financeiro`.`data_vencimento`) AS `mes`,
            YEAR(`$b`.`financeiro`.`data_vencimento`) AS `ano`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro`
            WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
            (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
            FROM `$b`.`financeiro`
            WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
            (SELECT ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`
            FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`mes` = MONTH(`$b`.`vendas`.`dataVenda`)) AND (`ano` = YEAR(`$b`.`vendas`.`dataVenda`)))
            GROUP BY MONTH(`$b`.`vendas`.`dataVenda`)
            ORDER BY MONTH(`$b`.`vendas`.`dataVenda`)) AS `custo`,
            (SELECT ROUND((`receita` / COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`)), 2)) AS `media`,
            (SELECT ROUND(((0.0 * `receita`) / 100), 2)) AS `comissao`
            FROM `$b`.`financeiro`
            WHERE ((MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
            GROUP BY MONTH(`$b`.`financeiro`.`data_vencimento`) , YEAR(`$b`.`financeiro`.`data_vencimento`) ";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, label, `ano`, SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo, ROUND(AVG(media), 2) AS media, ((((SUM(`receita`) / SUM(`custo`)) * 100) - 100) / 100) AS percentual
                FROM ($sql) as subquery
                WHERE ano = YEAR(CURDATE())
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function previsaoFinanceiroDetalheAno($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`, 'Red' AS `label`,
        COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`) AS `dias_lancamento`,
        MONTH(`$b`.`financeiro`.`data_vencimento`) AS `mes`,
        YEAR(`$b`.`financeiro`.`data_vencimento`) AS `ano`,
        (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
        FROM `$b`.`financeiro`
        WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
        (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
        FROM `$b`.`financeiro`
        WHERE ((`mes` = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (`ano` = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
        (SELECT ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `total`
        FROM (`$b`.`itens_de_vendas` JOIN `$b`.`vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`mes` = MONTH(`$b`.`vendas`.`dataVenda`)) AND (`ano` = YEAR(`$b`.`vendas`.`dataVenda`)))
        GROUP BY MONTH(`$b`.`vendas`.`dataVenda`)
        ORDER BY MONTH(`$b`.`vendas`.`dataVenda`)) AS `custo`,
        (SELECT ROUND((`receita` / COUNT(DISTINCT `$b`.`financeiro`.`data_vencimento`)), 2) ) AS `media`,
        (SELECT ROUND(((0.0 * `receita`) / 100), 2)) AS `comissao`
        FROM `$b`.`financeiro`
        WHERE ((MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(`$b`.`financeiro`.`data_vencimento`)) AND (YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(`$b`.`financeiro`.`data_vencimento`)) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
        GROUP BY MONTH(`$b`.`financeiro`.`data_vencimento`), YEAR(`$b`.`financeiro`.`data_vencimento`)";

        $sql = "SELECT loja, label, `ano`, SUM(dias_lancamento) AS dias_lancamento, SUM(receita) AS receita, SUM(despesa) AS despesa, SUM(custo) AS custo, ROUND(AVG(media), 2) AS media, ROUND(((((SUM(`receita`) / SUM(`custo`)) * 100) - 100) / 100), 2) AS percentual, ROUND(((0.0 * SUM(`receita`))/100), 2) AS comissao 
                FROM ($sql) as subquery
                GROUP BY loja, ano 
                ORDER BY ano
                DESC";

        return $this->db->query($sql)->result();
    }

    private function pegarBaseDaLojaPelaBasePrincipal($loja)
    {
        $basePrincipal = BDCAMINHO;
        $basePrincipal = explode("_", $basePrincipal);
        $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

        $base = $basePrincipal . strtolower($loja);

        return $base;
    }
}

/* End of file Financeiro_model.php */
/* Location: ./application/models/Financeiro_model.php */