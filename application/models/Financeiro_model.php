<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model {

	private $base  =  '';

	function __construct($foo = null) { 
        $this->base =  $this->session->userdata('empresa_nome_base');
    }

    

    /**
     * [Financeiro description] -- Financeiro Dia (ANTIGO)
     * @return [type] [description]
     */

  /*
	public function financeiroDia()
    {     

        $query = "  SELECT                     
                    view_financeiro_dia.*,
                    ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
                     FROM view_financeiro_dia
                      GROUP BY loja
                    ORDER BY num ";


        return $this->db->query($query)->result();
    }
  */



     /**
     * [Financeiro description] -- Financeiro Mes (ANTIGO)
     * @return [type] [description]
     */

    /*
      public function financeiroMes()
      {
       
        
        $query = "  SELECT                     
                    view_financeiro_mes.*,
                    ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
                     FROM view_financeiro_mes
                      GROUP BY loja
                    ORDER BY num ";


        return $this->db->query($query)->result();
      }
    */

     /**
     * [Financeiro description] -- Financeiro Ano (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function financeiroAno()
      {
               
       $query = "  SELECT                     
            view_financeiro_ano.*,
            ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
             FROM view_financeiro_ano
              GROUP BY loja
            ORDER BY num ";


        return $this->db->query($query)->result();
      }
  */

    /**
     * [Financeiro description] -- Financeiro Dia - Detalhe (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function financeiroDiaDetalhe($loja)
      {     
        
      
        $query = "  SELECT                     
                view_financeiro_dia.*,
                ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
                 FROM view_financeiro_dia
                  WHERE loja = '{$loja}'
                  GROUP BY loja
                ORDER BY num ";


        return $this->db->query($query)->result();
      }
  */



    /**
     * [Financeiro description] -- Financeiro Mes - Detalhe (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function financeiroMesDetalhe($loja)
      {     
        
      
        $query = "  SELECT                     
                view_financeiro_mes.*,
                ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
                 FROM view_financeiro_mes
                  WHERE loja = '{$loja}'
                  GROUP BY loja
                ORDER BY num ";


        return $this->db->query($query)->result();
      }
  */

     /**
     * [Financeiro description] -- Financeiro Ano - Detalhe (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function financeiroAnoDetalhe($loja)
      {
       
        $query = "  SELECT                     
            view_financeiro_ano.*,
            ROUND( ( (  (  (SUM(`receita`)/SUM(`despesa`)) *100) -100) /100) ,2) AS percentual
             FROM view_financeiro_ano
              WHERE loja = '{$loja}'
              GROUP BY loja
            ORDER BY num ";

        // $query = " SELECT loja, ano, SUM(`qtVendas`) AS qtVendas,  SUM(total) AS total, SUM(custo) AS custo, SUM(lucro) AS lucro, ROUND(AVG(`percentual`),2) AS percentual
        //              FROM view_vendas_{$loja} 
        //             GROUP BY ano 
        //              ORDER BY ano DESC    ";


        return $this->db->query($query)->result();
      }
  */

  public function financeiroDia()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      `$b`.`financeiro`.`data_vencimento` AS `dia`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`),2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
      (SELECT ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
      FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
      WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`produto_avarias`.`produto_avaria_cadastro`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`vendas`.`dataVenda`) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
      (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
      FROM `$b`.`sangria_pdv`
      WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
      (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0)) ) AS `saldo`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
      GROUP BY `$b`.`financeiro`.`data_vencimento`";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT *, ((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja
            ORDER BY num";

    return $this->db->query($sql)->result();
  }

  public function financeiroMes()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      `$b`.`financeiro`.`data_vencimento` AS `dia`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
      (SELECT ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
      FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
      WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`produto_avarias`.`produto_avaria_cadastro`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`vendas`.`dataVenda`) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
      (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
      FROM `$b`.`sangria_pdv`
      WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
      (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0))) AS `saldo`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
      GROUP BY YEAR(CURDATE()), MONTH(CURDATE())";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT *, ((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja
            ORDER BY num";

    return $this->db->query($sql)->result();
  }

  public function financeiroAno()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      `$b`.`financeiro`.`data_vencimento` AS `dia`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
      (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
      (SELECT ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
      FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
      WHERE ((YEAR(`$b`.`produto_avarias`.`produto_avaria_cadastro`) = YEAR(CURDATE())) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
      (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
      FROM `$b`.`sangria_pdv`
      WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
      (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0)) ) AS `saldo`
      FROM `$b`.`financeiro`
      WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
      GROUP BY YEAR(CURDATE()) , MONTH(CURDATE())";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT *, ((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja
            ORDER BY num";

    return $this->db->query($sql)->result();
  }

  public function financeiroDiaDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`,
    `$b`.`financeiro`.`data_vencimento` AS `dia`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
    (SELECT ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
    FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
    WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`produto_avarias`.`produto_avaria_cadastro`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`vendas`.`dataVenda`) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
    (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
    FROM `$b`.`sangria_pdv`
    WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
    (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0))) AS `saldo`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = CURDATE()) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
    GROUP BY `$b`.`financeiro`.`data_vencimento`";

    $sql = "SELECT *, ROUND(((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100), 2) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function financeiroMesDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`,
    `$b`.`financeiro`.`data_vencimento` AS `dia`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
    (SELECT ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
    FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
    WHERE ((`$b`.`financeiro`.`data_vencimento` = `$b`.`produto_avarias`.`produto_avaria_cadastro`) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`vendas`.`dataVenda`) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
    (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
    FROM `$b`.`sangria_pdv`
    WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
    (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0))) AS `saldo`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_vencimento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
    GROUP BY YEAR(CURDATE()) , MONTH(CURDATE())";

    $sql = "SELECT *, ROUND(((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100), 2) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function financeiroAnoDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`,
    `$b`.`financeiro`.`data_vencimento` AS `dia`,
    (SELECT  ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `receita`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_pagamento`) = YEAR(CURDATE())) AND (MONTH(`$b`.`financeiro`.`data_pagamento`) = MONTH(CURDATE())) AND (`$b`.`financeiro`.`data_vencimento` <> `$b`.`financeiro`.`data_pagamento`) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita') AND (`$b`.`financeiro`.`financeiro_baixado` = 1) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `pag_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_baixado` = 0) AND (`$b`.`financeiro`.`financeiro_visivel` = 1) AND (`$b`.`financeiro`.`financeiro_tipo` = 'receita'))) AS `comp_fiado`,
    (SELECT ROUND(SUM(`$b`.`financeiro`.`financeiro_valor`), 2) AS `valorTotal`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_tipo` = 'despesa') AND (`$b`.`financeiro`.`financeiro_visivel` = 1))) AS `despesa`,
    (SELECT  ROUND(SUM((`$b`.`produto_avarias`.`produto_avaria_preco_vendido` * `$b`.`produto_avarias`.`produto_avarias_quantidade`)), 2) AS `valorTotal`
    FROM (`$b`.`produto_avarias` JOIN `$b`.`vendas` ON ((`$b`.`produto_avarias`.`venda_id` = `$b`.`vendas`.`idVendas`)))
    WHERE ((YEAR(`$b`.`produto_avarias`.`produto_avaria_cadastro`) = YEAR(CURDATE())) AND (`$b`.`produto_avarias`.`produto_avaria_motivo` IN (2 , 3)))) AS `reembolso`,
    (SELECT ROUND(SUM(`$b`.`sangria_pdv`.`valor`), 2) AS `valor`
    FROM `$b`.`sangria_pdv`
    WHERE (`$b`.`sangria_pdv`.`dataRetirada` >= `$b`.`financeiro`.`data_vencimento`)) AS `sangria`,
    (SELECT (((`receita` + IF((`pag_fiado` IS NOT NULL), `pag_fiado`, 0)) - (IF((`despesa` IS NOT NULL), `despesa`, 0) + IF((`reembolso` IS NOT NULL), `reembolso`, 0))) - IF((`sangria` IS NOT NULL), `sangria`, 0))) AS `saldo`
    FROM `$b`.`financeiro`
    WHERE ((YEAR(`$b`.`financeiro`.`data_vencimento`) = YEAR(CURDATE())) AND (`$b`.`financeiro`.`financeiro_visivel` = 1))
    GROUP BY YEAR(CURDATE()) , MONTH(CURDATE())";

    $sql = "SELECT *, ROUND(((((SUM(`receita`) / SUM(`despesa`)) * 100) - 100) / 100), 2) AS percentual
            FROM ($sql) as subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  private function pegarBaseDaLojaPelaBasePrincipal($loja)
  {
    $basePrincipal = BDCAMINHO;
    $basePrincipal = explode("_", $basePrincipal);
    $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

    $base = $basePrincipal . strtolower($loja);

    return $base;
  }
}

/* End of file Financeiro_model.php */
/* Location: ./application/models/Financeiro_model.php */