<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Os_model extends CI_Model {

	private $base  =  '';

	function __construct($foo = null) { 
        $this->base =  $this->session->userdata('empresa_nome_base');
    }

    

    /**
     * [Os description] -- Os Dia (ANTIGO)
     * @return [type] [description]
     */

  /*
	public function osDia()
    {     

      $query = "  SELECT * FROM `view_os_dia` ";  


      return $this->db->query($query)->result();
    }
  */


     /**
     * [Os description] -- Os Mes (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function osMes()
      {
               
      $query = "  SELECT * FROM `view_os_mes` ";


        return $this->db->query($query)->result();
      }
  */

    /**
     * [Os description] -- Os Ano (ANTIGO)
     * @return [type] [description]
     */

  /*
      public function osAno()
      {
               
      $query = "  SELECT * FROM `view_os_ano` ";

        return $this->db->query($query)->result();
      }
  */










     /**
     * [Os description] -- Os Mes - Detalhe
     * @return [type] [description]
     */

      public function osMesDetalhe($loja)
      {
               
        $query = " SELECT * FROM view_os_{$loja} WHERE ano = YEAR(CURDATE()) ORDER BY mes DESC  ";


        return $this->db->query($query)->result();
      }


    /**
     * [Os description] -- Os Mes - Detalhe
     * @return [type] [description]
     */

      public function vendasMesDetalheAnoAnterior($loja)
      {
               
        $query = " SELECT * FROM view_vendas_{$loja} WHERE ano = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) ORDER BY mes DESC  ";


        return $this->db->query($query)->result();
      }



      public function vendasAnoAnterior()
      {
       
        
      //  $query = "  SELECT * FROM `view_vendas_ano_anterior` ";

      $query = "  SELECT  loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`,  SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
                FROM `view_vendas_ano_anterior`  
                GROUP BY loja ";   


        return $this->db->query($query)->result();
      }

      public function financeiroAno()
      {
       
        
        $query = "  SELECT * FROM `view_vendas_ano` ";


        return $this->db->query($query)->result();
      }


     /**
     * [Venda description] -- Venda Ano - Detalhe
     * @return [type] [description]
     */

      public function vendasAnoDetalhe($loja)
      {
       
        
        $query = " SELECT loja, ano, SUM(`qtVendas`) AS qtVendas,  SUM(total) AS total, SUM(custo) AS custo, SUM(lucro) AS lucro, ROUND(AVG(`percentual`),2) AS percentual
                     FROM view_vendas_{$loja} 
                    GROUP BY ano 
                     ORDER BY ano DESC    ";


        return $this->db->query($query)->result();
      }





      public function VendasMensalGrupo($loja, $ano, $mes = null, $dia = null)
      {
        $base = strtolower($loja);


        $this->db->select('  `categoria_prod_descricao` AS grupo,
        ROUND(SUM(`subTotal`), 2) AS total, 
        ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
        $this->db->join('mixcel17_'.GRUPOLOJA.'_'.$base.'.vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
        $this->db->join('mixcel17_'.GRUPOLOJA.'_'.$base.'.produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
        $this->db->join('mixcel17_'.GRUPOLOJA.'_'.$base.'.categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
       // $this->db->join('mixcel17_megacell_'.$base.'.financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
        $this->db->where('venda_visivel',1);
       // $this->db->where('faturado',1);
       // $this->db->where('financeiro.`financeiro_visivel`',1);
       // $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
       // $this->db->where('financeiro.`financeiro_tipo`', 'receita');
            $this->db->where('`itens_de_vendas`.filial', $base);
          
            $this->db->where('YEAR(dataVenda)', $ano);

            if ($mes <> null) {
              $this->db->where('MONTH(dataVenda)', $mes); 
                 
            }

            if ($dia <> null) {
              $this->db->where('DAY(dataVenda)', $dia); 
                 
            }
          

            $this->db->group_by('categoria_prod_id');
            return $this->db->get('mixcel17_'.GRUPOLOJA.'_'.$base.'.itens_de_vendas')->result_array();

      }

  public function osDia()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`os`.`os_id`) AS `osQtd`,
      SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) AS `totalServ`,
      SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `totalProd`,
      SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `custoTotal`,
      (((((SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) + SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) / SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`os` WHERE ((`$b`.`os`.`os_datafaturado` = CURDATE()) AND (`$b`.`os`.`os_visivel` = 1))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT * FROM ($sql) AS subquery";

    return $this->db->query($sql)->result();
  }

  public function osMes()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`os`.`os_id`) AS `osQtd`,
      SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) AS `totalServ`,
      SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `totalProd`,
      SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `custoTotal`,
      (((((SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) + SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM  `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) / SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`os` WHERE ((MONTH(`$b`.`os`.`os_datafaturado`) = MONTH(CURDATE())) AND (YEAR(`$b`.`os`.`os_datafaturado`) = YEAR(CURDATE())) AND (`$b`.`os`.`os_visivel` = 1))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT * FROM ($sql) AS subquery";

    return $this->db->query($sql)->result();
  }

  public function osAno()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`os`.`os_id`) AS `osQtd`,
      SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) AS `totalServ`,
      SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `totalProd`,
      SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `custoTotal`,
      (((((SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) + SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) / SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`os` WHERE ((YEAR(`$b`.`os`.`os_datafaturado`) = YEAR(CURDATE())) AND (`$b`.`os`.`os_visivel` = 1))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT * FROM ($sql) AS subquery";

    return $this->db->query($sql)->result();
  }

  public function emAberto($dias)
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`os`.`os_id`) AS `osQtd`,
      SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) AS `totalServ`,
      SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `totalProd`,
      SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`))) AS `custoTotal`,
      (((((SUM((SELECT SUM(`$b`.`itens_de_os_servicos`.`servico_os_subTotal`) FROM `$b`.`itens_de_os_servicos` WHERE (`$b`.`itens_de_os_servicos`.`os_id` = `$b`.`os`.`os_id`))) + SUM((SELECT SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) / SUM((SELECT SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` = `$b`.`os`.`os_id`)))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`os`
      WHERE (`$b`.`os`.`os_dataInicial` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`os`.`os_visivel` = 1) AND (`$b`.`os`.`os_faturado` = 0) AND (`$b`.`os`.`os_status` = '1- AGUARDANDO ORÇAMENTO')";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT * FROM ($sql) AS subquery";

    return $this->db->query($sql)->result();
  }

  public function emAbertoDetalhe($loja, $dias)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT
      `$b`.`os`.`os_id` AS `osId`,
      `$b`.`os`.`os_dataInicial` AS `dataInicial`,
      `$b`.`os`.`os_status` AS `status`,
      `$b`.`os`.`os_equipamento` AS `equipamento`,
      `$b`.`clientes`.`cliente_nome` AS `cliente`
    FROM `$b`.`os`
    JOIN `$b`.`clientes` ON `$b`.`os`.`clientes_id` = `$b`.`clientes`.`cliente_id`
    WHERE (`$b`.`os`.`os_dataInicial` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`os`.`os_visivel` = 1) AND (`$b`.`os`.`os_faturado` = 0) AND (`$b`.`os`.`os_status` = '1- AGUARDANDO ORÇAMENTO')
    ORDER BY `$b`.`os`.`os_dataInicial`
    DESC
    LIMIT 20";

    return $this->db->query($sql)->result();
  }

  public function quantidadesOs(array $todosDias)
	{
		$bases = SGBD;
		$resultados = [];

		foreach ($todosDias as $dias) {
			$queries = [];

			foreach ($bases as $b) {
				$queries[] = 
				"SELECT
					COUNT(DISTINCT `$b`.`os`.`os_id`) AS `osQtd`
				FROM `$b`.`os`
				WHERE (`$b`.`os`.`os_dataInicial` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`os`.`os_visivel` = 1) AND (`$b`.`os`.`os_faturado` = 0) AND (`$b`.`os`.`os_status` = '1- AGUARDANDO ORÇAMENTO')";
			}

			$sql = implode(' UNION ALL ', $queries);

			$sql = "SELECT SUM(osQtd) as osQtdTotal FROM ($sql) AS subquery";

			$resultados[$dias] = $this->db->query($sql)->result();
		}

		return $resultados;
	}

  private function pegarBaseDaLojaPelaBasePrincipal($loja)
  {
    $basePrincipal = BDCAMINHO;
    $basePrincipal = explode("_", $basePrincipal);
    $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

    $base = $basePrincipal . strtolower($loja);

    return $base;
  }
}

/* End of file Financeiro_model.php */
/* Location: ./application/models/Financeiro_model.php */