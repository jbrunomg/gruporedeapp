<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_model extends CI_Model {

	private $base  =  '';

    function __construct($foo = null) {
        $this->base =  $this->session->userdata('empresa_nome_base');
    }


    /**
     * [Produto description] -- Valor Estoque (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function valorEstoque()
    {   

        $query = "  SELECT * FROM `view_valor_estoque` ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * [Produto description] -- Valor Estoque Grupo (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function valorEstoqueGrupo($loja)
    {   
        
        $query = " SELECT view_valor_estoque_grupo.* ,
                   ROUND( ( ( (`produto_preco_venda`/`produto_preco_custo`) *100) -100) ,2) AS percentual
                   FROM `view_valor_estoque_grupo` where loja = '{$loja}' ";


        return $this->db->query($query)->result();
    }
    */

    public function valorEstoqueGrupoDetalhe($loja,$grupo)
    {
        // code...
        $servidorAtual = BDCAMINHO ;
        $servidorAtual = explode("_", $servidorAtual);
        $servidorAtual = $servidorAtual[0].'_'.$servidorAtual[1].'_';

        $lojaM = strtolower($loja);
        $query = " SELECT 
          '{$loja}' as loja,
          `categoria_produto`.`categoria_prod_descricao`,
          `produto`.`produto_descricao`,
          `produto`.`produto_estoque`,
          `produto`.`produto_preco_custo`,
          `produto`.`produto_preco_minimo_venda`,
          `produto`.`produto_preco_venda`,
          `produto`.`produto_preco_cart_debito` 
        FROM
          ".$servidorAtual.$lojaM.".`produto` 
          JOIN ".$servidorAtual.$lojaM.".`categoria_produto` 
            ON produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id` 
        WHERE `produto_visivel` = 1 
         --  AND `categoria_produto`.`categoria_prod_id` = '1' 
            AND `categoria_produto`.`categoria_prod_descricao` = '{$grupo}'
        ORDER BY `produto_descricao` ASC  ";


        return $this->db->query($query)->result();

    }



    /**
     * [Produto description] -- Mais Vendido (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisVendido()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_mes 
                    GROUP BY loja 										ORDER BY num ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Vendido Detalhe (ANTIGO)
     */

    /*
    public function maisVendidoDetalhe($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_mes.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_mes`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }
    */


    /**
     * [Produto description] -- Mais Vendido - Trimestre (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisVendidoT()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_trimestre 
                    GROUP BY loja 										ORDER BY num ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Vendido Detalhe - Trimestral (ANTIGO)
     */

    /*
    public function maisVendidoDetalheT($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_trimestre.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_trimestre`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }
    */


     /**
     * [Produto description] -- Mais Vendido - Semestre (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisVendidoS()
    {     

        $query = "  SELECT loja, SUM(qtd) AS qtd, SUM(venda_media*qtd) AS valorTotal, SUM(custo_medio*qtd) AS custoTotal,
                    ROUND( ( (  (  (SUM(`venda_media`)/SUM(`custo_medio`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_mais_vendidos_semestre 
                    GROUP BY loja 										ORDER BY num ";

        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Vendido Detalhe - Semestral (ANTIGO)
     */

    /*
    public function maisVendidoDetalheS($loja)
    {   
        
        $query = "  SELECT view_prod_mais_vendidos_semestre.*, (venda_media - custo_medio) AS lucro_medio,
                    ROUND( ( ( (`venda_media`/`custo_medio`) *100) -100) ,2) AS percentual
                    FROM `view_prod_mais_vendidos_semestre`
                    WHERE loja = '{$loja}' ORDER BY `qtd` DESC  ";


        return $this->db->query($query)->result();
    }
    */



    /**
     * [Produto description] -- Mais Parado (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisParado()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_mes 
                    GROUP BY loja 										ORDER BY num ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Vendido Detalhe (ANTIGO)
     */

    /*
    public function maisParadoDetalhe($loja)
    {     

        $query = " SELECT view_prod_parado_mes.*, (valorTotal - custoTotal ) AS lucro
                   FROM `view_prod_parado_mes` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * [Produto description] -- Mais Parado Trimestre (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisParadoT()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_trimestre 
                    GROUP BY loja 										ORDER BY num ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Parado Detalhe - Trimestral (ANTIGO)
     */

    /*
    public function maisParadoDetalheT($loja)
    {     

        $query = " SELECT view_prod_parado_trimestre.*, (valorTotal - custoTotal ) AS lucro
                   FROM `view_prod_parado_trimestre` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }
    */


    /**
     * [Produto description] -- Mais Parado Semestre (ANTIGO)
     * @return [type] [description]
     */

    /*
    public function maisParadoS()
    {     

        $query = " SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM view_prod_parado_semestre 
                    GROUP BY loja 										ORDER BY num ";


        return $this->db->query($query)->result();
    }
    */

    /**
     * Mais Parado Detalhe - Semestral (ANTIGO)
     */

    /*
    public function maisParadoDetalheS($loja)
    {     

        $query = " SELECT view_prod_parado_semestre.*, (valorTotal - custoTotal ) AS lucro
                   FROM `view_prod_parado_semestre` 
                   WHERE loja = '{$loja}' ORDER BY `estoque` DESC   ";


        return $this->db->query($query)->result();
    }
    */

    public function valorEstoque()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            SUM(`$b`.`produto`.`produto_estoque`) AS `estoqueQtd`,
            SUM((`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`)) AS `valorTotal`,
            SUM((`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`)) AS `custoTotal`,
            (SUM((`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`)) - SUM((`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`))) AS `lucro`,
            ((((SUM((`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`)) / SUM((`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`))) * 100) - 100) / 100) AS `percentual`
            FROM `$b`.`produto`
            WHERE ((`$b`.`produto`.`produto_estoque` > 0) AND (`$b`.`produto`.`produto_visivel` = 1))";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT * FROM ($sql) AS subquery";

        return $this->db->query($sql)->result();
    }

    public function maisVendido()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
            SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
            AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`) AS `custo_medio`,
            (SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)) AS `venda_media`
            FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
            GROUP BY `$b`.`produto`.`produto_descricao`
            ORDER BY `qtd`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtd) AS qtd, SUM(venda_media * qtd) AS valorTotal, SUM(custo_medio * qtd) AS custoTotal, ((((SUM(`venda_media`) / SUM(`custo_medio`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) AS subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisVendidoT()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Orange", "Yellow", "Blue", "Red", "Blue-light", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
            SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
            AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`) AS `custo_medio`,
            (SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)) AS `venda_media`
            FROM ((`$b`.`vendas`
            JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`)))
            JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`dataVenda` BETWEEN (CURDATE() - INTERVAL 90 DAY) AND CURDATE()))
            GROUP BY `$b`.`produto`.`produto_descricao`
            ORDER BY `qtd`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtd) AS qtd, SUM(venda_media * qtd) AS valorTotal, SUM(custo_medio * qtd) AS custoTotal, ((((SUM(`venda_media`) / SUM(`custo_medio`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisVendidoS()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
            SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
            AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`) AS `custo_medio`,
            (SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)) AS `venda_media`
            FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`)))
            JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
            WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`dataVenda` BETWEEN (CURDATE() - INTERVAL 180 DAY) AND CURDATE()))
            GROUP BY `$b`.`produto`.`produto_descricao`
            ORDER BY `qtd`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtd) AS qtd, SUM(venda_media * qtd) AS valorTotal, SUM(custo_medio * qtd) AS custoTotal, ((((SUM(`venda_media`)/SUM(`custo_medio`)) * 100) - 100) / 100) AS percentual, label 
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisParado()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `estoque`,
            (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
            (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
            FROM `$b`.`produto`
            WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 30 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
            ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisParadoT()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `estoque`,
            (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
            (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
            FROM `$b`.`produto`
            WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 90 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
            ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisParadoS()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `estoque`,
            (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
            (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
            FROM `$b`.`produto`
            WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 180 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
            ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual,  label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function valorEstoqueGrupo($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`categoria_produto`.`categoria_prod_descricao` AS `produto_descricao`,
        SUM(`$b`.`produto`.`produto_estoque`) AS `produto_estoque`,
        ROUND(SUM((`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`)), 2) AS `produto_preco_custo`,
        ROUND(SUM((`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`)), 2) AS `produto_preco_venda`,
        ROUND((SUM((`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`)) - SUM((`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`))), 2) AS `lucro`
        FROM (`$b`.`produto` JOIN `$b`.`categoria_produto` ON ((`$b`.`produto`.`produto_categoria_id` = `$b`.`categoria_produto`.`categoria_prod_id`)))
        WHERE (`$b`.`produto`.`produto_visivel` = 1)
        GROUP BY `$b`.`produto`.`produto_categoria_id`";

        $sql = "SELECT *, ROUND((((`produto_preco_venda` / `produto_preco_custo`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery";

        return $this->db->query($sql)->result();
    }

    public function maisVendidoDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
        SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
        ROUND(AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`), 2) AS `custo_medio`,
        ROUND((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)), 2) AS `venda_media`
        FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())))
        GROUP BY `$b`.`produto`.`produto_descricao`
        ORDER BY `qtd`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (venda_media - custo_medio) AS lucro_medio, ROUND((((`venda_media` / `custo_medio`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `qtd` DESC";

        return $this->db->query($sql)->result();
    }

    public function maisVendidoDetalheT($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
        SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
        ROUND(AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`), 2) AS `custo_medio`,
        ROUND((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)), 2) AS `venda_media`
        FROM ((`$b`.`vendas`JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`dataVenda` BETWEEN (CURDATE() - INTERVAL 90 DAY) AND CURDATE()))
        GROUP BY `$b`.`produto`.`produto_descricao`
        ORDER BY `qtd`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (venda_media - custo_medio) AS lucro_medio, ROUND((((`venda_media` / `custo_medio`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `qtd`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function maisVendidoDetalheS($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `produto_estoque`,
        SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
        ROUND(AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`), 2) AS `custo_medio`,
        ROUND((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)), 2) AS `venda_media`
        FROM ((`$b`.`vendas` JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) JOIN `$b`.`produto` ON ((`$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id`)))
        WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`dataVenda` BETWEEN (CURDATE() - INTERVAL 180 DAY) AND CURDATE()))
        GROUP BY `$b`.`produto`.`produto_descricao`
        ORDER BY `qtd`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (venda_media - custo_medio) AS lucro_medio, ROUND((((`venda_media` / `custo_medio`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `qtd`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function maisParadoDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `estoque`,
        (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
        (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
        FROM `$b`.`produto`
        WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 30 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
        ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro
                FROM ($sql) as subquery 
                ORDER BY `estoque`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function maisParadoDetalheT($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `estoque`,
        (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
        (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
        FROM `$b`.`produto`
        WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 90 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
        ORDER BY `$b`.`produto`.`produto_data_ultima_venda` DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal ) AS lucro
                FROM ($sql) as subquery
                ORDER BY `estoque`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function maisParadoDetalheS($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `estoque`,
        (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
        (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
        FROM `$b`.`produto`
        WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL 180 DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
        ORDER BY `$b`.`produto`.`produto_data_ultima_venda` DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal ) AS lucro
                FROM ($sql) as subquery
                ORDER BY `estoque`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function maisParadoGenerico($dias)
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
            `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
            `$b`.`produto`.`produto_estoque` AS `estoque`,
            (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
            (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
            FROM `$b`.`produto`
            WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
            ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
            DESC
            LIMIT 20)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(estoque) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function maisParadoGenericoDetalhe($loja, $dias)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_data_ultima_venda` AS `produto_data_ultima_venda`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,
        `$b`.`produto`.`produto_estoque` AS `estoque`,
        (`$b`.`produto`.`produto_preco_custo` * `$b`.`produto`.`produto_estoque`) AS `custoTotal`,
        (`$b`.`produto`.`produto_preco_venda` * `$b`.`produto`.`produto_estoque`) AS `valorTotal`
        FROM `$b`.`produto`
        WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))
        ORDER BY `$b`.`produto`.`produto_data_ultima_venda`
        DESC
        LIMIT 20";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro
                FROM ($sql) as subquery 
                ORDER BY `estoque`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function quantidadesProdutosParados(array $todosDias)
    {
		$bases = SGBD;
		$resultados = [];

		foreach ($todosDias as $dias) {
			$queries = [];

			foreach ($bases as $b) {
				$queries[] = 
				"SELECT
					COUNT(DISTINCT `$b`.`produto`.`produto_id`) AS `produtoQtd`
				FROM `$b`.`produto`
				WHERE ((`$b`.`produto`.`produto_data_ultima_venda` < (CURDATE() - INTERVAL $dias DAY)) AND (`$b`.`produto`.`produto_visivel` = 1) AND (`$b`.`produto`.`produto_estoque` <> '0'))";
			}

			$sql = implode(' UNION ALL ', $queries);

			$sql = "SELECT SUM(produtoQtd) as produtoQtdTotal FROM ($sql) AS subquery";

			$resultados[$dias] = $this->db->query($sql)->result();
		}

		return $resultados;
    }

    private function pegarBaseDaLojaPelaBasePrincipal($loja)
    {
        $basePrincipal = BDCAMINHO;
        $basePrincipal = explode("_", $basePrincipal);
        $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

        $base = $basePrincipal . strtolower($loja);

        return $base;
    }
}

/* End of file Produto_model.php */
/* Location: ./application/models/Produto_model.php */