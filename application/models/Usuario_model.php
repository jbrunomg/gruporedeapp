<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	private $tabela  = 'usuarios';
	private $id      = 'usuario_id';
	private $visivel = 'usuario_visivel';


	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('perfis')->result();		
	}


    


    public function listar()
	{
		$this->db->select('*');
		$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}









	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}




}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */