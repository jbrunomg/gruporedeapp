<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venda_model extends CI_Model {

	private $base  =  '';

	function __construct($foo = null) { 
        $this->base =  $this->session->userdata('empresa_nome_base');
    }

    

    /**
     * [Venda description] -- Venda Dia (ANTIGO)
     * @return [type] [description]
     */

  /*
  public function vendasDia()
    {     

      //  $query = "  SELECT * FROM `view_vendas_dia` ";

      $query = "    SELECT  loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`,  SUM(`itensQtd`) AS `itensQtd`,  SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
                FROM view_vendas_dia  
                GROUP BY loja ";


        return $this->db->query($query)->result();
    }
  */

     /**
     * [Venda description] -- Venda Mes (ANTIGO)
     * @return [type] [description]
     */

     /*
      public function vendasMes()
      {
       
        
      //  $query = "  SELECT * FROM `view_vendas_mes` ";


      $query = "    SELECT  loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`, SUM(`itensQtd`) AS `itensQtd`,  SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
                FROM `view_vendas_mes`  
                GROUP BY loja ";


        return $this->db->query($query)->result();
      }
    */



     /**
     * [Venda description] -- Venda Mes - Detalhe (ANTIGO)
     * @return [type] [description]
     */

    /*
      public function vendasMesDetalhe($loja)
      {
       
        
        $query = " SELECT * FROM view_vendas_{$loja} WHERE ano = YEAR(CURDATE()) ORDER BY mes DESC  ";


        return $this->db->query($query)->result();
      }
    */


    /**
     * [Venda description] -- Venda Mes - Detalhe (ANTIGO)
     * @return [type] [description]
     */

    /*
      public function vendasMesDetalheAnoAnterior($loja)
      {
       
        
        $query = " SELECT * FROM view_vendas_{$loja} WHERE ano = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) ORDER BY mes DESC  ";


        return $this->db->query($query)->result();
      }
    */


     /**
     * [Venda description] -- Venda Ano (ANTIGO)
     * @return [type] [description]
     */

    /*
      public function vendasAno()
      {
       
        
      //  $query = "  SELECT * FROM `view_vendas_ano` ";

      $query = "  SELECT  loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`, SUM(`itensQtd`) AS `itensQtd`, SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
                FROM `view_vendas_ano`  
                GROUP BY loja ";  


        return $this->db->query($query)->result();
      }
    */

    /**
     * Vendas Ano Anterior (ANTIGO)
     */

    /*
      public function vendasAnoAnterior()
      {
       
        
      //  $query = "  SELECT * FROM `view_vendas_ano_anterior` ";

      $query = "  SELECT  loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`, SUM(`itensQtd`) AS `itensQtd`, SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
                FROM `view_vendas_ano_anterior`  
                GROUP BY loja ";   


        return $this->db->query($query)->result();
      }
    */

      public function financeiroAno()
      {
       
        
        $query = "  SELECT * FROM `view_vendas_ano` ";


        return $this->db->query($query)->result();
      }


     /**
     * [Venda description] -- Venda Ano - Detalhe (ANTIGO)
     * @return [type] [description]
     */

     /*
      public function vendasAnoDetalhe($loja)
      {
       
        
        $query = " SELECT loja, ano, SUM(`qtVendas`) AS qtVendas, SUM(`itensQtd`) AS `itensQtd`, SUM(total) AS total, SUM(custo) AS custo, SUM(lucro) AS lucro, ROUND(AVG(`percentual`),2) AS percentual
                     FROM view_vendas_{$loja} 
                    GROUP BY ano 
                     ORDER BY ano DESC    ";


        return $this->db->query($query)->result();
      }
    */





    public function VendasMensalGrupo($loja, $ano, $mes = null, $dia = null)
    {
      // code...
      $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);
      // $servidorAtual = BDCAMINHO ;
      // $servidorAtual = explode("_", $servidorAtual);
      // $servidorAtual = $servidorAtual[0].'_'.$servidorAtual[1].'_';


      $base = strtolower($loja);

     // var_dump($mes);die();

      $this->db->select('  `categoria_prod_descricao` AS grupo,
      ROUND(SUM(`subTotal`), 2) AS total, 
      ROUND(SUM(prod_preco_custo  * `quantidade`), 2) AS custo ');
      $this->db->join($b.'.vendas','vendas.`idVendas` = itens_de_vendas.`vendas_id`' );
      $this->db->join($b.'.produto','produto.`produto_id` = `itens_de_vendas`.`produtos_id`' );
      $this->db->join($b.'.categoria_produto','produto.`produto_categoria_id` = categoria_produto.`categoria_prod_id`' );
     // $this->db->join('mixcel17_megacell_'.$base.'.financeiro ','`vendas`.`idVendas` = `financeiro`.`vendas_id`' , 'left');
      $this->db->where('venda_visivel',1);
     // $this->db->where('faturado',1);
     // $this->db->where('financeiro.`financeiro_visivel`',1);
     // $this->db->where('financeiro.`financeiro_forma_pgto` is NOT NULL', NULL, FALSE);
     // $this->db->where('financeiro.`financeiro_tipo`', 'receita');
          $this->db->where('`itens_de_vendas`.filial', $base);
        
          $this->db->where('YEAR(dataVenda)', $ano);

          if ($mes <> null) {
            $this->db->where('MONTH(dataVenda)', $mes); 
               
          }

          if ($dia <> null) {
            $this->db->where('DAY(dataVenda)', $dia); 
               
          }
        

          $this->db->group_by('categoria_prod_id');
          return $this->db->get($b.'.itens_de_vendas')->result_array();

    }


    public function ClientesMensalGrupo($loja, $ano, $mes = null, $dia = null)
    {
      // code...
      $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);
 
      $base = strtolower($loja);

     // var_dump($mes);die();

      $this->db->select('  `cliente_nome`, SUM(`valorTotal`) AS total  ');

      $this->db->join($b.'.clientes','vendas.`clientes_id` = clientes.`cliente_id`' );

      $this->db->where('venda_visivel',1);     
    
      $this->db->where('YEAR(dataVenda)', $ano);

      if ($mes <> null) {
        $this->db->where('MONTH(dataVenda)', $mes);            
      }

      if ($dia <> null) {
        $this->db->where('DAY(dataVenda)', $dia);            
      }
    

      $this->db->group_by('clientes_id');
      $this->db->order_by('total','DESC');
      return $this->db->get($b.'.vendas')->result_array();

    }


    public function vendasGrupoDetalhe($grupo, $loja, $ano, $mes = null, $dia = null)
    {
      // code...
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`produto`.`produto_descricao` AS `produto_descricao`,

        SUM(DISTINCT `$b`.`produto`.`produto_estoque`) AS `produto_estoque`,

        SUM(`$b`.`itens_de_vendas`.`quantidade`) AS `qtd`,
        ROUND(AVG(`$b`.`itens_de_vendas`.`prod_preco_custo`), 2) AS `custo_medio`,
        ROUND((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM(`$b`.`itens_de_vendas`.`quantidade`)), 2) AS `venda_media`
        FROM `$b`.`vendas` 
          JOIN `$b`.`itens_de_vendas` ON `$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`
          JOIN `$b`.`produto` ON `$b`.`itens_de_vendas`.`produtos_id` = `$b`.`produto`.`produto_id` 
          JOIN   `$b`.`categoria_produto`  ON   `$b`.`produto`.`produto_categoria_id` = `$b`.`categoria_produto`.`categoria_prod_id`
        WHERE `$b`.`vendas`.`venda_visivel` = 1
          AND YEAR(`$b`.`vendas`.`dataVenda`) = $ano";

        if ($mes <> null) {
        $sql = $sql . "  AND MONTH(`$b`.`vendas`.`dataVenda`) = $mes";
        } 
        if ($dia <> null) {
        $sql = $sql . "  AND DAY(`$b`.`vendas`.`dataVenda`) = $dia";
        }      
       
        $sql = $sql . " AND  `$b`.`categoria_produto`.`categoria_prod_descricao` = '$grupo' ";

        if(GRUPOLOJA == 'grupocell') { 
        $sql = $sql . " GROUP BY `$b`.`produto`.`produto_codigo`";
        } else {
        $sql = $sql . "  GROUP BY `$b`.`produto`.`produto_descricao`";
        } 

        $sql = $sql . " 
        ORDER BY `qtd`
        DESC
        /* LIMIT 20*/ ";


        $sql = "SELECT *, (venda_media - custo_medio) AS lucro_medio, ROUND((((`venda_media` / `custo_medio`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `qtd` DESC";

        return $this->db->query($sql)->result();

    }





  public function vendasDia()
  {
    $bases = SGBD;
    $queries = [];

    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`vendas_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_vendas` WHERE (`$b`.`itens_de_vendas`.`vendas_id` IN (SELECT `$b`.`vendas`.`idVendas` FROM `$b`.`vendas` WHERE ((`$b`.`vendas`.`dataVenda` = CURDATE()) AND (`$b`.`vendas`.`venda_visivel` = 1))) AND (`$b`.`itens_de_vendas`.`filial` = '$loja')) 
      UNION ALL
      SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`os_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) / SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` IN (SELECT `$b`.`os`.`os_id` FROM `$b`.`os` WHERE ((`$b`.`os`.`os_datafaturado` = CURDATE()) AND (`$b`.`os`.`os_visivel` = 1))) AND (`$b`.`itens_de_os_produto`.`filial` = '$loja'))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`,  SUM(`itensQtd`) AS `itensQtd`,  SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
            FROM ($sql) AS subquery  
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function vendasMes()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`vendas_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_vendas` WHERE (`$b`.`itens_de_vendas`.`vendas_id` IN (SELECT `$b`.`vendas`.`idVendas` FROM `$b`.`vendas` WHERE ((MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1))) AND (`$b`.`itens_de_vendas`.`filial` = '$loja')) 
      UNION ALL
      SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`os_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) / SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` IN (SELECT `$b`.`os`.`os_id` FROM `$b`.`os` WHERE ((MONTH(`$b`.`os`.`os_datafaturado`) = MONTH(CURDATE())) AND (YEAR(`$b`.`os`.`os_datafaturado`) = YEAR(CURDATE())) AND (`$b`.`os`.`os_visivel` = 1))) AND (`$b`.`itens_de_os_produto`.`filial` = '$loja'))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`, SUM(`itensQtd`) AS `itensQtd`, SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
            FROM ($sql) AS subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function vendasAno()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `vendas_id`) AS `vendasQtd`,
      COUNT(DISTINCT `idItens`) AS `itensQtd`,
      SUM(`subTotal`) AS `valorTotal`,
      SUM((`prod_preco_custo` * `quantidade`)) AS `custoTotal`,
      ((((SUM(`subTotal`) / SUM((`prod_preco_custo` * `quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_vendas`
      WHERE (`vendas_id` IN (SELECT `idVendas` FROM `$b`.`vendas` WHERE (YEAR(`dataVenda`) = YEAR(CURDATE()) AND `venda_visivel` = 1)) AND `filial` = '$loja')
      UNION ALL
      SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `os_id`) AS `vendasQtd`,
      COUNT(DISTINCT `idItens`) AS `itensQtd`,
      SUM(`produtos_os_subTotal`) AS `valorTotal`,
      SUM((`prod_preco_custo` * `produtos_os_quantidade`)) AS `custoTotal`,
      ((((SUM(`produtos_os_subTotal`) / SUM((`prod_preco_custo` * `produtos_os_quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_os_produto`
      WHERE (`os_id` IN (SELECT `os_id` FROM `$b`.`os` WHERE (YEAR(`os_datafaturado`) = YEAR(CURDATE()) AND `os_visivel` = 1)) AND `filial` = '$loja')";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT loja, num, label, SUM(vendasQtd) AS vendasQtd, SUM(itensQtd) AS itensQtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual
            FROM ($sql) AS subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function vendasAnoAnterior()
  {
    $bases = SGBD;
    $queries = [];
    $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

    foreach ($bases as $indice => $b) {
      $base = explode('_', $b);
      $loja = strtoupper($base[2]);
      $label = $labels[$indice];

      $num = sprintf("%02d", $indice + 1);

      $queries[] = 
      "SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`vendas_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_vendas` WHERE (`$b`.`itens_de_vendas`.`vendas_id` IN (SELECT `$b`.`vendas`.`idVendas` FROM `$b`.`vendas` WHERE ((YEAR(`$b`.`vendas`.`dataVenda`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (`$b`.`vendas`.`venda_visivel` = 1))) AND (`$b`.`itens_de_vendas`.`filial` = '$loja')) 
      UNION ALL
      SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`os_id`) AS `vendasQtd`,
      COUNT(DISTINCT `$b`.`itens_de_os_produto`.`idItens`) AS `itensQtd`,
      SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) AS `valorTotal`,
      SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`)) AS `custoTotal`,
      ((((SUM(`$b`.`itens_de_os_produto`.`produtos_os_subTotal`) / SUM((`$b`.`itens_de_os_produto`.`prod_preco_custo` * `$b`.`itens_de_os_produto`.`produtos_os_quantidade`))) * 100) - 100) / 100) AS `percentual`
      FROM `$b`.`itens_de_os_produto` WHERE (`$b`.`itens_de_os_produto`.`os_id` IN (SELECT `$b`.`os`.`os_id` FROM `$b`.`os` WHERE ((YEAR(`$b`.`os`.`os_dataInicial`) = YEAR((CURDATE() - INTERVAL 1 YEAR))) AND (`$b`.`os`.`os_visivel` = 1))) AND (`$b`.`itens_de_os_produto`.`filial` = '$loja'))";
    }

    $sql = implode(' UNION ALL ', $queries);

    $sql = "SELECT loja, num, label, SUM(`vendasQtd`) AS `vendasQtd`, SUM(`itensQtd`) AS `itensQtd`, SUM(`valorTotal`) AS valorTotal, SUM(custoTotal) AS custoTotal, AVG(percentual) AS percentual 
            FROM ($sql) AS subquery
            GROUP BY loja";

    return $this->db->query($sql)->result();
  }

  public function vendasMesDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`, 'Blue-light' AS `label`,
    MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
    DATE_FORMAT(`$b`.`vendas`.`dataVenda`, '%b') AS `mese`,
    YEAR(`$b`.`vendas`.`dataVenda`) AS `ano`,
    COUNT(DISTINCT `$b`.`vendas`.`idVendas`) AS `qtVendas`,
    COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
    SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `total`,
    SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custo`,
    (SUM(`$b`.`itens_de_vendas`.`subTotal`) - SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) AS `lucro`,
    (((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) AS `percentual`
    FROM (`$b`.`vendas`
    LEFT JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`)))
    WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja'))
    GROUP BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)
    ORDER BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)";

    $sql = "SELECT *
            FROM ($sql) AS subquery
            WHERE ano = YEAR(CURDATE())
            ORDER BY mes
            DESC";

    return $this->db->query($sql)->result();
  }


  public function clientesMesDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT 
    '$loja' AS `loja`,
    MONTH( `$b`.`vendas`.`dataVenda` ) AS `mes`,
    DATE_FORMAT(`$b`.`vendas`.`dataVenda`,'%b' ) AS `mese`,
    YEAR(`$b`.`vendas`.`dataVenda`) AS `ano`,
    `cliente_nome`,
    SUM(`valorTotal`) AS total    
    FROM
      `$b`.`vendas` 
      JOIN `$b`.`clientes` 
        ON `$b`.vendas.`clientes_id` = `$b`.clientes.`cliente_id` 
    WHERE `$b`.`vendas`.`venda_visivel` = 1 AND  `$b`.`vendas`.`valorTotal` IS NOT NULL
      AND YEAR(`$b`.`vendas`.`dataVenda`) =  YEAR(CURDATE())
    GROUP BY `$b`.`vendas`.`clientes_id`, ano, mes 
    ORDER BY mes, `total` DESC";


    // $sql = "SELECT *
    //         FROM ($sql) AS subquery
    //         WHERE ano = YEAR(CURDATE())
    //         ORDER BY mes
    //         DESC";

    return $this->db->query($sql)->result();

  }




  public function vendasAnoDetalhe($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`, 'Blue-light' AS `label`,
    MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
    DATE_FORMAT(`$b`.`vendas`.`dataVenda`, '%b') AS `mese`,
    YEAR(`$b`.`vendas`.`dataVenda`) AS `ano`,
    COUNT(DISTINCT `$b`.`vendas`.`idVendas`) AS `qtVendas`,
    COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
    SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `total`,
    SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custo`,
    (SUM(`$b`.`itens_de_vendas`.`subTotal`) - SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) AS `lucro`,
    (((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) AS `percentual`
    FROM (`$b`.`vendas` LEFT JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja'))
    GROUP BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)
    ORDER BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)";

    $sql = "SELECT loja, ano, SUM(`qtVendas`) AS qtVendas, SUM(`itensQtd`) AS `itensQtd`, SUM(total) AS total, SUM(custo) AS custo, SUM(lucro) AS lucro, ROUND(AVG(`percentual`),2) AS percentual
            FROM ($sql) AS subquery
            GROUP BY ano
            ORDER BY ano DESC";

    return $this->db->query($sql)->result();
  }

  public function vendasMesDetalheAnoAnterior($loja)
  {
    $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

    $loja = strtoupper($loja);

    $sql = 
    "SELECT '$loja' AS `loja`, 'Blue-light' AS `label`,
    MONTH(`$b`.`vendas`.`dataVenda`) AS `mes`,
    DATE_FORMAT(`$b`.`vendas`.`dataVenda`, '%b') AS `mese`,
    YEAR(`$b`.`vendas`.`dataVenda`) AS `ano`,
    COUNT(DISTINCT `$b`.`vendas`.`idVendas`) AS `qtVendas`,
    COUNT(DISTINCT `$b`.`itens_de_vendas`.`idItens`) AS `itensQtd`,
    SUM(`$b`.`itens_de_vendas`.`subTotal`) AS `total`,
    SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)) AS `custo`,
    (SUM(`$b`.`itens_de_vendas`.`subTotal`) - SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) AS `lucro`,
    (((SUM(`$b`.`itens_de_vendas`.`subTotal`) / SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`))) * 100) - 100) AS `percentual`
    FROM (`$b`.`vendas` LEFT JOIN `$b`.`itens_de_vendas` ON ((`$b`.`vendas`.`idVendas` = `$b`.`itens_de_vendas`.`vendas_id`))) WHERE ((`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja'))
    GROUP BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)
    ORDER BY YEAR(`$b`.`vendas`.`dataVenda`), MONTH(`$b`.`vendas`.`dataVenda`)";

    $sql = "SELECT *
            FROM ($sql) AS subquery
            WHERE ano = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))
            ORDER BY mes
            DESC";

    return $this->db->query($sql)->result();
  }

  private function pegarBaseDaLojaPelaBasePrincipal($loja)
  {
    $basePrincipal = BDCAMINHO;
    $basePrincipal = explode("_", $basePrincipal);
    $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

    $base = $basePrincipal . strtolower($loja);

    return $base;
  } 
}

/* End of file Financeiro_model.php */
/* Location: ./application/models/Financeiro_model.php */