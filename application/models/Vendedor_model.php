<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Vendedor_model extends CI_Model {


	private $base  =  '';

    function __construct($foo = null) {

        $this->base =  $this->session->userdata('empresa_nome_base');

    }


    /**
     * Top 10 Dia (ANTIGO) 
     */

    /*
    public function top10Dia()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_dia` 
                    GROUP BY loja
					ORDER BY num ";
        return $this->db->query($query)->result();
    }
    */

    /**
     * Top 10 Dia Detalhe (ANTIGO) 
     */

    /*
    public function top10DiaDetalhe($loja)
    {

        $query = "  SELECT view_func_top10_dia.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual 
                    FROM `view_func_top10_dia`
                    WHERE loja = '{$loja}'  ORDER BY `valorTotal` DESC ";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Top 10 Mês (ANTIGO)
     */

    /*
    public function top10Mes()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_mes` 
                    GROUP BY loja	
					ORDER BY num ";
        return $this->db->query($query)->result();
    }
    */

    /**
     * Top 10 Mês Detalhe (ANTIGO)
     */

    /*
    public function top10MesDetalhe($loja)
    {

        $query = "  SELECT view_func_top10_mes.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_func_top10_mes` 
                    WHERE loja = '{$loja}'  ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
    */

    /**
     * Top 10 Ano (ANTIGO)
     */

    /*
    public function top10Ano()
    {
        $query = " SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal,
                    ROUND( ( (  (  (SUM(`valorTotal`)/SUM(`custoTotal`)) *100) -100) /100) ,2) AS percentual, 
                    label 
                    FROM `view_func_top10_ano` 
                    GROUP BY loja				
					ORDER BY num ";
        return $this->db->query($query)->result();
    }
    */

    /**
     * Top 10 Ano Detalhe (ANTIGO)
     */

    /*
    public function top10AnoDetalhe($loja)
    {

        $query = "  SELECT view_func_top10_ano.*, (valorTotal - custoTotal) AS lucro,
                    ROUND( ( ( (`valorTotal`/`custoTotal`) *100) -100) ,2) AS percentual
                    FROM `view_func_top10_ano`
                    WHERE loja = '{$loja}' ORDER BY `valorTotal` DESC  ";

        return $this->db->query($query)->result();

    }
    */

    public function top10Dia()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((`$b`.`vendas`.`dataVenda` = CURDATE()) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
            WHERE ((`$b`.`vendas`.`dataVenda` = CURDATE()) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
            GROUP BY `$b`.`vendas`.`usuarios_id`
            ORDER BY `valorTotal`
            DESC
            LIMIT 10)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10Mes()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
            WHERE ((MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
            GROUP BY `$b`.`vendas`.`usuarios_id`
            ORDER BY `valorTotal`
            DESC
            LIMIT 10)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery 
                GROUP BY loja
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10Ano()
    {
        $bases = SGBD;
        $queries = [];
        $labels = ["Red", "Blue-light", "Blue", "Green", "Pink", "Brown"];

        foreach ($bases as $indice => $b) {
            $base = explode('_', $b);
            $loja = strtoupper($base[2]);
            $label = $labels[$indice];

            $num = sprintf("%02d", $indice + 1);

            $queries[] = 
            "(SELECT '$loja' AS `loja`, '$num' AS `num`, '$label' AS `label`,
            `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
            (SELECT COUNT(`$b`.`vendas`.`idVendas`)
            FROM `$b`.`vendas`
            WHERE ((YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`faturado` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
            ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
            ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
            FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
            WHERE ((YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`faturado` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
            GROUP BY `$b`.`vendas`.`usuarios_id`
            ORDER BY `valorTotal`
            DESC
            LIMIT 10)";
        }

        $sql = implode(' UNION ALL ', $queries);

        $sql = "SELECT loja, SUM(qtdVenda) AS qtd, SUM(valorTotal) AS valorTotal, SUM(custoTotal) AS custoTotal, ((((SUM(`valorTotal`) / SUM(`custoTotal`)) * 100) - 100) / 100) AS percentual, label
                FROM ($sql) as subquery
                GROUP BY loja	
                ORDER BY num";

        return $this->db->query($sql)->result();
    }

    public function top10DiaDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((`$b`.`vendas`.`dataVenda` = CURDATE()) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
        WHERE ((`$b`.`vendas`.`dataVenda` = CURDATE()) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
        GROUP BY `$b`.`vendas`.`usuarios_id`
        ORDER BY `valorTotal`
        DESC
        LIMIT 10";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal` / `custoTotal`) * 100) - 100), 2) AS percentual 
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function top10MesDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
        WHERE ((MONTH(`$b`.`vendas`.`dataVenda`) = MONTH(CURDATE())) AND (YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
        GROUP BY `$b`.`vendas`.`usuarios_id`
        ORDER BY `valorTotal`
        DESC
        LIMIT 10";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal` / `custoTotal`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    public function top10AnoDetalhe($loja)
    {
        $b = $this->pegarBaseDaLojaPelaBasePrincipal($loja);

        $loja = strtoupper($loja);

        $sql = 
        "SELECT '$loja' AS `loja`,
        `$b`.`usuarios`.`usuario_nome` AS `vendedor`,
        (SELECT COUNT(`$b`.`vendas`.`idVendas`)
        FROM `$b`.`vendas`
        WHERE ((YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`faturado` = 1) AND (`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) AS `qtdVenda`,
        ROUND(SUM((`$b`.`itens_de_vendas`.`prod_preco_custo` * `$b`.`itens_de_vendas`.`quantidade`)), 2) AS `custoTotal`,
        ROUND(SUM(`$b`.`itens_de_vendas`.`subTotal`), 2) AS `valorTotal`
        FROM (((`$b`.`vendas` JOIN `$b`.`usuarios` ON ((`$b`.`vendas`.`usuarios_id` = `$b`.`usuarios`.`usuario_id`))) JOIN `$b`.`itens_de_vendas` ON ((`$b`.`itens_de_vendas`.`vendas_id` = `$b`.`vendas`.`idVendas`))) LEFT JOIN `$b`.`financeiro` ON ((`$b`.`vendas`.`idVendas` = `$b`.`financeiro`.`vendas_id`)))
        WHERE ((YEAR(`$b`.`vendas`.`dataVenda`) = YEAR(CURDATE())) AND (`$b`.`vendas`.`venda_visivel` = 1) AND (`$b`.`vendas`.`faturado` = 1) AND (`$b`.`itens_de_vendas`.`filial` = '$loja') AND (`$b`.`financeiro`.`financeiro_descricao` LIKE '%$loja%'))
        GROUP BY `$b`.`vendas`.`usuarios_id`
        ORDER BY `valorTotal`
        DESC
        LIMIT 10";

        $sql = "SELECT *, (valorTotal - custoTotal) AS lucro, ROUND((((`valorTotal`/`custoTotal`) * 100) - 100), 2) AS percentual
                FROM ($sql) as subquery
                ORDER BY `valorTotal`
                DESC";

        return $this->db->query($sql)->result();
    }

    private function pegarBaseDaLojaPelaBasePrincipal($loja)
    {
        $basePrincipal = BDCAMINHO;
        $basePrincipal = explode("_", $basePrincipal);
        $basePrincipal = $basePrincipal[0] . '_' . $basePrincipal[1] . '_';

        $base = $basePrincipal . strtolower($loja);

        return $base;
    }
}



/* End of file Produto_model.php */

/* Location: ./application/models/Produto_model.php */