					
					<!-- Basic responsive configuration -->

					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Média Pagamentos: <?php  echo 'R$ '.number_format($resutado["media"],2,',','.');  ?></h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							Dias: <?php echo $resutado["dias"] ?>   | Total: <?php echo 'R$ '.number_format($resutado["totalpago"],2,',','.');  ?> 
						</div>

						<table class="table datatable-responsive">
							<thead>
								<tr>
									
									<th>Valor</th>	
									<th>Data</th>

																								
							
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($dados as $v) { 
								$dta_pagamento = date(('d/m/Y'), strtotime($v->data_baixa));
								    ?>   
								<tr>
									<td><?php echo 'R$ '.number_format($v->valor_baixa,2,',','.'); ?></td>	
									<td><?php echo $dta_pagamento  ?></td>										

								</tr>
								<?php } ?>  
					
					



							</tbody>
						</table>
					</div>
					<!-- /basic responsive configuration -->


	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/datatables_responsive.js"></script>




	