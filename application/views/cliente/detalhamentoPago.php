				







				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Detalhamento Pagamento Cliente: <?php echo $dados[0]->cliente ?> </h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                		
		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento  - Vendas - Pagas.
					</div>



					<table class="table table-togglable table-hover">

						<thead>

							<tr>

								<th data-toggle="true">Ano</th>
								<th data-hide="phone">mes</th>								
								<th data-hide="phone">Comprou</th>
								<th data-hide="phone">Pago</th>
								<!-- <th data-hide="phone">Saldo Mês</th> -->

								<th data-hide="phone">Aberto Acumulado</th>
							
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>

							</tr>

						</thead>

						<tbody>


							<?php  foreach ( $dados as $v  ) {  
							// var_dump($cliente["loja"]);die();
							 ?> 
							<tr>

								<td><?php echo $v->ano.'.'.$v->mes ?></td>

								<td><?php echo $v->mes ?></td>

								

								<td><span class="text-blue-600"><?php echo number_format($v->totalCompra,2,',','.'); ?></span></td> 

								<td><span class="text-teal-600"><?php echo number_format($v->total ,2,',','.');  ?>&nbsp;&nbsp;&nbsp;</span>
										                   
                					<a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/detalhamentoDiasPago/<?php echo $cliente["loja"].'/'.$cliente["cliente_id"].'/'.$v->ano.'/'.$v->mes ?>" data-toggle="tooltip" title="Editar"><i class="icon-info22 text-danger"></i> </a>              
              						
              					</td>	

								<!-- <td><span><?php echo str_replace("-","+",number_format($v->totalCompra - $v->total,2,',','.')); ?></span></td> -->

								<!-- <td><span class="text-danger-600"><?php echo number_format($valorAberto,2,',','.'); ?></span></td> 		 -->

								<td><span class="text-danger-600"><?php echo number_format($v->totalAberto,2,',','.'); ?></span></td> 	
								
								<td class="text-center">

								<!-- <ul class="icons-list">

										<li class="dropdown">

											<a href="#" class="dropdown-toggle" data-toggle="dropdown">

												<i class="icon-menu9"></i>

											</a>



											<ul class="dropdown-menu dropdown-menu-right">

												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>

												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>

												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>

											</ul>

										</li>

									</ul> -->

								</td> 

							</tr>
							<?php  }   ?> 
							

						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>