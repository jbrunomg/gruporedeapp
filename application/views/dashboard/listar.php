   <?php
    $receita = 0.00;
    $compras = 0.00;
    $despesa = 0.00;

    foreach($dadosFinanceiro as $v){
     if ($v->financeiro_tipo == 'receita') {
         $receita = $v->valor;
     }

     if ($v->financeiro_tipo == 'compras') {
         $compras = $v->valor;
     }

     if ($v->financeiro_tipo == 'despesa') {
         $despesa = $v->valor;
     }

    } 

    $lucroGeral   = $receita - $compras;
    $lucroLiquido = ($receita - $compras) - $despesa; 
    ?>


    <?php
    $receitaD = 0.00;
    $comprasD = 0.00;
    $despesaD = 0.00;

    foreach($dadosFinanceiroDia as $v){
    if ($v->dia == date("j")) {
            
       
     if ($v->financeiro_tipo == 'receita') {
         $receitaD = $v->valor;
     }

     if ($v->financeiro_tipo == 'compras') {
         $comprasD = $v->valor;
     }

     if ($v->financeiro_tipo == 'despesa') {
         $despesaD = $v->valor;
     }

    } 


    }

     $lucroGeralD   = $receitaD - $comprasD;
     $lucroLiquidoD = ($receitaD - $comprasD) - $despesaD; 

     ?>

            
            <!-- Main content -->
            <div class="content-wrapper">
                    <!-- Dashboard content -->
                <div class="row">
                    <div class="col-lg-12">

                                    <!-- Quick stats boxes -->
                        <div class="row">

                            <!--FATURAMENTO -->
                            <div class="col-lg-4">

                                <!-- Today's revenue -->
                                <div class="panel bg-primary-800">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="reload"></a></li>
                                            </ul>
                                        </div>

                                        <h2>Receita Bruta - Geral</h2>

                                        <h4 class="no-margin">Dia&nbsp;&nbsp;&nbsp;- R$ <?php echo number_format($receitaD,2,',','.');    ?></h4>                                     
                                        <h4 class="no-margin">Mês&nbsp;- R$ <?php echo number_format($receita,2,',','.'); ?></h4>            
                                                                           
                                        <div class="text-muted text-size-small"></div>
                                    </div>

                                    <div id="today-revenue"></div>
                                </div>
                                <!-- /today's revenue -->

                            </div>
                            


                            <!-- CUSTO MERCADORIA-->
                            <div class="col-lg-4">

                                <!-- Members online -->
                                <div class="panel bg-orange">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-orange-300">+53,6%</span>
                                        </div>

                                        <h2>Custo - Mercadoria</h2>

                                        <h4 class="no-margin">Dia&nbsp;&nbsp;&nbsp;- R$ <?php echo number_format($comprasD,2,',','.');    ?></h4>                                
                                        <h4 class="no-margin">Mês&nbsp;- R$ <?php echo number_format($compras,2,',','.'); ?></h4>  
                                                                           
                                        <div class="text-muted text-size-small"></div>
                                    </div>

                                    <div id="today-mercadoria"></div>
                                 
                                </div>
                                <!-- /members online -->

                            </div>


                            <!-- LUCRO BRUTO  -->
                            <div class="col-lg-4">

                                <!-- Members online -->
                                <div class="panel bg-teal-400">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <span class="heading-text badge bg-teal-800">+53,6%</span>
                                        </div>
                                        <h2>Lucro - Geral</h2>
                                        <h4 class="no-margin">Dia&nbsp;&nbsp;&nbsp;- R$ <?php echo number_format($lucroGeralD,2,',','.');    ?></h4>
                                        <h4 class="no-margin">Mês&nbsp;- R$ <?php echo number_format($lucroGeral,2,',','.');  ?></h4>
                                        <div class="text-muted text-size-small"></div>
                                    </div>

                                    <div class="container-fluid">
                                        <div id="members-online"></div>
                                    </div>
                                </div>
                                <!-- /members online -->

                            </div>


                            <!-- DESPESA  -->
                            <div class="col-lg-4">

                                <!-- Current server load -->
                                <div class="panel bg-danger-400">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                                        <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                                        <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                                        <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <h2>Despesa - Geral</h2>
                                        <h4 class="no-margin">Dia&nbsp;&nbsp;&nbsp;- R$ <?php echo number_format($despesaD,2,',','.');    ?></h4>
                                        <h4 class="no-margin">Mês&nbsp;- R$ <?php echo number_format($despesa,2,',','.');  ?></h4>                                     
                                        <div class="text-muted text-size-small"></div>
                                    </div>

                                    <div id="today-despesa"></div>
                                    
                                </div>
                                <!-- /current server load -->

                            </div>



                            <!--FATURAMENTO -->
                            <div class="col-lg-4">

                                <!-- Today's revenue -->
                                <div class="panel bg-blue-400">
                                    <div class="panel-body">
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="reload"></a></li>
                                            </ul>
                                        </div>

                                        <h2>Lucro - Liquido</h2>
                                        <h4 class="no-margin">Dia&nbsp;&nbsp;&nbsp;- R$ <?php echo number_format( $lucroLiquidoD,2,',','.');  ?></h4>
                                        <h4 class="no-margin">Mês&nbsp;- R$ <?php echo number_format( $lucroLiquido,2,',','.');  ?></h4>
                                        
                                        <div class="text-muted text-size-small"></div>
                                    </div>

                                    <div id="today-revenue"></div>
                                </div>
                                <!-- /today's revenue -->

                            </div>


            


                        </div>
                        <!-- /quick stats boxes -->

                       </div>
                </div>

            

 

                        <!-- Progress counters -->
                        <div class="row">
                            <div class="col-md-6">

                                <!-- Available hours -->
                                <div class="panel text-center">
                                    <div class="panel-body">
                  <!--                       <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li class="dropdown text-muted">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                                        <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                                        <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                                        <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div> -->

                                        <!-- Progress counter -->
                                        <a href="<?php echo base_url(); ?>Cliente/top10Ano">
                                            <div class="content-group-sm svg-center position-relative" id="hours-available-progress"></div>
                                        </a>
                                        <!-- /progress counter -->


                                        <!-- Bars -->
                                        <div id="hours-available-bars"></div>
                                        <!-- /bars -->

                                    </div>
                                </div>
                                <!-- /available hours -->

                            </div>

                            <div class="col-md-6">

                                <!-- Productivity goal -->
                                <div class="panel text-center">
                                    <div class="panel-body">
                        <!--                 <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li class="dropdown text-muted">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                                        <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                                        <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                                        <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div> -->

                                        <!-- Progress counter -->
                                        <a href="<?php echo base_url(); ?>vendedor/top03Ano">
                                            <div class="content-group-sm svg-center position-relative" id="goal-progress"></div>
                                        </a>
                                        <!-- /progress counter -->

                                        <!-- Bars -->
                                        <div id="goal-bars"></div>
                                        <!-- /bars -->

                                    </div>
                                </div>
                                <!-- /productivity goal -->

                            </div>
                        </div>
                        <!-- /progress counters -->


                 </div>     



            <!-- Stats with progress -->
            <h6 class="content-group text-semibold">
                Soma acumulada de <?php echo '02' ?> - Lojas
                <!-- <small class="display-block">Relatorios</small> -->
            </h6>   






 <!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/dashboard.js"></script>  -->

 <script type="text/javascript">

$(function() {  


    // Initialize charts

    dailyRevenue('#today-revenue', 50); // initialize chart - Receita
    

    // Daily revenue line chart
    // ------------------------------

    // Chart setup
    function dailyRevenue(element, height) {

 //   console.log('BRUNO');    
 
        // Add data set

        var dataset = [
        <?php foreach ($dadosFinanceiroReceitaDia as $v) { ?>  
            
            {
                "date": "<?php echo  $v->dia.'/'.date("m/y"); ?>",
                "alpha": "<?php echo $v->valor;  ?>"
            },

        <?php } ?>
            ];
  

        console.log(element)

        // Main variables
        var d3Container = d3.select(element),
            margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom,
            padding = 20;

        // Format date
        var parseDate = d3.time.format("%d/%m/%y").parse,
            formatDate = d3.time.format("%B %e");



        // Add tooltip
        // ------------------------------

        var tooltip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function (d) {
                return "<ul class='list-unstyled mb-5'>" +
                    "<li>" + "<div class='text-size-base mt-5 mb-5'><i class='icon-check2 position-left'></i>" + formatDate(d.date) + "</div>" + "</li>" +
                    "<li>" + "Receita: &nbsp;" + "<span class='text-semibold pull-right'>" + "R$ " + d.alpha + "</span>" + "</li>" +
                    "<li>" + "</li>" + 
                "</ul>";
            });



        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .call(tooltip);



        // Load data
        // ------------------------------

        dataset.forEach(function (d) {
            d.date = parseDate(d.date);
            d.alpha = +d.alpha;
        });



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.time.scale()
            .range([padding, width - padding]);

        // Vertical
        var y = d3.scale.linear()
            .range([height, 5]);



        // Set input domains
        // ------------------------------

        // Horizontal
        x.domain(d3.extent(dataset, function (d) {
            return d.date;
        }));

        // Vertical
        y.domain([0, d3.max(dataset, function (d) {
            return Math.max(d.alpha);
        })]);



        // Construct chart layout
        // ------------------------------

        // Line
        var line = d3.svg.line()
            .x(function(d) {
                return x(d.date);
            })
            .y(function(d) {
                return y(d.alpha)
            });



        //
        // Append chart elements
        //

        // Add mask for animation
        // ------------------------------

        // Add clip path
        var clip = svg.append("defs")
            .append("clipPath")
            .attr("id", "clip-line-small");

        // Add clip shape
        var clipRect = clip.append("rect")
            .attr('class', 'clip')
            .attr("width", 0)
            .attr("height", height);

        // Animate mask
        clipRect
              .transition()
                  .duration(1000)
                  .ease('linear')
                  .attr("width", width);



        // Line
        // ------------------------------

        // Path
        var path = svg.append('path')
            .attr({
                'd': line(dataset),
                "clip-path": "url(#clip-line-small)",
                'class': 'd3-line d3-line-medium'
            })
            .style('stroke', '#fff');

        // Animate path
        svg.select('.line-tickets')
            .transition()
                .duration(1000)
                .ease('linear');



        // Add vertical guide lines
        // ------------------------------

        // Bind data
        var guide = svg.append('g')
            .selectAll('.d3-line-guides-group')
            .data(dataset);

        // Append lines
        guide
            .enter()
            .append('line')
                .attr('class', 'd3-line-guides')
                .attr('x1', function (d, i) {
                    return x(d.date);
                })
                .attr('y1', function (d, i) {
                    return height;
                })
                .attr('x2', function (d, i) {
                    return x(d.date);
                })
                .attr('y2', function (d, i) {
                    return height;
                })
                .style('stroke', 'rgba(255,255,255,0.3)')
                .style('stroke-dasharray', '4,2')
                .style('shape-rendering', 'crispEdges');

        // Animate guide lines
        guide
            .transition()
                .duration(1000)
                .delay(function(d, i) { return i * 150; })
                .attr('y2', function (d, i) {
                    return y(d.alpha);
                });



        // Alpha app points
        // ------------------------------

        // Add points
        var points = svg.insert('g')
            .selectAll('.d3-line-circle')
            .data(dataset)
            .enter()
            .append('circle')
                .attr('class', 'd3-line-circle d3-line-circle-medium')
                .attr("cx", line.x())
                .attr("cy", line.y())
                .attr("r", 3)
                .style('stroke', '#fff')
                .style('fill', '#29B6F6');



        // Animate points on page load
        points
            .style('opacity', 0)
            .transition()
                .duration(250)
                .ease('linear')
                .delay(1000)
                .style('opacity', 1);


        // Add user interaction
        points
            .on("mouseover", function (d) {
                tooltip.offset([-10, 0]).show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })

            // Hide tooltip
            .on("mouseout", function (d) {
                tooltip.hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of first point
        d3.select(points[0][0])
            .on("mouseover", function (d) {
                tooltip.offset([0, 10]).direction('e').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of last point
        d3.select(points[0][points.size() - 1])
            .on("mouseover", function (d) {
                tooltip.offset([0, -10]).direction('w').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            })




    }


    dailyMercadoria('#today-mercadoria', 50); // initialize chart - Compras

    // Chart setup
    function dailyMercadoria(element, height) {

    //   console.log('BRUNO');    
 
        // Add data set

        var dataset = [
        <?php foreach ($resumoFinanceiroCompraDia as $v) { ?>  
            
            {
                "date": "<?php echo  $v->dia.'/'.date("m/y"); ?>",
                "alpha": "<?php echo $v->valor;  ?>"
            },
                    
        <?php } ?>
            ];    

        console.log(element)

        // Main variables
        var d3Container = d3.select(element),
            margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom,
            padding = 20;

        // Format date
        var parseDate = d3.time.format("%d/%m/%y").parse,
            formatDate = d3.time.format("%B %e");



        // Add tooltip
        // ------------------------------

        var tooltip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function (d) {
                return "<ul class='list-unstyled mb-5'>" +
                    "<li>" + "<div class='text-size-base mt-5 mb-5'><i class='icon-check2 position-left'></i>" + formatDate(d.date) + "</div>" + "</li>" +
                    "<li>" + "Compra: &nbsp;" + "<span class='text-semibold pull-right'>" + "R$ " + d.alpha + "</span>" + "</li>" +
                    "<li>" + "</li>" + 
                "</ul>";
            });



        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .call(tooltip);



        // Load data
        // ------------------------------

        dataset.forEach(function (d) {
            d.date = parseDate(d.date);
            d.alpha = +d.alpha;
        });



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.time.scale()
            .range([padding, width - padding]);

        // Vertical
        var y = d3.scale.linear()
            .range([height, 5]);



        // Set input domains
        // ------------------------------

        // Horizontal
        x.domain(d3.extent(dataset, function (d) {
            return d.date;
        }));

        // Vertical
        y.domain([0, d3.max(dataset, function (d) {
            return Math.max(d.alpha);
        })]);



        // Construct chart layout
        // ------------------------------

        // Line
        var line = d3.svg.line()
            .x(function(d) {
                return x(d.date);
            })
            .y(function(d) {
                return y(d.alpha)
            });



        //
        // Append chart elements
        //

        // Add mask for animation
        // ------------------------------

        // Add clip path
        var clip = svg.append("defs")
            .append("clipPath")
            .attr("id", "clip-line-small");

        // Add clip shape
        var clipRect = clip.append("rect")
            .attr('class', 'clip')
            .attr("width", 0)
            .attr("height", height);

        // Animate mask
        clipRect
              .transition()
                  .duration(1000)
                  .ease('linear')
                  .attr("width", width);



        // Line
        // ------------------------------

        // Path
        var path = svg.append('path')
            .attr({
                'd': line(dataset),
                "clip-path": "url(#clip-line-small)",
                'class': 'd3-line d3-line-medium'
            })
            .style('stroke', '#fff');

        // Animate path
        svg.select('.line-tickets')
            .transition()
                .duration(1000)
                .ease('linear');



        // Add vertical guide lines
        // ------------------------------

        // Bind data
        var guide = svg.append('g')
            .selectAll('.d3-line-guides-group')
            .data(dataset);

        // Append lines
        guide
            .enter()
            .append('line')
                .attr('class', 'd3-line-guides')
                .attr('x1', function (d, i) {
                    return x(d.date);
                })
                .attr('y1', function (d, i) {
                    return height;
                })
                .attr('x2', function (d, i) {
                    return x(d.date);
                })
                .attr('y2', function (d, i) {
                    return height;
                })
                .style('stroke', 'rgba(255,255,255,0.3)')
                .style('stroke-dasharray', '4,2')
                .style('shape-rendering', 'crispEdges');

        // Animate guide lines
        guide
            .transition()
                .duration(1000)
                .delay(function(d, i) { return i * 150; })
                .attr('y2', function (d, i) {
                    return y(d.alpha);
                });



        // Alpha app points
        // ------------------------------

        // Add points
        var points = svg.insert('g')
            .selectAll('.d3-line-circle')
            .data(dataset)
            .enter()
            .append('circle')
                .attr('class', 'd3-line-circle d3-line-circle-medium')
                .attr("cx", line.x())
                .attr("cy", line.y())
                .attr("r", 3)
                .style('stroke', '#fff')
                .style('fill', '#29B6F6');



        // Animate points on page load
        points
            .style('opacity', 0)
            .transition()
                .duration(250)
                .ease('linear')
                .delay(1000)
                .style('opacity', 1);


        // Add user interaction
        points
            .on("mouseover", function (d) {
                tooltip.offset([-10, 0]).show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })

            // Hide tooltip
            .on("mouseout", function (d) {
                tooltip.hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of first point
        d3.select(points[0][0])
            .on("mouseover", function (d) {
                tooltip.offset([0, 10]).direction('e').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of last point
        d3.select(points[0][points.size() - 1])
            .on("mouseover", function (d) {
                tooltip.offset([0, -10]).direction('w').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            })

    }




    dailyDespesa('#today-despesa', 50); // initialize chart - Compras

    // Chart setup
    function dailyDespesa(element, height) {

    //   console.log('BRUNO');    
 
        // Add data set

        var dataset = [
        <?php foreach ($resumoFinanceiroDespesaDia as $v) { ?>  
            
            {
                "date": "<?php echo  $v->dia.'/'.date("m/y"); ?>",
                "alpha": "<?php echo $v->valor;  ?>"
            },
                    
        <?php } ?>
            ];    

        console.log(element)

        // Main variables
        var d3Container = d3.select(element),
            margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom,
            padding = 20;

        // Format date
        var parseDate = d3.time.format("%d/%m/%y").parse,
            formatDate = d3.time.format("%B %e");



        // Add tooltip
        // ------------------------------

        var tooltip = d3.tip()
            .attr('class', 'd3-tip')
            .html(function (d) {
                return "<ul class='list-unstyled mb-5'>" +
                    "<li>" + "<div class='text-size-base mt-5 mb-5'><i class='icon-check2 position-left'></i>" + formatDate(d.date) + "</div>" + "</li>" +
                    "<li>" + "Despesa: &nbsp;" + "<span class='text-semibold pull-right'>" + "R$ " + d.alpha + "</span>" + "</li>" +
                    "<li>" + "</li>" + 
                "</ul>";
            });



        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append('svg');

        // Add SVG group
        var svg = container
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .call(tooltip);



        // Load data
        // ------------------------------

        dataset.forEach(function (d) {
            d.date = parseDate(d.date);
            d.alpha = +d.alpha;
        });



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.time.scale()
            .range([padding, width - padding]);

        // Vertical
        var y = d3.scale.linear()
            .range([height, 5]);



        // Set input domains
        // ------------------------------

        // Horizontal
        x.domain(d3.extent(dataset, function (d) {
            return d.date;
        }));

        // Vertical
        y.domain([0, d3.max(dataset, function (d) {
            return Math.max(d.alpha);
        })]);



        // Construct chart layout
        // ------------------------------

        // Line
        var line = d3.svg.line()
            .x(function(d) {
                return x(d.date);
            })
            .y(function(d) {
                return y(d.alpha)
            });



        //
        // Append chart elements
        //

        // Add mask for animation
        // ------------------------------

        // Add clip path
        var clip = svg.append("defs")
            .append("clipPath")
            .attr("id", "clip-line-small");

        // Add clip shape
        var clipRect = clip.append("rect")
            .attr('class', 'clip')
            .attr("width", 0)
            .attr("height", height);

        // Animate mask
        clipRect
              .transition()
                  .duration(1000)
                  .ease('linear')
                  .attr("width", width);



        // Line
        // ------------------------------

        // Path
        var path = svg.append('path')
            .attr({
                'd': line(dataset),
                "clip-path": "url(#clip-line-small)",
                'class': 'd3-line d3-line-medium'
            })
            .style('stroke', '#fff');

        // Animate path
        svg.select('.line-tickets')
            .transition()
                .duration(1000)
                .ease('linear');



        // Add vertical guide lines
        // ------------------------------

        // Bind data
        var guide = svg.append('g')
            .selectAll('.d3-line-guides-group')
            .data(dataset);

        // Append lines
        guide
            .enter()
            .append('line')
                .attr('class', 'd3-line-guides')
                .attr('x1', function (d, i) {
                    return x(d.date);
                })
                .attr('y1', function (d, i) {
                    return height;
                })
                .attr('x2', function (d, i) {
                    return x(d.date);
                })
                .attr('y2', function (d, i) {
                    return height;
                })
                .style('stroke', 'rgba(255,255,255,0.3)')
                .style('stroke-dasharray', '4,2')
                .style('shape-rendering', 'crispEdges');

        // Animate guide lines
        guide
            .transition()
                .duration(1000)
                .delay(function(d, i) { return i * 150; })
                .attr('y2', function (d, i) {
                    return y(d.alpha);
                });



        // Alpha app points
        // ------------------------------

        // Add points
        var points = svg.insert('g')
            .selectAll('.d3-line-circle')
            .data(dataset)
            .enter()
            .append('circle')
                .attr('class', 'd3-line-circle d3-line-circle-medium')
                .attr("cx", line.x())
                .attr("cy", line.y())
                .attr("r", 3)
                .style('stroke', '#fff')
                .style('fill', '#29B6F6');



        // Animate points on page load
        points
            .style('opacity', 0)
            .transition()
                .duration(250)
                .ease('linear')
                .delay(1000)
                .style('opacity', 1);


        // Add user interaction
        points
            .on("mouseover", function (d) {
                tooltip.offset([-10, 0]).show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })

            // Hide tooltip
            .on("mouseout", function (d) {
                tooltip.hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of first point
        d3.select(points[0][0])
            .on("mouseover", function (d) {
                tooltip.offset([0, 10]).direction('e').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            });

        // Change tooltip direction of last point
        d3.select(points[0][points.size() - 1])
            .on("mouseover", function (d) {
                tooltip.offset([0, -10]).direction('w').show(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 4);
            })
            .on("mouseout", function (d) {
                tooltip.direction('n').hide(d);

                // Animate circle radius
                d3.select(this).transition().duration(250).attr('r', 3);
            })

    }




});


 </script> 