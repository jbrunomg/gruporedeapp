				







				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Previsão Financeira Mes -  <?php echo $dia.' Dias' ?></h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>

		                		<li><a data-action="reload"></a></li>

		                		<li><a data-action="close"></a></li>

		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento - Previsão Financeira

					</div>



					<table class="table table-togglable table-hover">

						<thead>

							<tr>
								<th data-toggle="true">Loja</th>
								<th data-hide="phone">Receita</th>
								<th data-hide="phone,tablet">Laçamento</th>
								<th data-hide="phone,tablet">Média</th>
								<th data-hide="phone">Dias Ulteis</th>
								<th data-hide="phone">Projeção</th>
								<th data-hide="phone">Despesas</th>
								<th data-hide="phone">Comissão</th>
								<th data-hide="phone">Lucro Brutott</th>
								<th data-hide="phone">Lucro Líquido</th>
								<th data-hide="phone">Projeção L. Líquido</th>
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
							</tr>

						</thead>

						<tbody>
							<?php foreach ($dados as $val) { ?>
								
							<tr>
								<td><?php echo $val->loja  ?></td>
								<td><?php echo number_format($val->receita,2,',','.'); ?></td>
								<td><span class="text-danger-600"><?php echo $val->dias_lancamento ?></span></td>
								<td><span class="text-blue-600"><?php echo number_format($val->media,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo $dia  ?></span></td>
								<td><span class="text-teal-600"><?php echo number_format($val->media*$dia,2,",",".");  ?></span></td>
								<td><span class="text-danger-600"><?php echo number_format($val->despesa,2,',','.');  ?></span></td>
								<td><span class="text-danger-600"><?php echo number_format($val->comissao,2,',','.');  ?></span></td>

								<td><span class="text-teal-600"><?php echo number_format($val->receita - $val->custo,2,',','.');  ?></span></td>

								<td><span class="text-teal-600"><?php echo number_format((($val->receita - $val->custo)-$val->despesa),2,',','.');  ?></span></td>

								<td><span class="text-teal-600"><?php echo number_format((((($val->receita-$val->custo)-$val->despesa)/$val->dias_lancamento)*$dia),2,',','.');  ?></span></td>
								<td class="text-center">
								<!-- <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
											</ul>
										</li>
									</ul> -->
								</td> 
							</tr>


							<?php } ?>	
						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>