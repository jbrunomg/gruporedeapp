				







				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Financeiro destaque Mês - <?php echo $dados[0]->loja ?></h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>

		                		<li><a data-action="reload"></a></li>

		                		<li><a data-action="close"></a></li>

		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento - Financeiro.

					</div>



					<table class="table table-togglable table-hover">

						<thead>

							<tr>

								<th data-toggle="true">Loja</th>
								<th data-hide="phone">Receita</th>
								<th data-hide="phone,tablet">Despesa</th>
								<th data-hide="phone,tablet">Reembolso</th>
								<th data-hide="phone">Sangria</th>
								<th data-hide="phone">Venda Fiado</th>

								<th data-hide="phone">Sub-Total</th>
								<th data-hide="phone">Pag Prazo</th>
								<th data-hide="phone">Total Saldo</th>
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>

							</tr>

						</thead>

						<tbody>

							<?php foreach ($dados as $d) {   ?> 
							<tr>
								<td><?php echo $d->loja; ?></td>
								<td><?php echo number_format(($d->receita + $d->comp_fiado),2,',','.'); ?></td>
								<td><span class="text-danger-600"><?php echo '-'.number_format($d->despesa,2,',','.'); ?></span></td>
								<td><span class="text-blue-600"><?php echo '-'.number_format($d->reembolso,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo '-'.number_format($d->sangria,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo '-'.number_format($d->comp_fiado,2,',','.');  ?></span></td>

								<td><span class="text-teal-600"><?php echo number_format(($d->saldo - $d->pag_fiado),2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo '+'.number_format($d->pag_fiado,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo number_format($d->saldo,2,',','.');  ?></span></td>
								<td class="text-center">
								<!-- <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
											</ul>
										</li>
									</ul> -->
								</td> 
							</tr>
							<?php } ?> 	


						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>