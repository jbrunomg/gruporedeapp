<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-ComPpatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>REDE - Gestor</title>

	<!-- PWA -->
		<link rel="manifest" href="<?php echo base_url(); ?>manifest.webmanifest" /> 
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="application-name" content="App nome">
		<meta name="apple-mobile-web-app-title" content="App nome">
		<meta name="msapplication-starturl" content=".">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- IOS Configs -->
	
		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-512x512.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-128x128.png"> <!-- iPad-->
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-192x192.png"> <!-- iPhone Retina-->
		<link rel="apple-touch-icon" sizes="167x167" href="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-152x152.png"> <!-- iPad Retina-->
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-96x96.png">



<!-- iPhone SE (1ª geração): 640 x 1136 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-640x1136.png">
<!-- iPhone SE (2ª geração): 750 x 1334 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-750x1334.png">
<!-- iPhone 6, 6s, 7, 8: 750 x 1334 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-750x1334.png">
<!-- iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus: 1080 x 1920 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1080x1920.png">

<!-- iPhone 8 Plus, 7 Plus, 6s Plus, 6 Plus (1242px x 2208px) -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1242x2208.png">

<!-- iPhone X, XS, 11 Pro: 1125 x 2436 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1125x2436.png">
<!-- iPhone XR, 11: 828 x 1792 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-828x1792.png"> 
<!-- iPhone XS Max, 11 Pro Max: 1242 x 2688 pixels -->
	    <link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1242x2688.png">
<!-- iPhone 12 mini: 1080 x 2340 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1080x2340.png">

<!-- iPhone 12, 13: 1170 x 2532 pixels -->
<link rel="apple-touch-startup-image"  media="(device-width: 390px) and (device-height: 844px) and (-webkit-device-pixel-ratio: 3) href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1170x2532.png">


<!-- iPhone 12, 13: 1170 x 2532 pixels -->
		<!-- <link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1170x2532.png"> -->
		
<!-- iPhone 12 Pro, 13 Pro: 1170 x 2532 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1170x2532.png">
<!-- iPhone 12 Pro Max, 13 Pro Max: 1284 x 2778 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1284x2778.png">


	
		
	
		
		<!-- iPad Pro 12.9" (2048px x 2732px) -->
		<link rel="apple-touch-startup-image" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-2048x2732.png">
		<!-- iPad Pro 11” (1668px x 2388px) --> 
		<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1668x2388.png">
		<!-- iPad Pro 10.5" (1668px x 2224px) -->
		<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1668x2224.png">
		<!-- iPad Mini, Air (1536px x 2048px) -->
		<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1536x2048.png">

		<meta name="apple-mobile-web-app-title" content="App Titulo"> <!-- Titulo de inicialização IOS -->
		<meta name="apple-mobile-web-app-capacity" content="yes"> <!-- Ocultar componentes da interface do usuário do Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="rgba(0,0,0, 0.0)"> <!-- Aparencia da barra de status -->
		<!-- /IOS Config -->

		
		<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/style.css"> <!-- CSS modal IOS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/animate.css" /> <!-- CSS de animação do modal -->
	<!--  /PWA  -->

	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/assets/images/logo_icon_dark.png"/>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Icone Bruno para Mobile -->
	<link rel="icon" href="<?php echo base_url(); ?>public/assets/images/icons/icon-32x32.png" sizes="32x32" />
	<link rel="icon" href="<?php echo base_url(); ?>public/assets/images/icons/icon-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>public/assets/images/icons/icon-180x180.png" />
	<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>public/assets/images/icons/icon-270x270.png" />

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->
	
	<meta name="theme-color" content="#004623"> <!--Define a cor da barra superior do navegador-->
    <!-- <link rel="manifest" href="<?php echo base_url(); ?>public/manifest.webmanifest"> -->
	
	



<!-- 	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
	<script>
	   var OneSignal = window.OneSignal || [];
		var initConfig = {
			appId: "0db74c19-ae83-43d8-a2e2-c60f8b3ac473",
			notifyButton: {
				enable: true
			},
		};
		OneSignal.push(function () {
			OneSignal.SERVICE_WORKER_PARAM = { scope: 'https://gestor.wdmtecnologia.com.br/public/' };
			OneSignal.SERVICE_WORKER_PATH = 'OneSignalSDKWorker.js'
			OneSignal.SERVICE_WORKER_UPDATER_PATH = 'OneSignalSDKUpdaterWorker.js'
			OneSignal.init(initConfig);
		});
	</script> -->
	
</head>

<body class="navbar-bottom login-container">

<!--	<link rel="manifest" href="<?php echo base_url(); ?>public/manifest.webmanifest" /> -->

	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo">
		<div><br></div> <!-- Melhoria Visual mobile -->
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/assets/images/logo_light_<?php echo GRUPOLOJA ?>.png" alt="" ></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="https://wdmtecnologia.com.br/" target="_blank">
						<i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Website</span>
					</a>
				</li>

				<li>
					<a href="https://api.whatsapp.com/send?phone=5581997724036&amp;text=Olá%20Digite%20sua%20mensagem." target="_blank">
						<i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contato admin</span>
					</a>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog3"></i>
						<span class="visible-xs-inline-block position-right"> Options</span>
					</a>
				</li>

				<li class="dropdown">
					<a class="dropdown-toggle" id="setup_button" data-toggle="dropdown">
						<i class="icon-mobile"></i>
						<span class="visible-xs-inline-block position-right"> Baixar Aplicativo</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Form with validation -->
				<form action="<?php echo base_url() ?>auth/processarLogin" class="form-validate" method="post">
					<div class="panel panel-body login-form">
						<div class="text-center">
							<img src="<?php echo base_url() ?>/public/assets/images/logo_<?php echo GRUPOLOJA ?>.png" width="150px"> 
							<h5 class="content-group">Faça login para acessar sua conta <small class="display-block">.........</small></h5>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input type="text" class="form-control" placeholder="Usuário" name="login" required="required" value="<?php echo set_value('login'); ?>">
							<div class="form-control-feedback">
								<i class="icon-user text-muted"></i>
							</div>
							<?php echo form_error('login'); ?>
						</div>

						<div class="form-group has-feedback has-feedback-left">
							<input type="password" class="form-control" placeholder="Senha" name="senha" required="required" value="<?php echo set_value('senha'); ?>">
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
							<?php echo form_error('senha'); ?>
						</div>

						<div class="form-group login-options">
							<div class="row">
								<div class="col-sm-6">
								</div>

								<!-- <div class="col-sm-6 text-right">
									<a href="<?php echo base_url(); ?>welcome/esqueceuSenha">Esqueceu a senha?</a>
								</div> -->
							</div>
						</div>

						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

						<div class="form-group">
							<button type="submit" class="btn bg-blue btn-block">Logar<i class="icon-arrow-right14 position-right"></i></button>
						</div>
						
					</div>
				</form>
				<!-- /form with validation -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- Prompt do IOS -->
	<div id="prompt_ios" class="animated bounceInDown">
		<div class="prompt_app_icon"><img src="<?php echo base_url(); ?>public/assets/images/icons_<?php echo GRUPOLOJA ?>/icon-96x96.png">
		</div>
		<p class="tit_prompt">Instale o Nosso Aplicativo</p>
		<p class="txt_prompt">Clique em <img src="data:image/svg+xml;base64,
	PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGlkPSJDYXBhXzEiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDU1MS4xMyA1NTEuMTMiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTUxLjEzIDU1MS4xMyIgd2lkdGg9IjUxMiI+PGc+PHBhdGggZD0ibTQ2NS4wMTYgMTcyLjIyOGgtNTEuNjY4djM0LjQ0NmgzNC40NDZ2MzEwLjAxMWgtMzQ0LjQ1N3YtMzEwLjAxMWgzNC40NDZ2LTM0LjQ0NmgtNTEuNjY5Yy05LjUyIDAtMTcuMjIzIDcuNzAzLTE3LjIyMyAxNy4yMjN2MzQ0LjQ1NmMwIDkuNTIgNy43MDMgMTcuMjIzIDE3LjIyMyAxNy4yMjNoMzc4LjkwMmM5LjUyIDAgMTcuMjIzLTcuNzAzIDE3LjIyMy0xNy4yMjN2LTM0NC40NTZjMC05LjUyLTcuNzAzLTE3LjIyMy0xNy4yMjMtMTcuMjIzeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzAwMDAwMCIgc3R5bGU9ImZpbGw6IzNDN0RGRiI+PC9wYXRoPjxwYXRoIGQ9Im0yNTguMzQyIDY1LjkzMXYyNDQuMDhoMzQuNDQ2di0yNDQuMDhsNzMuOTM3IDczLjkzNyAyNC4zNTQtMjQuMzU0LTExNS41MTQtMTE1LjUxNC0xMTUuNTE0IDExNS41MTQgMjQuMzU0IDI0LjM1NHoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiMwMDAwMDAiIHN0eWxlPSJmaWxsOiMzQzdERkYiPjwvcGF0aD48L2c+IDwvc3ZnPg==" /> e depois em <strong>"Adicionar à Tela de Início"</strong></p>
		<div class="btns_promt">
			<p class="btn_prompt btn_solo" id="btn_agoranao" onclick="esconderPopoup()">AGORA NÃO</p>
			<p class="btn_prompt" id="btn_nunca" style="display: none;">NÃO MOSTRAR NOVAMENTE</p>
		</div>
	</div>
	<!-- /Prompt do IOS -->

	<!-- Fundo Do Prompt do IOS-->
	<div class="over_prompt animated fadeIn" id="fundo-popoup"></div>
	<!-- / Fundo Do Prompt do IOS-->


	<!-- Footer -->
	<div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="footer text-white">
						&copy; 2018 - <?php echo date('Y') ?>. <a href="www.wdmtecnologia.com.br" target="_blank" class="text-white">WDM - Gestor</a> by <a href="#" class="text-white" target="_blank">Link Para o autor</a>
					</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="http://wdmtecnologia.com.br/" target="_blank">À WDM-Tecnologia</a></li>
					<li><a href="#">Serviços</a></li> <!-- Abrir um catalogo com todos os produtos da WDM -->
					<li><a href="https://api.whatsapp.com/send?phone=5581997724036&amp;text=Olá%20Digite%20sua%20mensagem." target="_blank">Contato</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /footer -->
	
	<script src="<?php echo base_url(); ?>script.js"></script> <!-- Modal JS preferences / -->

		<?php if(!(NULL === $this->session->flashdata('erro'))){ ?>
		
		<script type="text/javascript">
			        swal({
			            title: "Oops...",
			            text: "<?php echo $this->session->flashdata('erro'); ?>",
			            confirmButtonColor: "#EF5350",
			            type: "error"
			        });
		</script>

	

	<?php }
	$this->session->unset_userdata('erro');
	 ?>

	 	<!-- or another CDN | Arquivos para tornar o PWA compativel com o Safari -->
		<script async src="https://cdn.jsdelivr.net/npm/pwacompat" crossorigin="anonymous"></script>
		<script async src="https://unpkg.com/pwacompat" crossorigin="anonymous"></script>

</body>
</html>
