

				<!-- Table with togglable columns -->

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Mais vendido - <?php echo $detalhe['grupo']; ?>
						</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						Detalhamento - Grupo - Produto - <?php echo $total.' UND' ?>
					</div>

					<table class="table table-togglable table-hover">
						<thead>
							<tr>
								<th data-toggle="true">Produto</th>
								<th data-hide="phone">Est atual</th>
								<th data-hide="phone">Qtd vendida</th>
							<!-- 	<th data-hide="phone,tablet">Custo medio</th>
								<th data-hide="phone,tablet">Venda media</th>
								<th data-hide="phone">Lucro medio</th>
								<th data-hide="phone">Percentual</th> -->
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
							</tr>
						</thead>
						<tbody>

							<?php foreach ($dados as $v) { 
							$estoqueInicial = $total;
							$unidadesVendidas = $v->qtd;	
							// Cálculo da porcentagem vendida
							$porcentagemVendida = ($unidadesVendidas / $estoqueInicial) * 100;
							 ?> 
							<tr>
								<td><?php echo $v->produto_descricao . " - <span class='badge badge-pill bg-teal'>". round($porcentagemVendida,2)."% </span>" ?></td>
								<td><?php echo $v->produto_estoque; ?></td>
								<td><?php echo $v->qtd; ?></td>
							<!-- 	<td><span class="text-danger-600"><?php echo number_format($v->custo_medio * $v->qtd ,2,',','.');  ?></span></td>
								<td><span class="text-blue-600"><?php echo number_format($v->venda_media   * $v->qtd,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo number_format( $v->lucro_medio  * $v->qtd ,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo $v->percentual.' %';  ?></span></td> -->
								<td class="text-center">

								</td> 

							</tr>
							<?php } ?>  


						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>