                <!-- Stats with progress -->
                <h6 class="content-group text-semibold">
                    Vendas - Mês
                    <small class="display-block">Relatorios</small>
                </h6>

                <div class="row">

                <?php 
                $valorOpe    = 0;
                $repasseLoja = 0;
                $lucroMix    = 0;

                foreach ($dados as $v) {                   

                    $valorOpe    = $valorOpe    + $v->valorOperacao;
                    $repasseLoja = $repasseLoja + $v->repasseLoja;
                    $lucroMix    = $lucroMix    + $v->lucroMix;
                ?>    
                    <div class="col-md-3">

                        <!-- Invitation stats white -->
                        <div class="panel text-center" style="border-right: 5px solid red">

                            <div class="panel-body">
                                <h6 class="text-semibold no-margin-bottom mt-5"><a href="#"><?php echo $v->loja;  ?></a></h6>
                                <div class="text-muted content-group">Qtd. Operações: <?php echo $v->operacaoQtd;  ?><br>CPNJ: <?php echo $v->credenciadora;  ?></div>
								
                                <a href="#">
                                <div class="svg-center position-relative mb-5" id="<?php echo $v->loja;  ?>"></div>
                                </a>

                            </div>

                            <div class="panel-body panel-body-accent pb-15">
                                <div class="row">                              

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Total Geral</div>
                                        <h5 class="text-semibold no-margin text-blue-600"> <?php echo number_format($v->valorOperacao,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Rep. Lojista</div>
                                        <h5 class="text-semibold no-margin text-danger-600"> <?php echo number_format($v->repasseLoja,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Lucro MixPay</div>
                                        <h5 class="text-semibold no-margin text-teal-600"> <?php echo number_format($v->lucroMix,2,',','.');  ?></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /invitation stats white -->
                    </div>
                <?php } ?>  

                <!-- TOTAL GERAL --> 

                    <div class="col-md-3">
                        <!-- Invitation stats white -->
                        <div class="panel text-center">
                            <div class="panel-body">
                                <h6 class="text-semibold no-margin-bottom mt-5">TOTAL GERAL - <?php echo count($dados); ?> LOJAS</h6>
                      
                            </div> 

                            <div class="panel-body panel-body-accent pb-15">
                                <div class="row">                              

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Total Geral</div>
                                        <h5 class="text-semibold no-margin text-blue-600"><?php echo number_format($valorOpe,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Rep. Lojista</div>
                                        <h5 class="text-semibold no-margin text-danger-600"><?php echo number_format($repasseLoja,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Lucro MixPay</div>
                                        <h5 class="text-semibold no-margin text-teal-600"><?php echo number_format($lucroMix,2,',','.'); ?></h5>
                                    </div>

                                </div>


                            <div class="panel-body panel-body-accent pb-15">
                                <div class="row">                              

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Despesa MixPay</div>
                                        <h5 class="text-semibold no-margin text-danger-600"><?php echo number_format($despesa[0]->totalDespesa,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Sobra</div>
                                        <h5 class="text-semibold no-margin text-blue-600"><?php echo number_format($sobra[0]->totalSobra,2,',','.');  ?></h5>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="text-uppercase text-size-mini text-muted">Lucro Liq. MixPay</div>
                                        <h5 class="text-semibold no-margin text-teal-600"><?php echo number_format((($lucroMix - $despesa[0]->totalDespesa) + $sobra[0]->totalSobra),2,',','.'); ?></h5>
                                    </div>

                                </div>
                            </div>


                            </div>
                        </div>
                        <!-- /invitation stats white -->
                    </div>
                <!-- FIM TOTAL GERAL -->

   

                </div>
                <!-- /stats with progress -->


                <!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/general_widgets_stats.js"></script> -->
             

            <script type="text/javascript">


            $(function() { 

                <?php foreach ($dados as $v) { ?>  
                progressPercentage('#<?php echo $v->loja;  ?>', 46, 3, "#eee", "<?php echo $v->label;  ?>", <?php echo $v->percentual;  ?>);
                <?php } ?>

                progressPercentage('#progress_percentage_two', 46, 3, "#eee", "#EF6C00", 0.62);
                progressPercentage('#progress_percentage_three', 46, 3, "#eee", "#EF5350", 0.79);
                progressPercentage('#progress_percentage_four', 46, 3, "#eee", "#03A9F4", 0.62);
                progressPercentage('#progress_percentage_five', 46, 3, "#eee", "#00897B", 0.62);        



            // Animated progress with percentage count
            // ------------------------------

            // Initialize charts

                   

            // Chart setup
                    function progressPercentage(element, radius, border, backgroundColor, foregroundColor, end = 0) {


                        // Basic setup
                        // ------------------------------

                        // Main variables
                        var d3Container = d3.select(element),
                            startPercent = 0,
                            fontSize = 22,
                            endPercent = end,
                            twoPi = Math.PI * 2,
                            formatPercent = d3.format('.2%'),
                            boxSize = radius * 2;

                        // Values count
                        var count = Math.abs((endPercent - startPercent) / 0.01);

                        // Values step
                        var step = endPercent < startPercent ? -0.01 : 0.01;


                        // Create chart
                        // ------------------------------

                        // Add SVG element
                        var container = d3Container.append('svg');

                        // Add SVG group
                        var svg = container
                            .attr('width', boxSize)
                            .attr('height', boxSize)
                            .append('g')
                                .attr('transform', 'translate(' + radius + ',' + radius + ')');


                        // Construct chart layout
                        // ------------------------------

                        // Arc
                        var arc = d3.svg.arc()
                            .startAngle(0)
                            .innerRadius(radius)
                            .outerRadius(radius - border)
                            .cornerRadius(20);


                        //
                        // Append chart elements
                        //

                        // Paths
                        // ------------------------------

                        // Background path
                        svg.append('path')
                            .attr('class', 'd3-progress-background')
                            .attr('d', arc.endAngle(twoPi))
                            .style('fill', backgroundColor);

                        // Foreground path
                        var foreground = svg.append('path')
                            .attr('class', 'd3-progress-foreground')
                            .attr('filter', 'url(#blur)')
                            .style({
                                'fill': foregroundColor,
                                'stroke': foregroundColor
                            });

                        // Front path
                        var front = svg.append('path')
                            .attr('class', 'd3-progress-front')
                            .style({
                                'fill': foregroundColor,
                                'fill-opacity': 1
                            });


                        // Text
                        // ------------------------------

                        // Percentage text value
                        var numberText = svg
                            .append('text')
                                .attr('dx', 0)
                                .attr('dy', (fontSize / 2) - border)
                                .style({
                                    'font-size': fontSize + 'px',
                                    'line-height': 1,
                                    'fill': foregroundColor,
                                    'text-anchor': 'middle'
                                });


                        // Animation
                        // ------------------------------

                        // Animate path
                        function updateProgress(progress) {
                            foreground.attr('d', arc.endAngle(twoPi * progress));
                            front.attr('d', arc.endAngle(twoPi * progress));
                            numberText.text(formatPercent(progress));
                        }

                        // Animate text
                        var progress = startPercent;
                        (function loops() {
                            updateProgress(progress);
                            if (count > 0) {
                                count--;
                                progress += step;
                                setTimeout(loops, 10);
                            } else {
                                updateProgress(end);
                            }
                        })();
                        }


            });

            </script>  