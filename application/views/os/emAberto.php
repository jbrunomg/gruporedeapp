<h6 class="content-group text-semibold">
    Os em Aberto + <?php echo $this->uri->segment(3); ?> dias
    <small class="display-block">Relatórios</small>
</h6>
<div class="row">
    <?php foreach ($dados as $v) { ?>
        <div class="col-md-3">
            <div class="panel text-center" style="border-right: 5px solid red">
                <div class="panel text-center">
                <div class="panel-body">
                    <h6 class="text-semibold no-margin-bottom mt-5">
                        <a href="<?php echo base_url(); ?>Os/emAbertoDetalhe/<?php echo $v->loja . '/' . $this->uri->segment(3); ?>"><?php echo $v->loja; ?></a>
                    </h6>
                    <div class="text-muted content-group">Qtd. OS: <?php echo $v->osQtd; ?></div>
                    <a href="<?php echo base_url(); ?>Os/emAbertoDetalhe/<?php echo $v->loja . '/' . $this->uri->segment(3); ?>">
                        <div class="svg-center position-relative mb-5" id="<?php echo $v->loja; ?>">
                        </div>
                    </a>
                </div>
                <div class="panel-body panel-body-accent pb-15">
                    <div class="row">
                    <div class="col-xs-4">
                        <div class="text-uppercase text-size-mini text-muted">Total</div>
                        <h5 class="text-semibold no-margin text-blue-600"> <?php echo number_format(($v->totalServ + $v->totalProd), 2, ',', '.'); ?> </h5>
                    </div>
                    <div class="col-xs-4">
                        <div class="text-uppercase text-size-mini text-muted">Custo</div>
                        <h5 class="text-semibold no-margin text-danger-600"> <?php echo number_format($v->custoTotal, 2, ',', '.'); ?> </h5>
                    </div>
                    <div class="col-xs-4">
                        <div class="text-uppercase text-size-mini text-muted">Lucro</div>
                        <h5 class="text-semibold no-margin text-teal-600"> <?php echo number_format((($v->totalServ + $v->totalProd) - $v->custoTotal), 2, ',', '.'); ?> </h5>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    <?php } ?> 
</div>

<script type="text/javascript">
    $(function() {
        <?php foreach ($dados as $v) { ?>  
            progressPercentage('#<?php echo $v->loja; ?>', 46, 3, "#eee", "<?php echo $v->label; ?>", <?php echo $v->percentual; ?>);
        <?php } ?>

        function progressPercentage(element, radius, border, backgroundColor, foregroundColor, end = 0) {
            var d3Container = d3.select(element),
                startPercent = 0,
                fontSize = 22,
                endPercent = end,
                twoPi = Math.PI * 2,
                formatPercent = d3.format('.2%'),
                boxSize = radius * 2;

            var count = Math.abs((endPercent - startPercent) / 0.01);
            var step = endPercent < startPercent ? -0.01 : 0.01;
            var container = d3Container.append('svg');

            var svg = container
                .attr('width', boxSize)
                .attr('height', boxSize)
                .append('g')
                .attr('transform', 'translate(' + radius + ',' + radius + ')');

            var arc = d3.svg.arc()
                .startAngle(0)
                .innerRadius(radius)
                .outerRadius(radius - border)
                .cornerRadius(20);

            svg.append('path')
                .attr('class', 'd3-progress-background')
                .attr('d', arc.endAngle(twoPi))
                .style('fill', backgroundColor);

            var foreground = svg.append('path')
                .attr('class', 'd3-progress-foreground')
                .attr('filter', 'url(#blur)')
                .style({
                    'fill': foregroundColor,
                    'stroke': foregroundColor
                });

            var front = svg.append('path')
                .attr('class', 'd3-progress-front')
                .style({
                    'fill': foregroundColor,
                    'fill-opacity': 1
                });

            var numberText = svg
                .append('text')
                    .attr('dx', 0)
                    .attr('dy', (fontSize / 2) - border)
                    .style({
                        'font-size': fontSize + 'px',
                        'line-height': 1,
                        'fill': foregroundColor,
                        'text-anchor': 'middle'
                    });

            function updateProgress(progress) {
                foreground.attr('d', arc.endAngle(twoPi * progress));
                front.attr('d', arc.endAngle(twoPi * progress));
                numberText.text(formatPercent(progress));
            }

            var progress = startPercent;
            (function loops() {
                updateProgress(progress);
                if (count > 0) {
                    count--;
                    progress += step;
                    setTimeout(loops, 10);
                } else {
                    updateProgress(end);
                }
            })();
        }
    });
</script>