<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Os em Aberto <?php echo $this->uri->segment(3) ?> </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        Os em Aberto + <?php echo $this->uri->segment(4); ?> dias - Detalhe
    </div>

    <table class="table table-togglable table-hover">
        <thead>
            <tr>
                <th data-toggle="true">Cliente</th>
                <th data-hide="phone">Entrada</th>
                <th data-hide="phone,tablet">Equipamento</th>
                <th data-hide="phone">Situação</th>
                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $v) { ?>
                <tr>
                    <td>
                        #<?php echo $v->osId . ' | ' . $v->cliente; ?> |

                        <?php
                            if ($this->uri->segment(4) <= 7) {
                                echo '<span class="badge bg-success badge-pill">Verde</span>';
                            } else if ($this->uri->segment(4) <= 12) {
                                echo '<span class="badge badge-pill" style="background: #FFE400;">Amarelo</span>';
                            } else if ($this->uri->segment(4) <= 16) {
                                echo '<span class="badge bg-warning-400 badge-pill">Laranja</span>';
                            } else if ($this->uri->segment(4) <= 21) {
                                echo '<span class="badge bg-danger badge-pill">Vermelho</span>';
                            } else {
                                echo '<span class="badge badge-pill" style="background: #592E03;">Marrom</span>';
                            }
                        ?>
                    </td>
                    <td><?php echo date('d/m/Y', strtotime($v->dataInicial)); ?></td>
                    <td><?php echo $v->equipamento; ?></td>
                    <td><?php echo $v->status; ?></td>
                    <td></td>
                </tr>
            <?php } ?> 
        </tbody>
    </table>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>