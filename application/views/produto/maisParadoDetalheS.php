				







				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Produtos parados <?php echo $dados[0]->loja ?> </h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                		
		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento Semenstral- Produto.
					</div>



					<table class="table table-togglable table-hover">

						<thead>

							<tr>

								<th data-toggle="true">Produto</th>
								<th data-hide="phone">Estoque</th>
								<th data-hide="phone,tablet">Ult. Venda</th>
								<!-- <th data-hide="phone,tablet">Venda media</th>
								<th data-hide="phone">Ultima</th> -->
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>

							</tr>

						</thead>

						<tbody>


							<?php foreach ($dados as $v) {     ?> 
							<tr>

								<td><?php echo $v->produto_descricao; ?></td>
								<td><?php echo $v->estoque; ?></td>
								<td><span class="text-danger-600"><?php echo date('d/m/Y', strtotime($v->produto_data_ultima_venda)); ?></span></td>

								<!-- <td><span class="text-blue-600">3,42</span></td>

								<td><span class="text-teal-600">352,80</span></td> -->

								<td class="text-center">

								<!-- <ul class="icons-list">

										<li class="dropdown">

											<a href="#" class="dropdown-toggle" data-toggle="dropdown">

												<i class="icon-menu9"></i>

											</a>



											<ul class="dropdown-menu dropdown-menu-right">

												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>

												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>

												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>

											</ul>

										</li>

									</ul> -->

								</td> 

							</tr>
							<?php } ?> 
							

						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>