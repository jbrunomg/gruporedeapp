<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Produtos parados + <?php echo $this->uri->segment(4); ?> dias - <?php echo $this->uri->segment(3); ?></h5>

        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        Detalhamento - Produto
    </div>

    <table class="table table-togglable table-hover">
        <thead>
            <tr>
                <th data-toggle="true">Produto</th>
                <th data-hide="phone">Estoque</th>
                <th data-hide="phone,tablet">Ult. Venda</th>
                <th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dados as $v) {     ?> 
            <tr>
                <td><?php echo $v->produto_descricao; ?></td>
                <td><?php echo $v->estoque; ?></td>
                <td><span class="text-danger-600"><?php echo date('d/m/Y', strtotime($v->produto_data_ultima_venda)); ?></span></td>
                <td class="text-center"></td> 
            </tr>
            <?php } ?> 
        </tbody>
    </table>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>