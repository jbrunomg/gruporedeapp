

				<!-- Table with togglable columns -->

				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Estoque Grupo - <?php echo $dados[0]->loja ?></h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						Detalhamento - Grupo.
					</div>

					<table class="table table-togglable table-hover">
						<thead>
							<tr>
								<th data-toggle="true">Grupo</th>
								<th data-hide="phone">Estoque</th>
								<th data-hide="phone,tablet">P. Custo</th>
								<th data-hide="phone,tablet">P. Venda</th>
								<th data-hide="phone">Lucro</th>
								<th data-hide="phone">Percentual</th>
								<th data-hide="phone">...</th>
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>
							</tr>
						</thead>
						<tbody>

                			<?php foreach ($dados as $v) {     ?>   
							<tr>
								<td><?php echo $v->produto_descricao; ?></td>
								<td><?php echo $v->produto_estoque; ?></td>
								<td><span class="text-danger-600"><?php echo number_format($v->produto_preco_custo,2,',','.');  ?></span></td>
								<td><span class="text-blue-600"><?php echo number_format($v->produto_preco_venda,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo number_format($v->lucro,2,',','.');  ?></span></td>
								<td><span class="text-teal-600"><?php echo $v->percentual.' %';  ?></span></td>
								<td><a href="<?php echo base_url(); ?>Produto/valorEstoqueGrupoDetalhe/<?php echo $v->loja.'/'.urlencode($v->produto_descricao);  ?>"><span class="label label-danger">Detalhado</span></a></td>


 								<td class="text-center">

								<!-- <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
											</ul>
										</li>
									</ul> -->
								</td> 
							</tr>
							<?php } ?>  

						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>