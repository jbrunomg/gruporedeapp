					
					<!-- Basic responsive configuration -->

					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Detalhe Estoque - <?php  echo $dados[0]->loja ?></h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							Detalhamento - Produto.
						</div>

						<table class="table datatable-responsive">
							<thead>
								<tr>
									<th>Produto</th>
									<th>Estoque</th>									
									<th>P. Custo</th>
									<th>P. Venda</th>									
									<th>Grupo</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($dados as $v) {     ?>   
								<tr>
									<?php if ($v->produto_estoque > 0) { ?>
									<td><?php echo $v->produto_descricao.' | '.' <span class="badge bg-success badge-pill">'.'COM - '.$v->produto_estoque.'</span>'  ?></td>
									<?php } else { ?>
									<td><?php echo $v->produto_descricao.' | '.' <span class="badge bg-danger-400 badge-pill">'.'SEM - '.$v->produto_estoque.'</span>'  ?></td>
									<?php } ?>
									<td><?php echo $v->produto_estoque; ?></td>
									<td><?php echo number_format($v->produto_preco_custo,2,',','.');  ?></td>
									<td><?php echo number_format($v->produto_preco_venda,2,',','.');  ?></td>				
									<td><?php echo $v->categoria_prod_descricao; ?></td>
									<td class="text-center">
						<!-- 				<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
													<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
													<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
												</ul>
											</li>
										</ul> -->
									</td>

								</tr>
								<?php } ?>  
					
					
			
	<!-- 						<tr>
									<td>Cicely</td>
									<td>Sigler</td>
									<td><a href="#">Senior Research Officer</a></td>
									<td>15 Mar 1960</td>
									<td><span class="label label-info">Pending</span></td>
									<td class="text-center">
										<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
													<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
													<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
												</ul>
											</li>
										</ul>
									</td>
								</tr> -->


							</tbody>
						</table>
					</div>
					<!-- /basic responsive configuration -->


	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/datatables_responsive.js"></script>




	