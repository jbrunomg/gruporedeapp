<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header"> <center><?php echo date("d/m/Y").' '. date("h:i:sa") ?></center>
			<a class="navbar-brand" href="<?php echo base_url(); ?>Dashboard"><img src="<?php echo base_url(); ?>public/assets/images/logo_light_<?php echo GRUPOLOJA ?>.png" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<div></div> <!--  Melhoria Visual mobile -->

			<ul class="nav navbar-nav">
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				<?php 
			
			    if (isset($dadostce[0]->total)) {
			      $totaltce = $dadostce[0]->total;                  
                } else {
                  $totaltce = 0; 
                }

                if (isset($dadosta[0]->total)) {
                  $totalta = $dadosta[0]->total;                 
                } else {
                   $totalta = 0;		
                }
				
				?>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-git-compare"></i>
						<span class="visible-xs-inline-block position-right">Notificações</span>
						<span class="badge bg-warning-400" id="somaNotificacao"></span>
					</a>
					
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-heading">
							Notificações
							<ul class="icons-list">
								<li><a href="#"><i class="icon-sync"></i></a></li>
							</ul>
						</div>

<!-- 						<ul class="media-list dropdown-content-body width-350">
							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTce" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>

								<div class="media-body">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTce">M.A </a> <span class="text-semibold">Mensalidade</span> em aberto.
									<div class="media-annotation">Total: <span id="tceNotificacao">1</span></div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTa" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
								</div>
								
								<div class="media-body">
									<a href="<?php echo base_url(); ?>contratoestudante/notificacaoTa">P.E </a> <span class="text-semibold">Produto</span> abaixo do estoque.
									<div class="media-annotation">Total: <span id="taNotificacao"></span></div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
								</div>
								
								<div class="media-body">									
									<a href="#">Contrato </a> <span class="text-semibold">Vencido</span> no sistema.
									<div class="media-annotation">Total </div>
								</div>
							</li>

						</ul> -->

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="Toda atividade"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>				
			</ul>



			<ul class="nav navbar-nav navbar-right">			
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo $this->session->userdata['usuario_imagem']; ?>" alt="">
						<span><?php echo $this->session->userdata('usuario_nome');?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?php echo base_url() ?>Usuario/visualizar/<?php echo $this->session->userdata['usuario_id']; ?>"><i class="icon-user-plus"></i> Meu Perfil</a></li>
						<li class="divider"></li>
						<!-- <li><a href="#"><i class="icon-cog5"></i> Configuração</a></li> -->
						<li><a href="<?php echo base_url() ?>Auth/sair"><i class="icon-switch2"></i> Sair</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main  sidebar-default">
				<div class="sidebar-content">

					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="sidebar-user-material">
							<div class="category-content">
								<div class="sidebar-user-material-content">
									<a href="#"><img src="<?php echo base_url(); ?>public/assets/usuarios/<?php echo GRUPOLOJA ?>.png" class="img-circle img-responsive" alt=""></a>

									<h6><?php echo $this->session->userdata('usuario_nome');?></h6>
									<span class="text-size-small"><?php echo $this->session->userdata('permissao_nome');?></span>
								</div>
															
								<div class="sidebar-user-material-menu">
									<a href="#user-nav" data-toggle="collapse"><span>Minha Conta</span> <i class="caret"></i></a>
								</div>
							</div>
							
							<div class="navigation-wrapper collapse" id="user-nav">
								<ul class="navigation">
									<li><a href="<?php echo base_url() ?>Usuario/visualizar/<?php echo $this->session->userdata['usuario_id']; ?>"><i class="icon-user-plus"></i> <span>Meu Perfil</span></a></li>
									
									<!-- <li><a href="<?php echo base_url() ?>dashboard/sistemaOpenClose" id="btn-finalizarVenda"><i class="icon-target2"></i> <span>Abrir/Fechar Sistema</span></a></li> -->
												

									<li><a  style="cursor:pointer;" data-toggle="modal" data-target="#modalSenha"><i class="icon-target2"></i> <span>Abrir/Fechar Sistema</span></a></li>
									
									<li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
									<li class="divider"></li>
									<!-- <li><a href="#"><i class="icon-cog5"></i> <span>Configuração</span></a></li> -->
									<li><a href="<?php echo base_url() ?>Auth/sair"><i class="icon-switch2"></i> <span>Sair</span></a></li>
								</ul>
							</div>
						</div>

						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="<?php echo base_url(); ?>Dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
									<a href=""><i class="icon-file-stats"></i> <span>Vendas</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Vendas/vendasDia">Dia</a></li>
										<li><a href="<?php echo base_url(); ?>Vendas/vendasMes">Mês</a></li>
										<li><a href="<?php echo base_url(); ?>Vendas/vendasAno">Ano</a></li>	
										<li><a href="<?php echo base_url(); ?>Vendas/vendasAnoAnterior">Ano - Anterior</a></li>									
									</ul>
								</li>
							
							<?php if(GRUPOLOJA == 'gruporeccell') { ?>

								<li>
									<a href=""><i class="icon-file-stats"></i> <span>Vendas - Celular</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Vendascell/vendasDia">Dia</a></li>
										<li><a href="<?php echo base_url(); ?>Vendascell/vendasMes">Mês</a></li>
										<li><a href="<?php echo base_url(); ?>Vendascell/vendasAno">Ano</a></li>	
										<li><a href="<?php echo base_url(); ?>Vendascell/vendasAnoAnterior">Ano - Anterior</a></li>									
									</ul>
								</li>

							<?php } ?>	

								<li>
									<a href=""><i class="icon-certificate"></i> <span>OS - Ordem de Serviço</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Os/osDia">Dia</a></li>
										<li><a href="<?php echo base_url(); ?>Os/osMes">Mês</a></li>
										<li><a href="<?php echo base_url(); ?>Os/osAno">Ano</a></li>																			
										<li>
											<a href="">Em aberto</a>
											<ul>
												<li><a href="<?php echo base_url(); ?>Os/emAberto/7">+ 7 Dias <span class="badge bg-success badge-pill" id="os7">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Os/emAberto/12">+ 12 Dias <span class="badge badge-pill" style="background: #FFE400;" id="os12">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Os/emAberto/16">+ 16 Dias <span class="badge bg-warning-400 badge-pill" id="os16">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Os/emAberto/21">+ 21 Dias <span class="badge bg-danger badge-pill" id="os21">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Os/emAberto/30">+ 30 Dias <span class="badge badge-pill" style="background: #592E03;" id="os30">0</span></a></li>
											</ul>
										</li>
									</ul>
								</li>

								<li>
									<a href=""><i class="icon-stack2"></i> <span>Produto</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Produto/valorEstoque">Valor Estoque</a></li>										
										<li>
											<a href="">Mais Vendidos</a>
											<ul>
												<li><a href="<?php echo base_url(); ?>Produto/maisVendido">Mês</a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisVendidoT">Trimestre</a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisVendidoS">Semestre</a></li>
											</ul>
										</li>
										<li>
											<a href="">Parado Estoque</a>
											<ul>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoGenerico/7">+ 7 Dias <span class="badge bg-success badge-pill" id="pr7">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoGenerico/12">+ 12 Dias <span class="badge badge-pill" style="background: #FFE400;" id="pr12">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoGenerico/16">+ 16 Dias <span class="badge bg-warning-400 badge-pill" id="pr16">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoGenerico/21">+ 21 Dias <span class="badge bg-danger badge-pill" id="pr21">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParado">+ 30 Dias <span class="badge badge-pill" style="background: #592E03;" id="pr30">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoT">+ 90 Dias <span class="badge badge-pill" style="background: #592E03;" id="pr90">0</span></a></li>
												<li><a href="<?php echo base_url(); ?>Produto/maisParadoS">+ 180 Dias <span class="badge badge-pill" style="background: #592E03;" id="pr180">0</span></a></li>
											</ul>
										</li>


									</ul>
								</li>

								<li>
									<a href=""><i class="icon-coins"></i> <span>Faturamento</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Faturamento/previsaoFinanceiro">Previsão Faturamento (Mês)</a></li>
										<li><a href="<?php echo base_url(); ?>Faturamento/previsaoFinanceiroAno">Previsão Faturamento (Ano)</a></li>																			
																								
									</ul>
								</li>

								<li>
									<a href=""><i class="icon-cash"></i> <span>Financeiro</span></a>
									<ul>										
										<li><a href="<?php echo base_url(); ?>Financeiro/financeiroDia">Receita/Despesa (Dia)</a></li>		
										<li><a href="<?php echo base_url(); ?>Financeiro/financeiroMes">Receita/Despesa (Mês)</a></li>		
										<li><a href="<?php echo base_url(); ?>Financeiro/financeiroAno">Receita/Despesa (Ano)</a></li>							
																								
									</ul>
								</li>


								<li>
									<a href=""><i class="icon-user-plus"></i> <span>Clientes</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>Cliente/top10Mes">Top 20 (Mês)</a></li>
										<li><a href="<?php echo base_url(); ?>Cliente/top10Ano">Top 20 (Ano)</a></li>
										<li><a href="<?php echo base_url(); ?>Cliente/top10AnoAnterior">Top 20 (Ano - Anterior)</a></li>
										<li><a href="<?php echo base_url(); ?>Cliente/pagamentoAberto">Pagamento em Aberto</a></li>																		
									</ul>
								</li>

								<li>
									<a href=""><i class="icon-users"></i> <span>Vendedores</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>vendedor/top10Dia">Top 10 (Dia)</a></li>
										<li><a href="<?php echo base_url(); ?>vendedor/top10Mes">Top 10 (Mês)</a></li>
										<li><a href="<?php echo base_url(); ?>vendedor/top10Ano">Top 10 (Ano)</a></li>																		
									</ul>
								</li>		


							<?php if(GRUPOLOJA == 'grupocell') { ?>

								<li>
									<a href=""><i class="icon-file-stats"></i> <span>MixPay</span></a>
									<ul>
										<li><a href="<?php echo base_url(); ?>mixpay/operacaoDiaAnterior">Dia - Anterior</a></li>
										<li><a href="<?php echo base_url(); ?>mixpay/operacaoMesAnterior">Mês - Anterior</a></li>
										<li><a href="<?php echo base_url(); ?>mixpay/operacaoMes">Mês</a></li>
										<li><a href="<?php echo base_url(); ?>mixpay/operacaoAno">Ano</a></li>	
									<!-- <li><a href="<?php echo base_url(); ?>MixPay/operacaoAnoAnterior">Ano - Anterior</a></li>	-->
										<li><a href="<?php echo base_url(); ?>mixpay/previsaoFinanceiro">Previsão Faturamento (Mês)</a></li>
									</ul>
								</li>

							<?php } ?>						
								
							</ul>
						</div>
					</div>	

					<!-- /main navigation --> 

				</div>
			</div>
			<!-- /main sidebar -->

<script>
	$.ajax({
		url: '<?php echo base_url(); ?>Os/quantidadesOs',
		type: 'GET',
		success: function(data) {
			var data = JSON.parse(data);

			$("#os7").text(data['7'][0].osQtdTotal);
			$("#os12").text(data['12'][0].osQtdTotal);
			$("#os16").text(data['16'][0].osQtdTotal);
			$("#os21").text(data['21'][0].osQtdTotal);
			$("#os30").text(data['30'][0].osQtdTotal);
		},
	});

	$.ajax({
		url: '<?php echo base_url(); ?>Produto/quantidadesProdutosParados',
		type: 'GET',
		success: function(data) {
			var data = JSON.parse(data);

			$("#pr7").text(data['7'][0].produtoQtdTotal);
			$("#pr12").text(data['12'][0].produtoQtdTotal);
			$("#pr16").text(data['16'][0].produtoQtdTotal);
			$("#pr21").text(data['21'][0].produtoQtdTotal);
			$("#pr30").text(data['30'][0].produtoQtdTotal);
			$("#pr90").text(data['90'][0].produtoQtdTotal);
			$("#pr180").text(data['180'][0].produtoQtdTotal);
		},
	});
</script>