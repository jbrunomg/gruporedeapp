				</div>
				<!-- /main content -->

			</div>
			<!-- /page content -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->


  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>




</body>

 <!--  MODAL  -->
 <div class="modal fade" id="modalSenha" data-backdrop="static" data-keyboard="false" style="z-index: 99999" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 350px; margin: 100px auto">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="closeModal()" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-center">Informar senha</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p>Informe a senha do gerente para liberação desta ação</p>
              <div class="form-group">
                <input type="password" onkeyup="habilitarBTN(this.value)" id="input-liberar-senha" class="form-control" placeholder="Senha do gerente" />
              </div>
              <button id="btn-confirm" disabled class="btn btn-primary btn-block">CONFIRMAR</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <script type="text/javascript">

    function habilitarBTN(valor) {
      const el = document.querySelector('#btn-confirm')
      if(valor.length > 0) {
        el.disabled = false
      } else {
          el.disabled = true
      }
    }

    function closeModal() {
        $('#modalSenha').modal('hide');
        document.querySelector('#input-liberar-senha').value = null;
        const el = document.querySelector('#btn-confirm')
        el.disabled = true
    }

    $('#btn-confirm').click(function(event) {

        let senha = $('#input-liberar-senha').val();

        $.ajax({
          method: "POST",
          url: base_url+"dashboard/validacaoGerente/",
          dataType: "JSON",
          data: { senha },
          success: function(data)
          {
            if(data.result == true){
				    //alert('Chamar a função de fechamento');
            closeModal();


            $.ajax({
            method: "POST",
            url: base_url+"dashboard/sistemaOpenClose",
            dataType: "JSON",
            data: "",
            success: function(data)
            {            
              if(data.result == 0){
               // alert('Sistema - Aberto');

                swal({
                  title: "Tem certeza?",
                  text: "Uma vez ativado, as lojas do GrupoRede ficarão sem acesso ao sistema.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {

                  $.post("<?php echo base_url() ?>/Dashboard/desativarLoja", "emitente_botao_panico='1'", function( data ) {
                      console.log(data);
                  }),

                    swal("OK! Lojas GrupoRede desativado.", {
                      icon: "success",
                    },    

                    );
                  } else {
                    swal("Ação cancelada!");
                  }
                });     

              










              }else{
                 // alert('Sistema - Fechado');


                  swal({
                    title: "Tem certeza?",
                    text: "Uma vez desativado, as lojas do GrupoRede ficarão com acesso ao sistema.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((willDelete) => {
                    if (willDelete) {
                      
                    $.post("<?php echo base_url() ?>/Dashboard/ativarLoja", "emitente_botao_panico='0'", function( data ) {
                        console.log(data);
                    }),

                      swal("OK! Lojas GrupoRede ativado.", {
                        icon: "success",
                      },    

                      );
                    } else {
                      swal("Ação cancelada!");
                    }
                  });  




              }
            }
              
            });  








            }else{
              alert('Senha Invalida.');
            //  location.reload();
            }
          }
          
        });
    }); 

</script>

</html>


