<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>APP - <?php echo strtoupper(GRUPOLOJA) ?></title>
	
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/assets/images/logo_icon_dark.png"/>


<!-- iPhone SE (1ª geração): 640 x 1136 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-640x1136.png">
<!-- iPhone SE (2ª geração): 750 x 1334 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-750x1334.png">
<!-- iPhone 6, 6s, 7, 8: 750 x 1334 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-750x1334.png">
<!-- iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus: 1080 x 1920 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1080x1920.png">

<!-- iPhone 8 Plus, 7 Plus, 6s Plus, 6 Plus (1242px x 2208px) -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1242x2208.png">

<!-- iPhone X, XS, 11 Pro: 1125 x 2436 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1125x2436.png">
<!-- iPhone XR, 11: 828 x 1792 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-828x1792.png"> 
<!-- iPhone XS Max, 11 Pro Max: 1242 x 2688 pixels -->
	    <link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1242x2688.png">
<!-- iPhone 12 mini: 1080 x 2340 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1080x2340.png">

<!-- iPhone 12, 13: 1170 x 2532 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1170x2532.png">

<!-- iPhone 12 Pro, 13 Pro: 1170 x 2532 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1170x2532.png">
<!-- iPhone 12 Pro Max, 13 Pro Max: 1284 x 2778 pixels -->
		<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="<?php echo base_url(); ?>public/assets/images/splash_<?php echo GRUPOLOJA ?>/apple-launch-1284x2778.png">




	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->


    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files --> 
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/charts/google/pies/donut_rotate.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/d3/d3_tooltip.js"></script> 

    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/switchery.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>  
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/ui/moment/moment.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/noty.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/jgrowl.min.js"></script>
  

    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/ui/ripple.min.js"></script>    
    <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/components_notifications_other.js"></script>

    <!-- /theme JS files -->

    <!-- GRAFICO APP-->
 	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/general_widgets_stats.js"></script> -->
 	<!-- /GRAFICO APP-->

	<meta name="theme-color" content="#3f51b5"> <!--Define a cor da barra superior do navegador-->
    <link rel="manifest" href="<?php echo base_url(); ?>manifest.webmanifest"> 
	
	<!-- <script src="<?php echo base_url(); ?>public/script.js"></script> <!-- Modal JS preferences / -->


	<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	var token = '<?php echo $this->security->get_csrf_hash(); ?>';
		
	</script>



</head>

<body class="navbar-bottom">

		
