<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Visualizar Usuário</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<!-- <li><a data-action="reload"></a></li> -->
			                		<!-- <li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url();?>usuario/editarExe" method="post" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Dados Pessoais:</legend>

									<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

									<input disabled type="hidden" name="usuario_id" value="<?php echo $dados[0]->usuario_id; ?>" />
 
							

									<div class="form-group">
										<label class="control-label col-lg-2">CPF:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" placeholder="" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="cpf" id="cpf" value="<?php echo $dados[0]->usuario_cpf; ?>">
										<?php echo form_error('cpf'); ?>
										</div>										
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Nome:</label>
										<div class="col-lg-5">
											<input disabled type="text" class="form-control" placeholder="Nome do Usuário" name="nome" id="nome" value="<?php echo $dados[0]->usuario_nome; ?>">
										<?php echo form_error('nome'); ?>
										</div>										
									</div>

							

									<legend class="text-bold">Dados Acesso:</legend>
									<div class="form-group">
										<label class="control-label col-lg-2">Email/login:</label>
										<div class="col-lg-5">
											<input disabled type="email" name="email"  placeholder="seu@email.com" class="form-control" name="email" id="email" value="<?php echo $dados[0]->usuario_email; ?>">
										<?php echo form_error('email'); ?>
										</div>										 
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Senha:</label>
										<div class="col-lg-5">
											<input disabled type="password" class="form-control" name="senha" id="senha" value="<?php echo set_value('senha'); ?>">
										<?php echo form_error('senha'); ?>
										</div>										
									</div>

									<div class="form-group">
			                        	<label class="control-label col-lg-2">Perfil:</label>
			                        	<div class="col-lg-5">
				                            <select disabled class="form-control" name="permissao" id="permissao">
				                           		<option value="">Selecione</option>
				                                <?php foreach ($permissoes as $valor) { ?>
				                                	<?php $selected = ($valor->perfil_id == $dados[0]->usuario_perfil)?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->perfil_id; ?>" <?php echo $selected; ?>><?php echo $valor->perfil_descricao; ?></option>
				  		                        <?php } ?>
				                               
				                            </select>
			                            </div>
			                        </div>
															
							
			                       
							</form>
						</div>
					</div>
