				







				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Venda Mês -  <?php echo $dados[0]->loja ?> </h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                		
		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento  - Clientes.
					</div>


					<table class="table table-togglable table-hover">

						<thead>

							<tr>

					 			<th data-toggle="true">Mes</th>			
								<th data-hide="phone">...</th> 
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>

							</tr>

						</thead>

						<tbody>


						<?php  
							$mesV2 = ''; // Inicializa a variável mesV2 vazia para controle do mês anterior.

							foreach($dados as $v) { 

							    // Verifica se o mês atual (v->mes) é diferente do mês do ciclo anterior (mesV2).
							    if ($mesV2 != $v->mes) { 

							        // Imprime o mês e as informações dos clientes para aquele mês.
							        echo '<tr>';
							        echo '<td>' . $v->mese . '</td>';  // Imprime o mês (v->mese) apenas uma vez
							        echo '<td><a href="' .base_url().'vendas/clienteGrupo/'.$v->loja.'/'.$v->ano.'/'.$v->mes.'"><span class="label label-danger">Detalhado</span></a></td>';    // Imprime os dados dos clientes (linha)
							        echo '</tr>';

							        // Atualiza o mês anterior para o mês atual para controle na próxima iteração.
							        $mesV2 = $v->mes;
							    }
							}  
						?>



		
							

						</tbody>

					</table>



				</div>

				<!-- /table with togglable columns -->







				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

				<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>