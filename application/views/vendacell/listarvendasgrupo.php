




				<!-- Table with togglable columns -->

				<div class="panel panel-flat">

					<div class="panel-heading">

						<h5 class="panel-title">Grupo Venda <?php echo $detalhe['dia'].'/'.$detalhe['mes'].'/'.$detalhe['ano'].' - '.$detalhe['loja'] ?> </h5>

						<div class="heading-elements">

							<ul class="icons-list">

		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                		
		                	</ul>

	                	</div>

					</div>



					<div class="panel-body">

						Detalhamento  - Grupo Vendas.
					</div>



					<table class="table table-togglable table-hover">

						<thead>

							<tr>

								<th data-toggle="true">Grupo</th>
								<th data-hide="phone,tablet">Total</th>
								<th data-hide="phone">Custo</th>								
								<th data-hide="phone,tablet">Lucro</th>
								<th data-hide="phone">Percentual</th>
								<th data-hide="phone">....</th>								
								<th class="text-center" style="width: 30px;"><i class="icon-menu-open2"></i></th>

							</tr>

						</thead>

						<tbody>


							<?php  foreach ( $dados as $v  ) { 

							$porcentagem = number_format( (($v['total'] / $v['custo'])*100)-100  ,2,",","."); 

							?> 						

							<tr>

								<td><?php echo $v['grupo'] ?></td>	

								<td><span><?php echo number_format($v['total'] ,2,',','.'); ?></span></td>							

								<td><span class="text-danger-600"><?php echo number_format($v['custo'] ,2,',','.'); ?></span></td>				

								<td><span class="text-blue-600"><?php echo number_format(($v['total'] - $v['custo']) ,2,',','.'); ?></span></td>

								<td><span class="text-teal-600"><?php echo $porcentagem.'%'; ?></span></td>

								<td><a href="<?php echo base_url(); ?>vendas/vendasGrupoDetalhe/<?php echo $v['grupo'].'/'.$detalhe['loja'].'/'.$detalhe['ano'].'/'.$detalhe['mes'].'/'.$detalhe['dia'];  ?>"><span class="label label-danger">Detalhado</span></a></td>

								<td class="text-center">

								<!-- <ul class="icons-list">

										<li class="dropdown">

											<a href="#" class="dropdown-toggle" data-toggle="dropdown">

												<i class="icon-menu9"></i>

											</a>



											<ul class="dropdown-menu dropdown-menu-right">

												<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>

												<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>

												<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>

											</ul>

										</li>

									</ul> -->

								</td> 

							</tr>
							<?php  }   ?> 


						</tbody>

					</table>

				</div>

				<!-- /table with togglable columns -->


				<!-- Basic pie charts -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Representação valor total</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						<!-- <p class="content-group">By default, <code>donut</code> charts begin with the left edge of the first slice pointing straight up. You can change that with the <code>pieStartAngle</code> option. Here, we rotate the chart clockwise 180 degrees with an option of <code>pieStartAngle: 180</code>. So chosen because that particular angle happens to make the slices reflected.</p> -->

						<div class="chart-container text-center">
							<div class="display-inline-block" id="google-donut-rotate"></div>
						</div>
					</div>
				</div>
				<!-- /basic pie charts -->



		<script type="text/javascript">
			google.load("visualization", "1", {packages:["corechart"]});
			google.setOnLoadCallback(drawDonutRotated);


			// Chart settings
			function drawDonutRotated() {

			    // Data
			    var data = google.visualization.arrayToDataTable([
			        ['Task', 'Hours per Day'],
			    <?php  foreach ( $dados as $v  ) {  ?>
			    	 ['<?php echo $v['grupo'] ?>',  <?php echo $v['total'] ?>],
			    <?php } ?>    
			       // ['LACRADO',     930838.00],
			       // ['LACRADO XIAOMI',     689870.00],
			       // ['IPHONE VITRINE',  4516615.00]
			    ]);


			    // Options
			    var options_donut_rotate = {
			        fontName: 'Roboto',
			        pieHole: 0.55,
			        pieStartAngle: 180,
			        height: 300,
			        width: 500,
			        chartArea: {
			            left: 50,
			            width: '90%',
			            height: '90%'
			        }
			    };


			    // Instantiate and draw our chart, passing in some options.
			    var donut_rotate = new google.visualization.PieChart($('#google-donut-rotate')[0]);
			    donut_rotate.draw(data, options_donut_rotate);
			}
		</script>		

			<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

			<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>