<?php  
 
/* 
|-------------------------------------------------------------------------- 
| Caminho do servidor 
|-------------------------------------------------------------------------- 
| Pega automaticamente o caminho do servidor 
|  
*/ 
$base_url  = "http://" . $_SERVER['HTTP_HOST']; 
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']); 
define('URLCAMINHO', $base_url);// Consntante utilizada no arquivo de config do CI 
 
/* 
|-------------------------------------------------------------------------- 
| Conexões do FTP do boletoswebBisa 
|-------------------------------------------------------------------------- 
| Constantes para conexão ao ftp do boletoswebbisa 
| é usado no controller Boletos.php para a leitura do arquivo de retorno 
*/ 
 
$host = $_SERVER["HTTP_HOST"]; 
$host = explode('.',$host);

if($host[0] == 'www'){
$dominio = $host[1];	
} else {
$dominio = $host[0];
}


//var_dump($dominio);die();


define('SUBDOMINIO', $dominio);
//define('GRUPOLOJA', 'gruporedecell');    // CENTRO
//define('CONEXAO', 'mixcel17_megacell'); 

define('GRUPOLOJA', 'grupocell');     // BOA VIAGEM
define('CONEXAO', 'grup7514_grupocell');


//define('GRUPOLOJA', 'gruporeccell');  // ILHA DO LEITE
//define('CONEXAO', 'gru38653_gruporeccell');



 
switch ($dominio){ 

    case 'localhost': 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_matriz'); 
        define('CLIENTE', 'green');
        break; 

    case 'app': 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_matriz'); 
        define('CLIENTE', 'black');
        break;    
 	
	
    default: 
        define('BDCAMINHO', 'mixcel17_'.GRUPOLOJA.'_matriz');
        define('CLIENTE', 'red');	
       

}

if (GRUPOLOJA == 'gruporedecell') {
    // GRUPO REDE CELL
    $bases = array(
    'mixcel17_megacell_carlos',
    'mixcel17_megacell_mega',
    'mixcel17_megacell_mix',
    'mixcel17_megacell_multpecas',
    'mixcel17_megacell_paloma',
    'mixcel17_megacell_rcimports'
    );
} elseif (GRUPOLOJA == 'grupocell') {
    // GRUPO CELL
    $bases = array(
    'grup7514_grupocell_byte',
    'grup7514_grupocell_cell',
    'grup7514_grupocell_fix'
    );
} elseif (GRUPOLOJA == 'gruporeccell') {
    // GRUPO REC CELL
    $bases = array(
    'gru38653_gruporeccell_appleresolve',
    'gru38653_gruporeccell_cdd',
    'gru38653_gruporeccell_mhrecife',
    'gru38653_gruporeccell_oxepaulista',
    'gru38653_gruporeccell_oxerecife',
    'gru38653_gruporeccell_shopbyte'
    );
}




define('SGBD', $bases);
