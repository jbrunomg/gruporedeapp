//Popup script

const popoup = document.getElementById('prompt_ios')
const fundoPopoup = document.getElementById('fundo-popoup')
const btnInstall = document.getElementById('setup_button')
	
	// Detecta se o dispositivo está no iOS e mostrar o popoup se for IOS
function iosOrNot(){
	const isIos = () => {
		const userAgent = window.navigator.userAgent.toLowerCase();
		return /iphone|ipad|ipod/.test(userAgent);
	}
	// Tetecta se o dispositivo está no modo standalone
	const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

	// Verifica se deve exibir notificação popup de instalação:
	if (isIos()){
		btnInstall.style.display = 'block'
		btnInstall.setAttribute('onclick', 'iosOrNot()')
		mostrarPopoup()
		
	}else{
		btnInstall.setAttribute('onclick', 'installApp()')
		esconderPopoup()
	}
}

function mostrarPopoup(){
	popoup.style.display = 'block'
	fundoPopoup.style.display = 'block'
}

function esconderPopoup(){
	popoup.style.display = 'none'
	fundoPopoup.style.display = 'none'
}

function isRunningStandalone() {
    return (window.matchMedia('(display-mode: standalone)').matches);
}
if(!isRunningStandalone()) {
   /* código que sera executado se o site estiver em modo standalone */
	 iosOrNot()
}

//SW
if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register(`/sw.js`)
		.then(serviceWorker => {
			console.log('Service Worker registered: ' + serviceWorker);
		})
		.catch(error => {
			console.log('Error registering the Service Worker: ' + error);
		});
}

let deferredPrompt; // Allows to show the install prompt
let setupButton;

window.addEventListener('beforeinstallprompt', (e) => {
	// Prevent Chrome 67 and earlier from automatically showing the prompt
	e.preventDefault();
	// Stash the event so it can be triggered later.
	deferredPrompt = e;
	console.log("beforeinstallprompt fired");
	if (setupButton == undefined) {
		setupButton = document.getElementById("setup_button");
	}
	// Show the setup button
	setupButton.style.display = "inline";
	setupButton.disabled = false;
});

function installApp() {
	// Show the prompt
	deferredPrompt.prompt();
	setupButton.disabled = true;
	// Wait for the user to respond to the prompt
	deferredPrompt.userChoice
		.then((choiceResult) => {
			if (choiceResult.outcome === 'accepted') {
				console.log('PWA setup accepted');
				// hide our user interface that shows our A2HS button
				setupButton.style.display = 'none';
				
			} else {
				console.log('PWA setup rejected');
			}
			deferredPrompt = null;
		});
}

window.addEventListener('appinstalled', (evt) => {
	console.log("appinstalled fired", evt);
	isInstaled = true
	console.log('Onload removido.')
});